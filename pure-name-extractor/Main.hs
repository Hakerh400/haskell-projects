import qualified Data.Set as Set
import qualified Data.Map as Map
import System.Directory

import Util
import Bit
import Parser
import FileSystem
import Path

pure_ext = "pure"

pure_dir = "../../pure"
db_dir = pth_join pure_dir "db"
names_file = pth_join db_dir "names.txt"
src_dir = pth_join pure_dir "src"
elab_dir = pth_join src_dir "Elaborator"
elab_util_file = pth_join elab_dir "Util.hs"

custom_names :: Set Name
custom_names = Set.fromList
  ["lam", "app", "appr"]

main :: IO ()
main = do
  names0 <- get_names0
  let names0_set = Set.fromList names0
  names_th <- get_names_th
  names <- pure # (names0 ++) # filter <~ names_th # \name ->
    not # Set.member name names0_set
  write_file names_file # unlines' names

get_names0 :: IO [Name]
get_names0 = do
  src <- read_file elab_util_file
  return # do
    s <- lines src
    let p = tab ++ ", namei_"
    (_, "", s) <- pure # common_prefix p s
    return # mk_custom_name s

get_names_th :: IO [Name]
get_names_th = do
  s <- read_file names_file
  return # nub # lines s

is_custom_name :: Name -> Prop
is_custom_name a = Set.member a custom_names

mk_custom_name' :: Name -> Name
mk_custom_name' a = "_custom_prefix_" ++ a

mk_custom_name :: Name -> Name
mk_custom_name a = if is_custom_name a
  then mk_custom_name' a else a