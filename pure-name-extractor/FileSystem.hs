module FileSystem where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import System.Directory

import Util

add_file_ext :: String -> String -> String
add_file_ext s e = concat [s, ".", e]

fs_cwd :: IO FilePath
fs_cwd = getCurrentDirectory

fs_ls :: FilePath -> IO [FilePath]
fs_ls = listDirectory

fs_md :: FilePath -> IO ()
fs_md = createDirectoryIfMissing True

fs_exi :: FilePath -> IO Prop
fs_exi = doesPathExist

fs_exi_file :: FilePath -> IO Prop
fs_exi_file = doesFileExist

read_file :: FilePath -> IO String
read_file pth = do
  s <- readFile pth
  () <- seq (len s) # pure ()
  -- print ("read", pth, s)
  return s

write_file :: FilePath -> String -> IO ()
write_file pth s = do
  -- print ("write", pth, s)
  writeFile pth s

is_dir :: FilePath -> IO Prop
is_dir = doesDirectoryExist

is_file :: FilePath -> IO Prop
is_file = doesFileExist

ite_dir_file :: (Inhabited a) => FilePath -> IO a -> IO a -> IO a
ite_dir_file pth m1 m2 = do
  p <- is_dir pth
  if p then m1 else do
    p <- is_file pth
    ifm' p m2

ite_file_dir :: (Inhabited a) => FilePath -> IO a -> IO a -> IO a
ite_file_dir pth = flip # ite_dir_file pth