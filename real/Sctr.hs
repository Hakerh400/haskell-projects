module Sctr where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)

import Util

-- type Sctr1 = (String, String)
-- type Sctr = [Sctr1]
-- 
-- class (MonadError Sctr m) => MonadShow a m where
--   m_show :: a -> m String
-- 
-- scope_dflt = "dflt"
-- scope_err = "err"
-- 
-- show_sctr :: Sctr -> String
-- show_sctr msg = msg >>= snd
-- 
-- dfsc :: String -> Sctr1
-- dfsc = mk_pair scope_dflt
-- 
-- err :: (MonadError Sctr m) => Sctr -> m a
-- err msg = raise # (scope_err, "[Error] ") : msg
-- 
-- sctr_add :: Name -> Char -> Sctr -> Sctr
-- sctr_add scope c sctr = case sctr of
--   [] -> [(scope, [c])]
--   ((scope0, str0) : sctr') -> if scope == scope0
--     then (scope0, c : str0) : sctr'
--     else (scope, [c]) : sctr

type Sctr1 = String
type Sctr = [Sctr1]

class (MonadError Sctr m) => MonadShow a m where
  m_show :: a -> m String

scope_dflt = "dflt"
scope_err = "err"

show_sctr :: Sctr -> String
show_sctr = concat

dfsc :: String -> Sctr1
dfsc = id

err :: (MonadError Sctr m) => Sctr -> m a
err msg = {-trace' msg #-} raise # msg -- "[Error] " : msg

sctr_add :: Name -> Char -> Sctr -> Sctr
sctr_add scope c sctr = case sctr of
  [] -> [[c]]
  (cs : sctr) -> (c : cs) : sctr

sctr_rev_strs :: Sctr -> Sctr
sctr_rev_strs = map reverse