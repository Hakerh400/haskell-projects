module Real where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.List
import Data.Char
import Data.Ratio
import Control.Applicative
import Control.Monad.State
import Control.Monad.Except
import Control.Monad.Error.Class

import Prelude hiding (Real)
import qualified Prelude as P

import Util
import Bit

newtype Epsilon = Epsilon {_epsilon :: Rational}
  deriving (Eq, Ord, Show)

newtype Real' a = Real {_real :: Epsilon -> a}
  deriving (Functor, Applicative, Monad)

type Real = Real' Rational

mk_epsilon :: Rational -> Epsilon
mk_epsilon e = if e > 0 && e < 1 then Epsilon e else error #
  "Epsilon must be in interval (0, 1), got " ++ show e

get_epsilon :: (Rational -> Rational) -> Real' Epsilon
get_epsilon f = do
  Epsilon e <- Real id
  !e@(Epsilon _) <- pure # mk_epsilon # f e
  return e

real_to_rat_epsilon :: Epsilon -> Real -> Rational
real_to_rat_epsilon e (Real f) = f e

mk_real :: (P.Real a) => a -> Real
mk_real r = do
  !(Epsilon _) <- get_epsilon id
  return # toRational # r

real_neg :: Real -> Real
real_neg (Real f) = do
  e <- get_epsilon id
  return # -(f e)

real_add :: Real -> Real -> Real
real_add (Real f) (Real g) = do
  e <- get_epsilon (/ 2)
  return # f e + g e

real_sub :: Real -> Real -> Real
real_sub (Real f) (Real g) = do
  e <- get_epsilon (/ 2)
  return # f e - g e

real_mul :: Real -> Real -> Real
real_mul (Real f) (Real g) = do
  a <- Real # abs . f
  b <- Real # abs . g
  e <- get_epsilon # \e -> (e - e ^ 2) / (a + b + 4 * e)
  return # f e * g e

real_inv' :: Epsilon -> Real -> Real
real_inv' e_test@(Epsilon e) r@(Real f) = do
  a <- pure # abs # f e_test
  !e_test'@(Epsilon _) <- pure # mk_epsilon # e / 2
  if a <= e then real_inv' e_test' r else do
    !e@(Epsilon _) <- pure # mk_epsilon #
      (e / ite (a > 1) a 1) ^ 2
    return # 1 / f e

real_inv :: Real -> Real
real_inv r = do
  !e@(Epsilon _) <- Real id
  real_inv' e r

real_div :: Real -> Real -> Real
real_div a b = real_mul a # real_inv b

rat_root' :: N -> Epsilon -> Rational -> Rational -> Rational -> Rational
rat_root' n e'@(Epsilon e) a i j = let
  k = (i + j) / 2
  k2 = k ^ n
  dif = a - k2
  in if abs dif < e then k
    else if dif < 0 then rat_root' n e' a i k
    else rat_root' n e' a k j

rat_root :: N -> Epsilon -> Rational -> Rational
rat_root n e' a = rat_root' n e' a 0 # max 1 # abs a

real_root :: N -> Real -> Real
real_root n (Real f) = do
  !e'@(Epsilon e) <- get_epsilon # \e -> e ^ n
  a <- pure # f e'
  return # rat_root n e' a

real_sqrt :: Real -> Real
real_sqrt = real_root 2