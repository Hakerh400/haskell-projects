{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE IncoherentInstances #-}

module Parser where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad.State
import Control.Monad.Except
import Control.Monad.Error.Class

import Util
import Bit
import Sctr

data ParserT = ParserT
  { -- _par_str1 :: String
    _par_str :: String
  , _par_index :: N
  , _par_scope :: Name
  , _par_sctr :: Sctr
  , _par_modified :: Prop
  } deriving (Eq, Ord, Show)

newtype Parser io a = Parser (StateT ParserT (ExceptT Sctr io) a)
  deriving (Functor, Applicative, Monad)

-- instance MonadError Sctr Parser where
--   throwError msg = Parser # StateT # const #
--     ExceptT # pure # Left msg
--   catchError f = undefined

instance (MonadIO io) => MonadState ParserT (Parser io) where
  get = Parser get
  put s = Parser # put s

instance (MonadIO io) => MonadIO (Parser io) where
  liftIO m = Parser # lift2 # liftIO m

instance (MonadIO io) => MonadFail (Parser io) where
  fail s = Parser # lift #
    ExceptT # pure # Left [s]

newtype ParserM s io a = ParserM (StateT s (ExceptT Sctr (Parser io)) a)
  deriving (Functor, Applicative, Monad)

instance (MonadIO io) => MonadError Sctr (ParserM s io) where
  throwError msg = ParserM # StateT # const #
    ExceptT # pure # Left msg
  catchError f = undefined

instance (MonadIO io) => MonadState s (ParserM s io) where
  get = ParserM get
  put s = ParserM # put s

instance (MonadIO io) => MonadIO (ParserM s io) where
  liftIO m = ParserM # lift2 # Parser # lift2 # liftIO m

instance (MonadIO io) => MonadFail (ParserM s io) where
  fail s = ParserM # lift2 # Parser # lift #
    ExceptT # pure # Left [s]

-- instance (MonadFail io, MonadExt io) => MonadExt (ParserM s io) where
--   monad_ext m = do
--     Right res <- parse_ext_m m undefined undefined
--     return res

par_m_lift :: (Monad io) => io a -> ParserM s io a
par_m_lift m = ParserM # lift2 # Parser # lift2 m

instance MonadTrans (ParserM s) where
  lift = par_m_lift

-----

par_get_str :: (MonadIO io) => Parser io String
par_get_str = gets _par_str

par_put_str :: (MonadIO io) => String -> Parser io ()
par_put_str str = modify # \st -> st {_par_str = str}

par_fail :: HCS => (MonadIO io) => Parser io a
par_fail = do
  -- lift # Left [show_stack]
  rest <- gets _par_str
  Parser # lift # ExceptT # pure # Left ["Syntax error\n\n" ++ rest]

par_is_eof :: (MonadIO io) => Parser io Prop
par_is_eof = do
  st <- get
  return # null # _par_str st

par_eof :: (MonadIO io) => Parser io ()
par_eof = do
  True <- par_is_eof
  nop

par_get :: (MonadIO io) => Parser io Char
par_get = do
  st <- get
  let scope = _par_scope st
  (c : str) <- pure # _par_str st
  put # st
    { -- _par_str1 = c : _par_str1 st
      _par_str = str
    , _par_index = _par_index st + 1
    , _par_sctr = sctr_add scope c # _par_sctr st 
    }
  -- aa <- gets _par_sctr
  -- tracem aa
  return c

-- par_scope :: Name -> Parser io a -> Parser io a
-- par_scope name m = do
--   i <- gets _par_index
--   a <- m
--   j <- gets _par_index
--   pt <- get
--   let s = reverse # take (fi # j - i) # _par_str1 pt
--   put # pt {_par_sctr = (s, name) : _par_sctr pt}
--   return a

par_try :: (MonadIO io) => Parser io a -> Parser io (Either Sctr a)
par_try (Parser m) = Parser # StateT # \st -> lift # do
  res <- runExceptT # runStateT m st
  return # case res of
    Left e -> (Left e, st)
    Right (a, st') -> (Right a, st')

p_try :: (MonadIO io) => ParserM s io a -> ParserM s io (Either Sctr a)
p_try (ParserM m) = ParserM # StateT # \st -> lift #
  Parser # StateT # \pt -> lift # do
    let Parser res' = runExceptT # runStateT m st
    res <- runExceptT # runStateT res' pt
    return # case res of
      Left msg -> ((Left msg, st), pt)
      Right (res, pt') -> case res of
        Left msg -> ((Left msg, st), pt)
        Right (a, st') -> ((Right a, st'), pt')

par_save :: (MonadIO io) => Parser io ParserT
par_save = get

par_restore :: (MonadIO io) => ParserT -> Parser io ()
par_restore = put

instance (MonadIO io) => Try (Parser io) where
  try m = fmap either_to_maybe # par_try m

instance (MonadIO io) => Try (ParserM s io) where
  try m = fmap either_to_maybe # p_try m

-- instance MonadFail (Either Sctr) where
--   fail s = Left [s]

par_sctr_add :: (MonadIO io) => Name -> Char -> Parser io ()
par_sctr_add scope c = modify # \pt -> pt
  {_par_sctr = sctr_add scope c # _par_sctr pt }

p_sctr_add :: (MonadIO io) => Name -> Char -> ParserM s io ()
p_sctr_add scope c = plift # par_sctr_add scope c

p_modify :: (MonadIO io) => (String -> String) -> ParserM s io ()
p_modify f = plift # modify # \pt -> pt
  { _par_str = f # _par_str pt
  , _par_modified = True
  }

-----

-- parse_raw :: Parser io a -> String -> Maybe (a, String, Sctr)
-- parse_raw m str = do
--   st <- pure # ParserT
--     { -- _par_str1 = ""
--       _par_str = str
--     , _par_index = 0
--     , _par_scope = scope_dflt
--     , _par_sctr = []
--     , _par_modified = False
--     }
--   (a, st) <- runStateT m st
--   sctr <- pure # reverse # sctr_rev_strs # _par_sctr st
--   -- tracem (_par_str st, sctr)
--   return (a, _par_str st, sctr)

-- instance (MonadExt io) => MonadExt (ParserM s io) where
--   monad_ext m = undefined

-- monad_ext :: (MonadExt io1, MonadIO io2) => io1 a -> io2 a
-- monad_ext m = liftIO # monad_ext m

type ParserExt io1 io2 = (MonadIO io1, MonadIO io2, MonadExt io1 io2)

instance (ParserExt io1 io2) => MonadExt io1 (ParserM s io2) where
  monad_ext m = par_m_lift # monad_ext m

parse_ext_m' :: (ParserExt io1 io2) => ParserM s io1 a -> s -> String -> io2 (Either Sctr (a, String))
parse_ext_m' (ParserM m) st str = do
  pt <- pure # ParserT
    { -- _par_str1 = ""
      _par_str = str
    , _par_index = 0
    , _par_scope = scope_dflt
    , _par_sctr = []
    , _par_modified = False
    }
  
  let Parser res' = runExceptT # runStateT m st
  res <- monad_ext # runExceptT # runStateT res' pt
  return # case res of
    Left msg -> Left msg
    Right (res, pt') -> case res of
      Left msg -> Left msg
      Right (a, st') -> Right (a, _par_str pt')
  
parse_ext_m :: (ParserExt io1 io2) => ParserM s io1 a -> s -> String -> io2 (Either Sctr a)
parse_ext_m m st str = do
  res <- parse_ext_m' m st str
  return # do
    (a, rest) <- res
    ifmn (null rest) # err
      ["Syntax error\n\n" ++ rest]
    return a

parse_ext :: (ParserExt io1 io2) => ParserM s1 io1 a -> s1 -> ParserM s2 io2 a
parse_ext m st = do
  str <- plift par_get_str
  res <- parse_ext_m' m st str
  case res of
    Left msg -> raise msg
    Right (a, str) -> do
      plift # modify # \pt -> pt {_par_str = str}
      return a

parse' :: (MonadIO io) => ParserM s io a -> s -> String -> io (Either Sctr (a, String))
parse' = parse_ext_m'
  
parse :: (MonadIO io) => ParserM s io a -> s -> String -> io (Either Sctr a)
parse = parse_ext_m

  -- case runStateT (runExceptT # runStateT m st) pt of
  --   Left msg -> Left msg
  --   Right (res, pt) -> case res of
  --     Left msg -> Left msg
  --     Right (a, st) -> Right a
    
  -- Left msg -> Left msg
  -- Right ((a, _), pt) -> do
  --   let rest = drop (fi # _par_index pt) str
  --   if null rest then Right a else
  --     Left ["Syntax error\n\n" ++ rest]

-- parse' :: Parser io a -> String -> Either Sctr a
-- parse' m str = case filter (null . snd) # parse_raw m str of
--   [(a, "")] -> Right a
--   (a : b : _) -> Left ["Ambiguous parse"]
--   _ -> Left ["No parse"]
-- 
-- parse :: Parser io a -> String -> a
-- parse m str = case parse' m str of
--   Left msg -> error # concat msg
--   Right a -> a

-----

pget :: (MonadIO io) => ParserM s io Char
pget = ParserM # lift2 par_get

pfail :: (MonadIO io) => ParserM s io a
pfail = ParserM # lift2 par_fail

plift :: (MonadIO io) => Parser io a -> ParserM s io a
plift m = ParserM # lift2 m

-- type PliftSig0 s m e a = m (Either e (a, s))
-- type PliftSig1 s m e a = StateT s (ExceptT e m) a
-- type PliftSig2 f s m1 m2 m3 e1 e2 e3 a1 a2 a3 =
--   f s m1 e1 a1 -> f s m2 e2 a2 -> f s m3 e3 a3
-- type PliftSig3 f1 f2 s m1 m2 m3 e1 e2 e3 a1 a2 a3 =
--   PliftSig2 f1 s m1 m2 m3 e1 e2 e3 a1 a2 a3 ->
--   PliftSig2 f2 s m1 m2 m3 e1 e2 e3 a1 a2 a3

-- plift2 :: PliftSig3 PliftSig0 PliftSig1 s m1 m2 m3 e1 e2 e3 a1 a2 a3
plift2 f a b = StateT # \s -> ExceptT # f
  (runExceptT # runStateT a s) (runExceptT # runStateT b s)

infixr 5 <++
(<++) :: (MonadIO io) => ParserM s io a -> ParserM s io a -> ParserM s io a
(<++) (ParserM a) (ParserM b) = ParserM # StateT # \st -> ExceptT #
  Parser # StateT # \pt -> ExceptT # do
    let Parser res' = runExceptT # runStateT a st
    res <- runExceptT # runStateT res' pt
    case res of
      Left msg -> do
        Parser res' <- pure # runExceptT # runStateT b st
        runExceptT # runStateT res' pt
      Right (res, pt') -> case res of
        Left msg -> pure # Right (Left msg, pt)
        Right (a, st') -> pure # Right (Right (a, st'), pt')

infixr 5 ++>
(++>) :: (MonadIO io) => ParserM s io a -> ParserM s io a -> ParserM s io a
(++>) = flip (<++)

p_save :: (MonadIO io) => ParserM s io ParserT
p_save = plift par_save

p_restore :: (MonadIO io) => ParserT -> ParserM s io ()
p_restore s = plift # par_restore s

p_query :: (MonadIO io) => ParserM s io a -> ParserM s io a
p_query m = do
  s <- p_save
  res <- m
  p_restore s
  return res

p_char :: (MonadIO io) => Char -> ParserM s io ()
p_char c = pget >>= \c1 ->
  ite (c1 == c) nop pfail

p_char' :: (MonadIO io) => Char -> ParserM s io ()
p_char' c = p_char c >> pure ()

p_str :: (MonadIO io) => String -> ParserM s io ()
p_str [] = nop
p_str s@(c:cs) = p_char c >> p_str cs

-- p_ (MonadIO io) => cope :: Name -> ParserM s io a -> ParserM s io a
-- p_scope name m = do
--   i <- lift # gets _par_index
--   a <- m
--   j <- lift # gets _par_index
--   lift # modify # \st -> st
--     {_par_sctr = (name, i, j) : _par_sctr st}
--   return a

p_scope :: (MonadIO io) => Name -> ParserM s io a -> ParserM s io a
p_scope scope m = do
  i <- plift # gets _par_index
  plift # modify # \pt -> pt {_par_scope = scope}
  a <- m
  plift # modify # \pt -> pt {_par_scope = scope_dflt}
  -- pt <- plift get
  -- s <- pure # take (fi # _par_index pt - i) #
  --   _par_str1 pt
  return a

p_all' :: (MonadIO io) => ParserM s io a -> ParserM s io [a]
p_all' r = pure [] ++> do
  x <- r
  xs <- p_all' r
  return # x : xs

p_all :: (MonadIO io) => ParserM s io a -> ParserM s io [a]
p_all r = do
  xs <- p_all' r
  ite (null xs) pfail (pure xs)

p_sep' :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io ([a], [b])
p_sep' r s = pure ([], []) ++> do
  x <- r
  zs <- p_all' # do
    b <- s
    a <- r
    return (a, b)
  let (xs, ys) = unzip zs
  return (x : xs, ys)

p_sep :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io ([a], [b])
p_sep r s = do
  (a, b) <- p_sep' r s
  ite (null a) pfail (pure (a, b))

p_sep1 :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io ([a], [b])
p_sep1 r s = do
  (a, b) <- p_sep r s
  ite (null # tail a) pfail (pure (a, b))

p_sepf' :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io [a]
p_sepf' r s = do
  result <- p_sep' r s
  return # fst result

p_sepf :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io [a]
p_sepf r s = do
  result <- p_sep r s
  return # fst result

p_take_while :: (MonadIO io) => (Char -> Prop) -> ParserM s io String
p_take_while f = p_all' # pget >>= \c ->
  ite (f c) (pure c) pfail

p_drop_while :: (MonadIO io) => (Char -> Prop) -> ParserM s io ()
p_drop_while f = p_take_while f >> pure ()

p_take_while1 :: (MonadIO io) => (Char -> Prop) -> ParserM s io String
p_take_while1 f = do
  xs <- p_take_while f
  if null xs then pfail else pure xs

p_digit :: (MonadIO io) => ParserM s io N
p_digit = do
  c <- pget
  True <- pure # is_digit c
  return # fi # ord c - 48

is_ident_char :: Char -> Prop
is_ident_char c = or
  [ is_letter c
  , is_digit c
  , is_sub_digit c
  , is_greek c
  , c == '\x5F'
  , c == '\''
  ]

p_ident_no_trim :: (MonadIO io) => ParserM s io String
p_ident_no_trim = do
  c <- pget
  True <- pure # c == usc' || is_letter c || is_greek c
  s <- p_take_while is_ident_char
  return # c : s

p_ident :: (MonadIO io) => ParserM s io String
p_ident = trimmed p_ident_no_trim

p_ident' :: (MonadIO io) => ParserM s io String
p_ident' = trimmed # p_take_while1 is_ident_char

p_is_valid_nat :: String -> Prop
p_is_valid_nat s = s == "0" ||
  ( not (null s) &&
    all is_digit s &&
    any (/= '0') s )

p_nat :: (MonadIO io) => ParserM s io N
p_nat = do
  s <- p_ident'
  ite (p_is_valid_nat s) (pure # (read s :: N)) pfail

p_nat' :: (MonadIO io) => ParserM s io N
p_nat' = p_maybe' 0 p_nat

p_surrounded :: (MonadIO io) => String -> String -> ParserM s io a -> ParserM s io a
p_surrounded s1 s2 r = do
  p_tok s1
  a <- r
  p_tok s2
  return a

p_parens :: (MonadIO io) => ParserM s io a -> ParserM s io a
p_parens = p_surrounded "(" ")"

p_brackets :: (MonadIO io) => ParserM s io a -> ParserM s io a
p_brackets = p_surrounded "[" "]"

p_braces :: (MonadIO io) => ParserM s io a -> ParserM s io a
p_braces = p_surrounded "{" "}"

p_either :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io (Either a b)
p_either a b = (a >>= liftm Left) <++ (b >>= liftm Right)

-- p_ (MonadIO io) => ither_swap :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io (Either b a)
-- p_either_swap a b = (a >>= liftm Right) <++ (b >>= liftm Left)
-- 
-- p_ (MonadIO io) => ither' :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io (Either b a)
-- p_either' = flip p_either
-- 
-- p_ (MonadIO io) => ither_swap' :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io (Either a b)
-- p_either_swap' = flip p_either_swap

p_either' :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io (Either a b)
p_either' a b = (b >>= liftm Right) <++ (a >>= liftm Left)

-- p_ (MonadIO io) => ither' :: (MonadIO io) => ParserM s io a -> ParserM s io b -> ParserM s io (Either a b)
-- p_either' a b = do
--   s <- get
--   f1 <- pure # do
--     res <- runExceptT # runStateT a s
--     case res of
--       Left _ -> par_fail
--       Right a -> pure # Just a
--   res <- lift2 # par_orelse f1 # pure Nothing
--   case res of
--     Nothing -> b >>= liftm Right
--     Just (x, s) -> do
--       put s
--       return # Left x

p_sp :: (MonadIO io) => ParserM s io ()
p_sp = p_char' space

p_rest :: (MonadIO io) => ParserM s io String
-- p_rest = p_take_while # const True
p_rest = plift # do
  s <- gets _par_str
  modify # \par -> par {_par_str = ""}
  return s

p_eof :: (MonadIO io) => ParserM s io ()
p_eof = plift par_eof

choice :: (MonadIO io) => [ParserM s io a] -> ParserM s io a
choice xs = if null xs then pfail else foldr1 (<++) xs

p_new_line' :: (MonadIO io) => ParserM s io ()
p_new_line' = p_str "\r\n" <++ choice [p_str "\r", p_str "\n"]

p_new_line :: (MonadIO io) => ParserM s io ()
p_new_line = p_new_line' >> nop

p_ws_aux :: (MonadIO io) => Prop -> ParserM s io ()
p_ws_aux one = do
  lf <- pure True
  let f = ite one p_all p_all'
  let p = choice [p_sp, p_new_line]
  f p >> nop

p_ws :: (MonadIO io) => ParserM s io ()
p_ws = p_ws_aux False

p_ws1 :: (MonadIO io) => ParserM s io ()
p_ws1 = p_ws_aux True

trimmed_aux :: (MonadIO io) => ParserM s io () -> ParserM s io a -> ParserM s io a
trimmed_aux ws r = do
  ws
  x <- r
  ws
  return x

trimmed :: (MonadIO io) => ParserM s io a -> ParserM s io a
trimmed = trimmed_aux p_ws

p_tok :: (MonadIO io) => String -> ParserM s io ()
p_tok t = trimmed # p_str t

p_tok' :: (MonadIO io) => String -> ParserM s io String
p_tok' t = trimmed (p_str t) >> pure t

p_toks :: (MonadIO io) => [String] -> ParserM s io ()
p_toks = mapM_ p_tok

p_maybe :: (MonadIO io) => ParserM s io a -> ParserM s io (Maybe a)
p_maybe p = (p >>= liftm Just) <++ pure Nothing

p_maybe' :: (MonadIO io) => a -> ParserM s io a -> ParserM s io a
p_maybe' z p = do
  res <- p_maybe p
  return # fromMaybe z res

p_assert :: (MonadIO io) => Prop -> ParserM s io ()
p_assert p = if p then nop else pfail

parse_word :: (MonadIO io) => Name -> ParserM s io ()
parse_word name = do
  name' <- p_ident
  if name' == name then nop else pfail

p_usc :: (MonadIO io) => ParserM s io ()
p_usc = parse_word usc >> nop

-----

p_list_aux' :: (MonadIO io) => String -> String -> Prop -> Prop -> ParserM s io a -> ParserM s io [a]
p_list_aux' tok1 tok2 empty trailing m = do
  p_tok tok1
  let sep = p_tok ","
  let f = ite empty p_sepf' p_sepf
  xs <- f m sep
  if trailing then p_maybe sep >> nop else nop
  p_tok tok2
  return xs

p_list_aux :: (MonadIO io) => String -> String -> Prop -> Prop -> Prop -> ParserM s io a -> ParserM s io [a]
p_list_aux tok1 tok2 empty single trailing m = do
  let m1 = p_list_aux' tok1 tok2 empty trailing m
  if single then m1 <++ (m >>= liftm pure) else m1

p_list_parens :: (MonadIO io) => Prop -> Prop -> Prop -> ParserM s io a -> ParserM s io [a]
p_list_parens = p_list_aux "(" ")"

p_list_brackets :: (MonadIO io) => Prop -> Prop -> Prop -> ParserM s io a -> ParserM s io [a]
p_list_brackets = p_list_aux "[" "]"

p_list_braces :: (MonadIO io) => Prop -> Prop -> Prop -> ParserM s io a -> ParserM s io [a]
p_list_braces = p_list_aux "{" "}"

p_list :: (MonadIO io) => ParserM s io a -> ParserM s io [a]
p_list = p_list_brackets False False False

p_list' :: (MonadIO io) => ParserM s io a -> ParserM s io [a]
p_list' = p_list_brackets False True False

p_list0 :: (MonadIO io) => ParserM s io a -> ParserM s io [a]
p_list0 = p_list_brackets True False False

p_list0' :: (MonadIO io) => ParserM s io a -> ParserM s io [a]
p_list0' = p_list_brackets True True False

p_prop :: (MonadIO io) => ParserM s io a -> ParserM s io Prop
p_prop p = pure False ++> (p >> pure True)

p_test :: (MonadIO io) => ParserM s io a -> ParserM s io Prop
p_test p = p_query # p_prop p

p_bit :: (MonadIO io) => ParserM s io Bit
p_bit = do
  c <- pget
  case c of
    '0' -> pure 0
    '1' -> pure 1
    _ -> pfail

p_bits :: (MonadIO io) => ParserM s io [Bit]
p_bits = trimmed # p_all p_bit