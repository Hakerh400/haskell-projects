import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.List
import Data.Char
import Data.Ratio
import Control.Monad.State
import Control.Monad.Except
import Control.Monad.Error.Class

import Prelude hiding (Real)
import qualified Prelude as P

import Util
import Bit
import Parser
import Real

precision = 64

epsilon :: Epsilon
epsilon = mk_epsilon # 1 / 2 ^ precision

main :: IO ()
main = do
  let n = 10
  let a = 2
  let b = 1024
  -- actual <- pure # sqrt a
  approx <- pure # real_to_rat_epsilon epsilon #
    real_sub (real_root n # mk_real b) (mk_real a)
  -- dif <- pure # abs # actual - approx
  print # fromRational approx