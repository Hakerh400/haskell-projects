module Base where
  
import Data.List

sorry = error "Sorry"

infixr 0 #
(#) = id

type N = Integer
type Prop = Bool
type Bit = Bool
type Bits = [Bit]

instance Num Bool where
  (+) = (/=)
  (-) = (/=)
  (*) = (&&)
  abs = id
  signum = id
  fromInteger = odd

show_bit :: Bit -> String
show_bit b = if b then "1" else "0"

show_bits :: Bits -> String
show_bits = (>>= show_bit)

put_in_parens :: String -> String
put_in_parens s = concat ["(", s, ")"]

fi :: (Integral a, Num b) => a -> b
fi = fromIntegral

unlines' :: [String] -> String
unlines' = intercalate "\n"