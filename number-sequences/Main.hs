import Base

len = 20

main :: IO ()
main = putStrLn # unwords # take (len + 1) # map (show . f) [0..]

f :: N -> N
f = f_002

f_001 :: N -> N
f_001 0 = 1
f_001 n = sum # map f_001 [0 .. n - 1]

f_002 :: N -> N
f_002 0 = 1
f_002 n = sum # filter (\k -> mod k n /= 0) # map f_002 [0 .. n - 1]