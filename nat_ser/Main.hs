import Prelude hiding (putStr, putStrLn, print, log)
import Data.Maybe
import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Reader

import Util
import Bit

t_nat :: NatSerT () N
t_nat = NatSerT
  { _ns_is_type_fin = \s -> False
  , _ns_get_ctors = \s -> [[], [()]]
  , _ns_mk = \s k vs -> case (s, k, vs) of
    ((), 0, []) -> 0
    ((), 1, [n]) -> n + 1
  , _ns_cases = \v -> case v of
    0 -> (0, [])
    n -> (1, [n - 1])
  }

data Bin
  = BinNil
  | Bin0 Bin
  | Bin1 Bin
  deriving Show

-- instance Show Bin where
--   show b = case b of
--     Bin0 -> "0"
--     Bin1 -> "1"
--     BinNil -> ""
--     BinCons Bin0 a -> '0' : show a
--     BinCons Bin1 a -> '1' : show a

t_bin :: NatSerT () Bin
t_bin = NatSerT
  { _ns_is_type_fin = \s -> False
  , _ns_get_ctors = \s -> [[], [()], [()]]
  , _ns_mk = \s k vs -> case (s, k, vs) of
    ((), 0, []) -> BinNil
    ((), 1, [a]) -> Bin0 a
    ((), 2, [a]) -> Bin1 a
  , _ns_cases = \v -> case v of
    BinNil -> (0, [])
    Bin0 a -> (1, [a])
    Bin1 a -> (2, [a])
  }

main :: IO ()
main = do
  let s = ()
  f <- pure # \v -> print # val_to_nat s v t_bin
  f # BinNil
  f # Bin0 # BinNil
  f # Bin1 # BinNil
  f # Bin0 # Bin0 # BinNil
  f # Bin1 # Bin0 # BinNil
  f # Bin0 # Bin1 # BinNil
  f # Bin1 # Bin1 # BinNil
  f # Bin0 # Bin0 # Bin0 # BinNil

data NatSerT s v = NatSerT
  { _ns_is_type_fin :: s -> Prop
  , _ns_get_ctors :: s -> [[s]]
  , _ns_mk :: s -> N -> [v] -> v
  , _ns_cases :: v -> (N, [v])
  }

type Table s v = Map s [v]

type NatSer s v = StateT
  (Table s v, [(N, N)])
  (Reader (NatSerT s v))

is_type_fin :: (Ord s, Show s, Show v) => s -> NatSer s v Prop
is_type_fin s = do
  is_type_fin <- lift # reader _ns_is_type_fin
  return # is_type_fin s

is_type_inf :: (Ord s, Show s, Show v) => s -> NatSer s v Prop
is_type_inf s = is_type_fin s >>= liftm not

is_ctor_fin :: (Ord s, Show s, Show v) => [s] -> NatSer s v Prop
is_ctor_fin args = do
  res <- mapM is_type_fin args
  return # and res

is_ctor_inf :: (Ord s, Show s, Show v) => [s] -> NatSer s v Prop
is_ctor_inf args = is_ctor_fin args >>= liftm not

ns_mk :: (Ord s, Show s, Show v) => s -> N -> [v] -> NatSer s v v
ns_mk s i xs = do
  f <- lift # reader _ns_mk
  return # f s i xs

ns_cases :: (Ord s, Show s, Show v) => v -> NatSer s v (N, [v])
ns_cases v = do
  f <- lift # reader _ns_cases
  return # f v

-- get_nat :: (Ord s, Show s, Show v) => NatSer s v N
-- get_nat = gets snd
-- 
-- put_nat :: (Ord s, Show s, Show v) => N -> NatSer s v ()
-- put_nat n = modify # \(t, _) -> (t, n)
-- 
-- modify_nat :: (Ord s, Show s, Show v) => (N -> N) -> NatSer s v ()
-- modify_nat f = modify # \(t, n) -> (t, f n)

get_table :: (Ord s, Show s, Show v) => NatSer s v (Table s v)
get_table = gets fst

put_table :: (Ord s, Show s, Show v) => Table s v -> NatSer s v ()
put_table t = modify # \(_, n) -> (t, n)

modify_table :: (Ord s, Show s, Show v) => (Table s v -> Table s v) -> NatSer s v ()
modify_table f = modify # \(t, n) -> (f t, n)

ns_add_bnd :: (Ord s, Show s, Show v) => N -> N -> NatSer s v ()
ns_add_bnd n k = modify # \(t, xs) -> (t, (n, k) : xs)

ns_add :: (Ord s, Show s, Show v) => N -> NatSer s v ()
ns_add = ns_add_bnd 1

table_insert :: (Ord s, Show s, Show v) => s -> v -> NatSer s v ()
table_insert s v = modify_table # Map.adjust (snoc' v) s

get_ctors :: (Ord s, Show s, Show v) => s -> NatSer s v [[s]]
get_ctors s = do
  f <- lift # reader _ns_get_ctors
  return # f s

get_fin_ctors' :: (Ord s, Show s, Show v) => [[s]] -> NatSer s v [[s]]
get_fin_ctors' ctors = case ctors of
  [] -> pure []
  (ctor:ctors) -> do
    fin <- is_ctor_fin ctor
    if fin then do
      ctors <- get_fin_ctors' ctors
      return # ctor : ctors
    else pure []

get_fin_ctors :: (Ord s, Show s, Show v) => s -> NatSer s v [[s]]
get_fin_ctors s = do
  ctors <- get_ctors s
  get_fin_ctors' ctors

get_ctors_num :: (Ord s, Show s, Show v) => s -> NatSer s v N
get_ctors_num s = do
  ctors <- get_ctors s
  return # len ctors

get_fin_ctors_num :: (Ord s, Show s, Show v) => s -> NatSer s v N
get_fin_ctors_num s = do
  ctors <- get_fin_ctors s
  return # len ctors

get_ctor :: (Ord s, Show s, Show v) => s -> N -> NatSer s v [s]
get_ctor s i = do
  ctors <- get_ctors s
  return # list_get i ctors

get_vals_1 :: (Ord s, Show s, Show v) => s -> N -> NatSer s v [v]
get_vals_1 s i = do
  ctor <- get_ctor s i
  lists <- mapM get_vals ctor
  mapM (ns_mk s i) # mk_combs lists

get_vals_n :: (Ord s, Show s, Show v) => s -> N -> NatSer s v [v]
get_vals_n s n = do
  lists <- mapM (get_vals_1 s) [0 .. n - 1]
  return # concat lists

get_vals :: (Ord s, Show s, Show v) => s -> NatSer s v [v]
get_vals s = do
  n <- get_ctors_num s
  get_vals_n s n

get_fin_vals :: (Ord s, Show s, Show v) => s -> NatSer s v [v]
get_fin_vals s = do
  n <- get_fin_ctors_num s
  get_vals_n s n

prepare_table :: (Ord s, Show s, Show v) => s -> Table s v -> NatSer s v (Table s v)
prepare_table s t = if Map.member s t then pure t else do
  xs <- get_fin_vals s
  modify_table # map_insert s xs
  get_table

ns_eq :: (Ord s, Show s, Show v) => v -> v -> NatSer s v Prop
ns_eq x y = do
  (i, xs) <- ns_cases x
  (j, ys) <- ns_cases y
  if i /= j then pure False else do
    res <- zipWithM ns_eq xs ys
    return # and res

ns_index_of :: (Ord s, Show s, Show v) => v -> [v] -> NatSer s v (Maybe N)
ns_index_of v xs = case xs of
  [] -> pure Nothing
  (x:xs) -> do
    eq <- ns_eq v x
    if eq then pure # Just 0 else do
      index <- ns_index_of v xs
      return # index >>= liftm (+1)

table_index_of :: (Ord s, Show s, Show v) => s -> v -> Table s v -> NatSer s v (Maybe N)
table_index_of s v t = ns_index_of v # map_get s t

is_in_table :: (Ord s, Show s, Show v) => s -> v -> Table s v -> NatSer s v Prop
is_in_table s v t = do
  index <- table_index_of s v t
  return # isJust index

get_table_size :: (Ord s, Show s, Show v) => s -> Table s v -> NatSer s v N
get_table_size s t = pure # len # map_get s t

val_to_nat_aux :: (Ord s, Show s, Show v) => s -> v -> Prop -> Table s v -> NatSer s v ()
val_to_nat_aux s v is_last t = do
  t <- prepare_table s t
  index <- table_index_of s v t
  if is_last then do
    case index of
      Just i -> ns_add i
      Nothing -> do
        n <- get_table_size s t
        ns_add n
        
        d <- get_fin_ctors_num s
        n <- get_ctors_num s
        (k, args) <- ns_cases v
        ns_add_bnd (n - d) (k - d)
        
        ss <- get_ctor s k
        let arity = len args
        
        flip mapM_ (zip args [0..]) # \(v, i) -> do
          let s' = list_get i ss
          let is_last = i == arity - 1
          t <- get_table
          t <- if not is_last then pure t else
            case Map.lookup s' t of
              Nothing -> pure t
              Just vs -> do
                let args' = init args
                vs <- flip filterM vs # \arg -> do
                  v <- ns_mk s k # snoc args' arg
                  inside <- is_in_table s v t
                  return # not inside
                return # map_insert s' vs t
          val_to_nat_aux s' v is_last t
        
        table_insert s v
    else case index of
      Just i -> do
        is_fin <- is_type_fin s
        if is_fin then nop else
          ns_add_bnd 2 0
        
        n <- get_table_size s t
        ns_add_bnd n i
      Nothing -> do
        ns_add_bnd 2 1
        
        n <- get_ctors_num s
        d <- get_fin_ctors_num s
        (k, args) <- ns_cases v
        ns_add_bnd (n - d) (k - d)
        
        ss <- get_ctor s k
        let arity = len args
        
        flip mapM_ (zip args [0..]) # \(v, i) -> do
          let s' = list_get i ss
          let is_last = i == arity - 1
          t <- get_table
          t <- if not is_last then pure t else
            case Map.lookup s' t of
              Nothing -> pure t
              Just vs -> do
                let args' = init args
                vs <- flip filterM vs # \arg -> do
                  v <- ns_mk s k # snoc args' arg
                  inside <- is_in_table s v t
                  return # not inside
                return # map_insert s' vs t
          val_to_nat_aux s' v False t
        
        table_insert s v

val_to_nat :: (Ord s, Show s, Show v) => s -> v -> NatSerT s v -> N
val_to_nat s v ns = let
  xs = snd #
    flip runReader ns #
    flip execStateT (Map.empty, []) #
    val_to_nat_aux s v True Map.empty
  in (\f -> foldl f 0 xs) # \acc (n, k) -> acc * n + k