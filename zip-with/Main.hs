import Prelude hiding (zipWith)

import ZipWith

main :: IO ()
main = do
  print $ zipWith (+1) [1 :: Int, 2, 3]
  print $ zipWith (,,) [1, 2, 3] [True, False] ["abc", "de"]