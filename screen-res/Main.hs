import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import System.Process

import Util

width = 1920
height = 1080
refresh_rate = 60.0

main :: IO ()
main = do
  inp <- pure # concat
    [ "xrandr | grep -e \" connected [^(]\" | "
    , "sed -e \"s/\\([A-Z0-9]\\+\\) connected.*/\\1/\"" ]
  device_id <- readProcess "bash" [] inp
  device_id <- pure # head # filter (not . null) # lines device_id

  args <- pure
    [ show width
    , show height
    , show refresh_rate ]
  cvt_info <- readProcess "cvt" args ""

  (name:opts) <- pure # tail # words # lines cvt_info !! 1
  name <- pure # init # tail name
  args <- pure # ["--newmode", name] ++ opts
  readProcessWithExitCode "xrandr" args ""

  args <- pure ["--addmode", device_id, name]
  s <- readProcess "xrandr" args ""

  args <- pure ["--output", device_id, "--mode", name]
  readProcess "xrandr" args ""
  
  putStr s