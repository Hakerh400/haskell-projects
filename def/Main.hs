import Base

import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

main = do
  let f = def2 ["a", "b"] # ["x", "a", "y", "b"]
  let g = def1 ["z"] # f "0" "z" ++ ["1"]
  let h = def0 [] # g "a"
  putStrLn # "x0ya1"
  putStrLn # concat h

type S = String

mk_map_func :: [S] -> [S] -> [S] -> [S]
mk_map_func args body xs = map (\s -> maybe s (xs!!) # elemIndex s args) body

def0 :: [S] -> [S] -> [S]
def0 args body = mk_map_func args body []

def1 :: [S] -> [S] -> (S -> [S])
def1 args body = \a -> mk_map_func args body [a]

def2 :: [S] -> [S] -> (S -> S -> [S])
def2 args body = \a b -> mk_map_func args body [a, b]