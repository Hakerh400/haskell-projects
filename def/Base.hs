module Base where
  
import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

sorry = error "Sorry"

infixr 0 #
(#) = id

type N = Integer
type Prop = Bool
type Bit = Bool
type Bits = [Bit]

instance Num Bool where
  (+) = (/=)
  (-) = (/=)
  (*) = (&&)
  abs = id
  signum = id
  fromInteger = odd

show_bit :: Bit -> String
show_bit b = if b then "1" else "0"

show_bits :: Bits -> String
show_bits = (>>= show_bit)

surround :: a -> a -> [a] -> [a]
surround a b xs = [a] ++ xs ++ [b]

surround_by_parens :: String -> String
surround_by_parens = surround '(' ')'

fi :: (Integral a, Num b) => a -> b
fi = fromIntegral

unlines' :: [String] -> String
unlines' = intercalate "\n"

dbg :: (Show a) => a -> b
dbg a = error # show a

allM :: (Monad m) => (a -> m Prop) -> [a] -> m Prop
allM f = foldM (allM' f) True

allM' :: (Monad m) => (a -> m Prop) -> Prop -> a -> m Prop
allM' f p x = if p then f x else pure False

set_at :: N -> a -> [a] -> [a]
set_at 0 z (x:xs) = z : xs
set_at n z (x:xs) = x : set_at (n - 1) z xs

insert_at :: N -> a -> [a] -> [a]
insert_at 0 z xs = z : xs
insert_at n z (x:xs) = x : insert_at (n - 1) z xs

append_at :: N -> [a] -> [a] -> [a]
append_at 0 zs xs = zs ++ xs
append_at n zs (x:xs) = x : append_at (n - 1) zs xs

remove_at :: N -> [a] -> [a]
remove_at 0 (x:xs) = xs
remove_at n (x:xs) = x : remove_at (n - 1) xs

ite :: Prop -> a -> a -> a
ite True a b = a
ite False a b = b

-- assert :: (Monad m) => Prop -> m ()
-- assert True = pure ()

swap :: N -> N -> [a] -> [a]
swap i j xs = set_at i (xs !! fi j) # set_at j (xs !! fi i) xs

lift :: (Monad m) => (a -> a) -> a -> m a
lift f x = pure # f x

get_avail :: (Ord a) => [a] -> Set a -> a
get_avail (x:xs) s = if Set.member x s then get_avail xs s else x

get_avail' :: (Ord a) => [a] -> Set a -> (a, Set a)
get_avail' xs s = let
  x = get_avail xs s
  in (x, Set.insert x s)

nop :: (Monad m) => m ()
nop = pure ()

fix :: (a -> a) -> a
fix f = f (fix f)