import Prelude hiding (putStr, putStrLn, print, log)
import Data.List
import Data.Maybe
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Util
import Logic

parens :: Prop -> [String] -> String
parens top xs = put_in_parens' (not top) # unwords xs

show_expr' :: Prop -> Expr -> String
show_expr' top e = let
  f = show_expr' False
  p = parens top
  in if e == get_prop true then "⊤"
    else if e == get_prop false then "⊥"
    else case e of
      Zero -> "0"
      Succ a -> show # read (f a) + 1
      Var a -> [letters_low !! read (f a)]
      Some a b -> p ["%", f (Var a) ++ ",", show b]
      Set a -> p ["set", f a]
      Mem a b -> p [f a, "∈", f b]
      Imp a b -> p [f a, "→", f b]
      Univ a b -> case show b of
        ('∀':_:s) -> p ["∀", f (Var a), s]
        s -> p ["∀", f (Var a) ++ ",", s]
      Wff a -> p ["wff", f a]

show_expr :: Expr -> String
show_expr = show_expr' True

instance Show Expr where
  show = show_expr

show_proof :: Proof -> String
show_proof p = unwords ["⊢", show # _expr p]

instance Show Proof where
  show = show_proof

get_prop :: Proof -> Expr
get_prop p = case _expr p of
  Wff a -> a

-- Variables

var' :: N -> Proof
var' n = if n == 0 then set_var_zero else
  set_var_succ # var' # n - 1

var :: String -> Proof
var (c:_) = var' # fi # fromJust # elemIndex c letters_low

a = var "a"; b = var "b"; c = var "c"; d = var "d"; e = var "e"

-- Definitions

false :: Proof
false = wff_univ a # wff_univ b # wff_mem a b

true :: Proof
true = wff_imp false false

-- Proofs

imp_rfl :: Proof -> Proof
imp_rfl a = let
  a' = wff_imp a a
  in mp (mp (ax_imp_2 a a' a) # ax_imp_1 a a') # ax_imp_1 a a

triv :: Proof
triv = imp_rfl false

-- Main

proof :: Proof
proof = let
  a = triv
  in a

main :: IO ()
main = do
  print proof