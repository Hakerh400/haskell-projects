module Logic.Logic where

import Prelude hiding (putStr, putStrLn, print, log)

import Util

data Expr
  = Zero
  | Succ Expr
  | Var Expr
  | Some Expr Expr
  | Set Expr
  | Mem Expr Expr
  | Imp Expr Expr
  | Univ Expr Expr
  | Wff Expr
  | VarLt Expr Expr
  | VarNe Expr Expr
  | Fresh Expr Expr
  | Subst Expr Expr Expr Expr
  deriving (Eq, Ord)

data Proof = P Expr

_expr :: Proof -> Expr
_expr (P e) = e

test' :: Prop -> a -> a
test' True a = a

test :: [Expr] -> Proof -> Proof
test xs p = case xs of
  [] -> p
  (x:y:ys) -> test' (x == y) # test ys p

set_var_zero :: Proof
set_var_zero = P # Set # Var Zero

set_var_succ :: Proof -> Proof
set_var_succ (P (Set (Var a))) = P # Set # Var # Succ a

set_some :: Proof -> Proof -> Proof
set_some (P (Set (Var a))) (P (Wff b)) = P # Set # Some a b

wff_mem :: Proof -> Proof -> Proof
wff_mem (P (Set a)) (P (Set b)) = P # Wff # Mem a b

wff_imp :: Proof -> Proof -> Proof
wff_imp (P (Wff a)) (P (Wff b)) = P # Wff # Imp a b

wff_univ :: Proof -> Proof -> Proof
wff_univ (P (Set (Var a))) (P (Wff b)) = P # Wff # Univ a b

var_zero_lt_succ :: Proof -> Proof
var_zero_lt_succ (P (Set (Var a))) = P # VarLt Zero # Succ a

var_succ_lt_succ_of :: Proof -> Proof
var_succ_lt_succ_of (P (VarLt a b)) = P # VarLt (Succ a) (Succ b)

var_ne_of_lt :: Proof -> Proof
var_ne_of_lt (P (VarLt a b)) = P # VarNe a b

fresh_var :: Proof -> Proof
fresh_var (P (VarNe a b)) = P # Fresh a # Set # Var b

fresh_some_1 :: Proof -> Proof -> Proof
fresh_some_1 (P (Wff a)) (P (Set (Var b))) = P # Fresh b # Some b a

fresh_some_2 :: Proof -> Proof -> Proof -> Proof
fresh_some_2 (P (Wff a)) (P (Set (Var b))) (P (Fresh c a')) =
  test [a, a'] # P # Fresh c # Some b a

fresh_mem :: Proof -> Proof -> Proof -> Proof -> Proof
fresh_mem (P (Set a)) (P (Set b)) (P (Fresh c a')) (P (Fresh c' b')) =
  test [a, a', b, b', c, c'] # P # Fresh c (Mem a b)

fresh_imp :: Proof -> Proof -> Proof -> Proof -> Proof
fresh_imp (P (Wff a)) (P (Wff b)) (P (Fresh c a')) (P (Fresh c' b')) =
  test [a, a', b, b', c, c'] # P # Fresh c (Imp a b)

fresh_univ_1 :: Proof -> Proof -> Proof
fresh_univ_1 (P (Wff a)) (P (Set (Var b))) = P # Fresh b # Univ b a

fresh_univ_2 :: Proof -> Proof -> Proof -> Proof
fresh_univ_2 (P (Wff a)) (P (Set (Var b))) (P (Fresh c a')) =
  test [a, a'] # P # Fresh c # Univ b a

subst_var_1 :: Proof -> Proof -> Proof
subst_var_1 (P (Set r)) (P (Set (Var x))) = P # Subst x r (Var x) r

subst_var_2 :: Proof -> Proof -> Proof
subst_var_2 (P (Set r)) (P (VarNe x y)) = P # Subst x r (Var y) (Var y)

subst_some_1 :: Proof -> Proof -> Proof -> Proof
subst_some_1 (P (Set r)) (P (Wff s)) (P (Set (Var x))) =
  P # Subst x r (Some x s) (Some x s)

subst_some_2 :: Proof -> Proof -> Proof -> Proof -> Proof
subst_some_2 (P (Wff s)) (P (VarNe x y)) (P (Fresh y' r)) (P (Subst x' r' s' a)) =
  test [x, x', y, y', r, r', s, s'] # P # Subst x r (Some y s) (Some y a)

subst_mem :: Proof -> Proof -> Proof -> Proof -> Proof
subst_mem (P (Set s)) (P (Set t)) (P (Subst x r s' a)) (P (Subst x' r' t' b)) =
  test [x, x', r, r', t, t', s, s'] # P # Subst x r (Mem s t) (Mem a b)

subst_imp :: Proof -> Proof -> Proof -> Proof -> Proof
subst_imp (P (Wff s)) (P (Wff t)) (P (Subst x r s' a)) (P (Subst x' r' t' b)) =
  test [x, x', r, r', t, t', s, s'] # P # Subst x r (Imp s t) (Imp a b)

subst_univ_1 :: Proof -> Proof -> Proof -> Proof
subst_univ_1 (P (Set r)) (P (Wff s)) (P (Set (Var x))) =
  P # Subst x r (Univ x s) (Univ x s)

subst_univ_2 :: Proof -> Proof -> Proof -> Proof -> Proof
subst_univ_2 (P (Wff s)) (P (VarNe x y)) (P (Fresh y' r)) (P (Subst x' r' s' a)) =
  test [x, x', y, y', r, r', s, s'] # P # Subst x r (Univ y s) (Univ y a)

ax_imp_1 :: Proof -> Proof -> Proof
ax_imp_1 (P (Wff a)) (P (Wff b)) = P # Imp a # Imp b a

ax_imp_2 :: Proof -> Proof -> Proof -> Proof
ax_imp_2 (P (Wff a)) (P (Wff b)) (P (Wff c)) = P #
  Imp (Imp a # Imp b c) (Imp (Imp a b) (Imp a c))

univ_i :: Proof -> Proof -> Proof
univ_i (P (Imp a b)) (P (Fresh x a')) = test [a, a'] # P # Imp a (Univ x b)

univ_e :: Proof -> Proof -> Proof
univ_e (P (Imp a (Univ x b))) (P (Subst x' r b' c)) = test [x, x', b, b'] # P # Imp a c

mp :: Proof -> Proof -> Proof
mp (P (Imp a b)) (P a') = test [a, a'] # P b