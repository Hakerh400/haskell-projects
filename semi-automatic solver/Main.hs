import Util
import Bit

import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Monad
import Control.Monad.State
import System.Directory
import System.IO

file = "../../Data/semi-automatic solver/1.txt"
avail_moves = "0123abcdx"
search_mode = BFS

data SearchMode = BFS | DFS
  deriving (Eq, Ord, Show, Read)

data InfoT = InfoT
  { _mvs_st :: Map String String
  , _st_mvs :: Map String String
  , _mvs_list :: [String]
  } deriving (Eq, Ord, Show, Read)

type Info = StateT InfoT IO

init_info :: InfoT
init_info = InfoT
  { _mvs_st = Map.empty
  , _st_mvs = Map.empty
  , _mvs_list = [""]
  }

main :: IO ()
main = do
  mapM_ (flip hSetBuffering NoBuffering) [stdin, stdout, stderr]
  info <- load_file
  info <- execStateT run info
  save_file info

run :: Info ()
run = do
  info <- get
  let mvs_list = _mvs_list info
  if null mvs_list then nop else do
    (mvs:mvs_list) <- pure mvs_list
    let mvs_st = _mvs_st info
    st <- liftIO # do
      s <- if null mvs then pure "" else do
        let st_prev = map_get (init mvs) mvs_st
        let s = trim # unwords # [init mvs, [last mvs]]
        return # concat
          [replicate (fi # len s + 1) ' ', st_prev, "\n", s, " "]
      putStr s
      getLine
    if st == "\\" then nop else do
      info <- pure # info {_mvs_list = mvs_list}
      put info
      if null st then nop else do
        let st_mvs = _st_mvs info
        if Map.member st st_mvs then nop else put # info
          { _mvs_st = Map.insert mvs st mvs_st
          , _st_mvs = Map.insert st mvs st_mvs
          , _mvs_list = add_moves mvs mvs_list
          }
      run

add_moves' :: [String] -> [String] -> [String]
add_moves' mvs_new mvs_list = case search_mode of
  BFS -> mvs_list ++ mvs_new
  DFS -> mvs_new ++ mvs_list

add_moves :: String -> [String] -> [String]
add_moves mvs = add_moves' # map (snoc mvs) avail_moves

init_file :: IO InfoT
init_file = do
  let info = init_info
  save_file info
  return info

save_file :: InfoT -> IO ()
save_file info = writeFile file # show info

load_file :: IO InfoT
load_file = do
  p <- doesPathExist file
  if not p then init_file else do
    s <- readFile file
    if null s then init_file else do
      let info = read s
      seq info # pure info