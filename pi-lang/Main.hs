{-# LANGUAGE ScopedTypeVariables #-}

import Data.List hiding (and, or, union, insert)
import Data.Maybe
import Control.Monad

import qualified Data.Set as Set
import Data.Set (Set)

import qualified Prelude as P
import Prelude hiding (pi, not, and, or, id, succ)

import Base

data S =
  FromSet (Set S) |
  FromPred (S -> Prop)

toSet (FromSet a) = a
toSet (FromPred p) = sorry

lift (f :: Set S -> Set S) (a :: S) = FromSet (f (toSet a))
lift2 (f :: Set S -> Set S -> Set S) (a :: S) (b :: S) = FromSet (f (toSet a) (toSet b))
lifts (f :: Set (Set S) -> Set S) (a :: S) = let
  b = Set.map toSet (toSet a)
  in FromSet (f b)

-- le (a :: Set S) (b :: Set S) = let
--   elems = Set.union a b
--   filtered = Set.filter (\c -> P.not (Set.member c a && Set.member c b)) elems
--   in case Set.lookupMin filtered of
--     Nothing -> True
--     Just c -> Set.member c b

le (a :: Set S) (b :: Set S) = a <= b

instance Eq S where
  (a :: S) == (b :: S) = toSet a == toSet b

instance Ord S where
  (a :: S) <= (b :: S) = le (toSet a) (toSet b)

instance Show S where
  show (a :: S) = let
    strs = map show (Set.elems (toSet a))
    in concat ["{", intercalate ", " strs , "}"]

empty = FromSet Set.empty
unions (a :: S) = lifts Set.unions a
sngl (a :: S) = FromSet (Set.singleton a)
upair (a :: S) (b :: S) = FromSet (Set.fromList [a, b])
pair (a :: S) (b :: S) = upair a (upair a b)
pset (a :: S) = FromSet # Set.map FromSet # Set.powerSet # toSet a
pick (a :: S) = case Set.lookupMin (toSet a) of
  Nothing -> sorry
  Just x -> toSet x

-- paird (p :: S) = let
--   elems = Set.elems (toSet p)
--   in if length elems /= 2 then Nothing else let
--     [a, b] = elems
--     (x, b') = if Set.member a (toSet b) then (a, b) else (b, a)
--     ys = Set.elems # Set.delete x # toSet b'
--     y = if null ys then x else head ys
--     in if p /= pair x y then Nothing else Just (x, y)
-- 
-- isPair (p :: S) = paird p /= Nothing
-- pfst (p :: S) = paird p >>= pure . fst
-- psnd (p :: S) = paird p >>= pure . snd

paird (p :: S) = let
  [a, b] = Set.elems (toSet p)
  (x, b') = if Set.member a (toSet b) then (a, b) else (b, a)
  ys = Set.elems # Set.delete x # toSet b'
  y = if null ys then x else head ys
  in (x, y)

pfst (p :: S) = fst # paird p
psnd (p :: S) = snd # paird p

fnToSet (xs :: [(S, S)]) = let
  ps = map (uncurry pair) xs
  in FromSet (Set.fromList ps)

fnPairs :: [S] -> (S -> [S]) -> [[(S, S)]]
fnPairs [] f = [[]]
fnPairs (x:xs) f = do
  a <- fnPairs xs f
  y <- f x
  return # (x, y) : a

pi (a :: S) (f :: S -> S) = let
  xs = Set.elems (toSet a)
  f' (x :: S) = Set.elems # toSet # f x
  ps = map fnToSet (fnPairs xs f')
  in FromSet (Set.fromList ps)

pi' (a :: S) (b :: S) = pi a (const b)

fn (a :: S) (f :: S -> S) = let
  xs = Set.elems (toSet a)
  ys = map f xs
  in fnToSet (zip xs ys)

fn2 (a :: S) (b :: S) (f :: S -> S -> S) = fn a # \x -> fn b (f x)

call :: S -> S -> S
call (f :: S) (a :: S) = let
  ps = map paird (Set.toList (toSet f))
  in fromJust # lookup a ps
  -- ps = map paird (Set.toList (toSet f))
  -- filtered = flip filter ps # \p -> case p of
  --   Nothing -> False
  --   Just (x, y) -> x == a
  -- in if length filtered /= 1 then sorry else
  --   snd # fromJust # head filtered

infixl 9 @
(@) = call

-----

spec (a :: S) (f :: S -> S) = lift (Set.filter (\x -> P.not # Set.null # toSet # f x)) a

-----

emp = empty
false = emp
true = sngl false
prop = upair false true
is_empty (s :: S) = fn s # \a -> pi' a false
not = is_empty prop
nonempty (s :: S) = fn s # \a -> not @ (is_empty s @ a)
nonempty' (s :: S) = nonempty (sngl s) @ s
imp = fn2 prop prop # \a b -> nonempty' (pi' a b)
or = fn2 prop prop # \a b -> imp @ (not @ a) @ b
and = fn2 prop prop # \a b -> not @ (or @ (not @ a) @ (not @ b))
iff = fn2 prop prop # \a b -> and @ (imp @ a @ b) @ (imp @ b @ a)
xor = fn2 prop prop # \a b -> not @ (iff @ a @ b)
id (s :: S) = fn s # \a -> a

-- nat = sorry :: S
-- zero = emp
-- succ = fn nat # \a -> upair a (sngl a)
-- 
-- instance Num S where
--   a + b = sorry
--   a - b = sorry
--   a * b = sorry
--   abs a = sorry
--   signum a = sorry
--   fromInteger n = sorry

forall (s :: S) = fn (pi' s prop) # \p -> nonempty' (pi s # \x -> p @ x)
exists (s :: S) = fn (pi' s prop) # \p -> not @ (forall s @ (fn s # \x -> not @ (p @ x)))

surj_prop (s :: S) = exists (pi' s prop) @
  (fn (pi' s prop) # \f -> and @
    (exists s @ (fn s # \x -> f @ x)) @
    (exists s @ (fn s # \x -> not @ (f @ x))))

eq (s :: S) = fn2 s s # \a b -> not @ surj_prop (upair a b)
neq (s :: S) = fn2 s s # \a b -> not @ (eq s @ a @ b)

union (s :: S) = fn2 s s # \a b -> unions (upair a b)
remove (s :: S) = fn2 s s # \a b -> spec b # \x -> neq (upair x a) @ x @ a
mem (s :: S) = fn2 s s # \a b -> neq s @ (remove s @ a @ b) @ b
inter (s :: S) = fn2 s s # \a b -> spec a # \x -> mem (upair x b) @ x @ b

main :: IO ()
main = do
  print # mem prop