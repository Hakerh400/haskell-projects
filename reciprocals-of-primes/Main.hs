import Base

num :: N
num = 61561

main :: IO ()
main = print # f num

f :: N -> N
f n = nat_find # \k -> k /= 0 && mod (10 ^ k - 1) n == 0