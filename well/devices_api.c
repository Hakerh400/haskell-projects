#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <windows.h>

#define log(a) log1(a)

void log(char* str){
  printf("%s", str);
  fflush(stdout);
}

void sleep(WORD ms){
  Sleep(ms);
}

void kb_key(bool press, WORD key){
  INPUT* input = calloc(sizeof(INPUT), 1);
  
  input->type = INPUT_KEYBOARD;
  input->ki.wVk = key;
  if(!press) input->ki.dwFlags = KEYEVENTF_KEYUP;
  
  SendInput(1, input, sizeof(INPUT));
  free(input);
}