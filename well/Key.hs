module Key where

import qualified Prelude

type Int = Prelude.Int

esc = 0x1B :: Int
space = 0x20 :: Int
left = 0x25 :: Int
up = 0x26 :: Int
right = 0x27 :: Int
down = 0x28 :: Int
letter_w = 0x57 :: Int