module Devices
  ( module DevicesAPI
  , module Devices
  ) where

import Util
import DevicesAPI
import qualified Key

sleep' :: Int -> IO ()
sleep' = _api_sleep

sleep :: Int -> IO ()
sleep n = sleep' # n * 1000

kb_key :: Prop -> Int -> IO ()
kb_key = _api_kb_key

kb_press :: Int -> IO ()
kb_press = kb_key True

kb_release :: Int -> IO ()
kb_release = kb_key False

kb_hold :: Int -> Int -> IO ()
kb_hold key ms = do
  kb_press key
  sleep' ms
  kb_release key

kb_click :: Int -> IO ()
kb_click key = kb_hold key 100