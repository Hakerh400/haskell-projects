import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Monad

import Util
import Bit
import Devices
import qualified Key

stairs_num = 6

restart_cd = 500
enter_well_cd = 300
move_dur = 170
move_cd = move_dur * 2
jump_dur = 150
jump_cd = 1000
jump_over_cd = 350
move_jump_dur = 250
move_jump_cd = move_cd
move_w_dur = 250
move_w_cd = move_cd
move_turn_dur = 50
move_turn_cd = 0
release_cd = 1000
enter_cd = 2500
exit_cd = enter_cd

main :: IO ()
main = do
  sleep 1
  restart
  solve

restart :: IO ()
restart = do
  mapM_ kb_click [Key.esc, Key.down, Key.space]
  sleep' restart_cd

solve :: IO ()
solve = do
  bring_chest_into_well
  init_stone
  
  let n = 6
  mk_jar_a n
  move_w_right 1
  release_and_enter
  lift_stone_to n

insert_into_fresh :: IO ()
insert_into_fresh = do
  enter
  goto_jar_place 1
  move_right 1
  release
  move_left 2
  exit

insert_jar :: IO ()
insert_jar = do
  enter
  move_right 2
  release_and_pick
  move_left 2
  release
  move_right 1
  exit

insert_empty :: IO ()
insert_empty = do
  move_w_right 1
  release
  move_w_left 1
  mk_jar_a 0
  move_w_right 1
  insert_jar
  move_w_left 1

with_empty :: IO ()
with_empty = do
  insert_into_fresh
  insert_empty

pair_with_jar :: Int -> IO () -> IO ()
pair_with_jar n f = do
  let n1 = n - 1
  move_w_right n
  release
  move_w_left n
  f >> insert_into_fresh
  move_w_right 1
  release
  move_w_right n1
  pick
  move_w_left n1
  insert_jar
  move_w_left 1

mk_jar_a :: Int -> IO ()
mk_jar_a n = case n of
  0 -> do
    enter
    goto_jar_place 0
    exit
  1 -> do
    mk_jar_a 0
    insert_into_fresh
  2 -> do
    mk_jar_a 1
    insert_empty
  3 -> do
    mk_jar_a 2
    replicateM_ 2 with_empty
  4 -> do
    mk_jar_a 3
    with_empty
    pair_with_jar 2 # mk_jar_a 2
    with_empty
  5 -> do
    mk_jar_a 4
    with_empty
    pair_with_jar 3 # mk_jar_a 3
    pair_with_jar 2 # mk_jar_a 2
    with_empty
  6 -> do
    mk_jar_a 5
    with_empty
    pair_with_jar 4 # mk_jar_a 4
    pair_with_jar 3 # mk_jar_a 3
    pair_with_jar 2 # mk_jar_a 2
    with_empty

lift_stone_to :: Int -> IO ()
lift_stone_to n = do
  aux <- pure # do
    move_left 1
    pick
    move_right 5
    jump
    jump_over_right
    move_right 7
    move_left 1
    release
    move_left 7
    jump_over_left
    move_left 5
    lift_stone_to 1
    exit_room
    move_right 7
    jump_right
    release
    move_left 3
    pick
    move_right 1
    jump_right
    turn_left
    jump_left
    move_left 4
    release_and_enter
  case n of
    1 -> do
      move_right 2
      pick
      replicateM_ 2 jump_over_right
      release_and_enter
      move_right 5
      pick
    2 -> do
      aux
      lift_stone_from 1
    3 -> do
      aux
      lift_stone_from 1
      release
      move_left 5
      enter
      lift_stone_to 2
      exit_room
      release
      descend_from 2
      exit_room
      move_right 2
      pick
      exit_room
      jump_over_left
      move_left 4
      release_and_enter
      lift_stone_from 2
    4 -> do
      lift_stone_to 3
      release
      move_left 5
      enter
      lift_stone_to 3
      exit_room
      release
      descend_from 3
      exit_room
      move_right 2
      pick
      exit_room
      jump_over_left
      move_left 4
      release_and_enter
      lift_stone_from 3
    5 -> do
      lift_stone_to 4
      release
      move_left 5
      enter
      lift_stone_to 4
      exit_room
      release
      descend_from 4
      exit_room
      move_right 2
      pick
      exit_room
      jump_left
      release_and_enter
      lift_stone_from 4
    6 -> do
      lift_stone_to 5
      release
      move_left 5
      enter
      lift_stone_to 5
      exit_room
      release

lift_stone_from :: Int -> IO ()
lift_stone_from n = do
  dflt <- pure # do
    ascend_to n
    pick
    descend_from n
  case n of
    1 -> do
      ascend_to 1
      squat
      pick
      descend_from 1
    2 -> dflt
    3 -> dflt
    4 -> dflt

init_stone :: IO ()
init_stone = do
  enter
  ascend_to 3
  jump_over_left
  move_left 1
  pick
  descend_from 4
  move_right 1
  release
  exit_room

ascend_to :: Int -> IO ()
ascend_to n = do
  ifm (n > 0) # ascend_to (n - 1)
  dflt <- pure # do
    jump_over_left
    move_left 2
  case n of
    0 -> move_right 8
    1 -> do
      jump_over_right
      move_right 7
      jump_right
    2 -> do
      turn_left
      jump_over_left
      move_left 3
    3 -> dflt
    4 -> dflt
    5 -> jump_left

descend_from :: Int -> IO ()
descend_from n = ifm (n > 0) # do
  dflt <- pure # do
    move_right 2
    jump_over_right
  case n of
    1 -> do
      move_left 10
      jump_over_left
      move_right 1
    2 -> dflt
    3 -> dflt
    4 -> dflt
    5 -> jump_right
  descend_from (n - 1)

bring_chest_into_well :: IO ()
bring_chest_into_well = do
  move_left 2
  pick
  move_right 4
  jump_over_right
  move_left 1
  sleep' enter_well_cd
  release

move' :: Int -> Int -> Int -> Int -> IO ()
move' dur cd dir n = do
  key <- pure # case dir of
    (-1) -> Key.left
    1 -> Key.right
  kb_hold key # dur * n
  sleep' cd

move :: Int -> Int -> IO ()
move = move' move_dur move_cd

move_jump :: Int -> Int -> IO ()
move_jump = move' move_jump_dur move_jump_cd

move_w :: Int -> Int -> IO ()
move_w = move' move_w_dur move_w_cd

move_turn :: Int -> Int -> IO ()
move_turn = move' move_turn_dur move_turn_cd

move_left :: Int -> IO ()
move_left = move (-1)

move_right :: Int -> IO ()
move_right = move 1

move_jump_left :: Int -> IO ()
move_jump_left = move_jump (-1)

move_jump_right :: Int -> IO ()
move_jump_right = move_jump 1

move_w_left :: Int -> IO ()
move_w_left = move_w (-1)

move_w_right :: Int -> IO ()
move_w_right = move_w 1

turn_left :: IO ()
turn_left = move_turn (-1) 1

turn_right :: IO ()
turn_right = move_turn 1 1

jump' :: Int -> IO ()
jump' dir = do
  kb_click Key.up
  sleep' jump_dur
  case dir of
    0 -> sleep' jump_cd
    -1 -> do
      move_jump_left 1
      sleep' jump_over_cd
    1 -> do
      move_jump_right 1
      sleep' jump_over_cd

jump :: IO ()
jump = do
  jump' 0

jump_left :: IO ()
jump_left = jump' (-1)

jump_right :: IO ()
jump_right = jump' 1

jump_over :: Int -> IO ()
jump_over dir = do
  jump' dir
  -- ifm (dir /= 0) # sleep' jump_cd
  case dir of
    0 -> nop
    -1 -> move_left 1
    1 -> move_right 1

jump_over_left :: IO ()
jump_over_left = jump_over (-1)

jump_over_right :: IO ()
jump_over_right = jump_over 1

pick :: IO ()
pick = kb_click Key.space

release :: IO ()
release = do
  kb_click Key.space
  sleep' release_cd

enter :: IO ()
enter = do
  kb_click Key.up
  sleep' enter_cd

exit :: IO ()
exit = do
  kb_click Key.space
  sleep' exit_cd
  
re_enter :: IO ()
re_enter = do
  exit
  enter

goto_fst_jar_place :: IO ()
goto_fst_jar_place = move_right 4

goto_snd_jar_place :: IO ()
goto_snd_jar_place = move_right 5

goto_jar_place :: Int -> IO ()
goto_jar_place n = case n of
  0 -> goto_fst_jar_place
  1 -> goto_snd_jar_place

mk_jar :: Int -> IO ()
mk_jar n = do
  goto_jar_place n
  exit

exit_room' :: Prop -> IO ()
exit_room' pick_p = do
  move_left 5
  ifm pick_p pick
  replicateM_ 2 jump_left
  exit

exit_room :: IO ()
exit_room = exit_room' False

pick_and_exit_room :: IO ()
pick_and_exit_room = exit_room' True

release_and_enter :: IO ()
release_and_enter = do
  release
  enter

squat :: IO ()
squat = do
  kb_press Key.down
  jump
  kb_release Key.down

release_and_pick :: IO ()
release_and_pick = do
  kb_press Key.letter_w
  kb_click Key.space
  sleep' move_cd
  kb_release Key.letter_w
  pick