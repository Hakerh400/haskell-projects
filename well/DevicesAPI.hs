{-# LANGUAGE ForeignFunctionInterface #-}

module DevicesAPI
  ( _api_sleep
  , _api_kb_key
  ) where

import Util

foreign import ccall sleep :: Int -> IO ()
foreign import ccall kb_key :: Prop -> Int -> IO ()

_api_sleep = sleep
_api_kb_key = kb_key