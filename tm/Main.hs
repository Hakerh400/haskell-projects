import Util
import Bit

import Data.List
import Data.Set (Set)
import Data.Map (Map)

import qualified Data.Set as Set
import qualified Data.Map as Map

type M a s = s -> Maybe a -> Maybe (Maybe a, Bit, s)
type T a = N -> Maybe a
type C a s = (M a s, T a, s, N)

out :: T a -> N -> [a]
out t i = case t i of
  Nothing -> []
  Just a -> a : out t (i + 1)

run' :: C a s -> [a]
run' (m, t, s, i) = case m s (t i) of
  Nothing -> out t 0
  Just (a, d, s) -> let
    t' = \j -> ite (j == i) a # t j
    i' = i + ite (d == 0) (-1) 1
    in run' (m, t', s, i')

run :: M a s -> s -> [a] -> [a]
run m s xs = run' (m, flip list_get xs, s, 0)

prog :: M Char N
prog s a = do
  let str = "Hello, World!"
  let n = len str
  if s < n then Just (list_get s str, 1, s + 1)
  else if s == n then Just (Nothing, 1, s + 1)
  else Nothing

main :: IO ()
main = do
  putStrLn # run prog 0 ""