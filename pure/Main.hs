import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util
import Bit
import Parser

data TypeVal
  = TypeProp
  | TypeSet
  | TypeIdent N
  | TypeFunc Type Type
  | TypeLam Type
  deriving (Eq, Ord, Show)

data TypeCtx = TypeCtx (Set N)
  deriving (Eq, Ord)

show_type_ctx :: TypeCtx -> String
show_type_ctx (TypeCtx c) = show_set show c

instance Show TypeCtx where
  show = show_type_ctx

data Type = Type TypeCtx N TypeVal
  deriving (Eq, Ord, Show)

asrt :: () -> Prop -> a -> a
asrt _ True = id
asrt () False = id

type_prop :: Type
type_prop = Type (TypeCtx Set.empty) 0 TypeProp

type_set :: Type
type_set = Type (TypeCtx Set.empty) 0 TypeSet

type_ident :: N -> Type
type_ident n = Type (TypeCtx # Set.singleton n) 0 #
  TypeIdent n

type_func :: Type -> Type -> Type
type_func a@(Type (TypeCtx c1) 0 v1) b@(Type (TypeCtx c2) 0 v2) =
  Type (TypeCtx # Set.union c1 c2) 0 # TypeFunc a b

type_lam :: Type -> Type
type_lam a@(Type (TypeCtx c) k v) =
  asrt undefined (Set.member 0 c) #
  let c1 = Set.map dec # Set.delete 0 c
  in Type (TypeCtx c1) (k + 1) # TypeLam a

type_app :: Type -> Type -> Type
type_app a@(Type c1 k1 (TypeLam t)) b@(Type c2 0 v2) =
  asrt undefined (k1 /= 0) # subst b 0 t

subst :: Type -> N -> Type -> Type
subst x@(Type _ _ _) k t@(Type (TypeCtx c) _ v) =
  if not # Set.member k c then type_lift (-1) k t
  else case v of
    TypeIdent n -> if n < k then t
      else if n > k then type_ident # n - 1
      else type_lift k 0 x
    TypeFunc t1 t2 -> type_func (subst x k t1) (subst x k t2)
    TypeLam t1 -> type_lam # subst x (k + 1) t1

type_lift :: N -> N -> Type -> Type
type_lift n k t@(Type (TypeCtx c) _ v) =
  if Set.null c then t else case v of
    TypeIdent m -> if m < k then t else
      let x = m + n
      in asrt (error # show (n, k, t)) (x >= 0) # type_ident x
    TypeFunc t1 t2 -> type_func (type_lift n k t1) (type_lift n k t2)
    TypeLam t1 -> type_lam # type_lift n (k + 1) t1

data TermVal
  = TermImp
  | TermUniv
  | TermIdent Type N
  | TermLam Type Term
  | TermApp Term Term
  | TermTypeLam Term
  | TermTypeApp Term Type
  deriving (Eq, Ord, Show)

data TermCtx = TermCtx (Map N Type)
  deriving (Eq, Ord)

show_term_ctx :: TermCtx -> String
show_term_ctx (TermCtx c) = show_set <~ map_to_set c #
  \(n, t) -> concat [show n, " -> ", show t]

instance Show TermCtx where
  show = show_term_ctx

data Term = Term TypeCtx TermCtx Type TermVal
  deriving (Eq, Ord, Show)

main :: IO ()
main = do
  a <- pure #
    type_lam # type_lam # type_app
      (type_lam # type_lam # type_func (type_ident 1) (type_ident 0))
      (type_func (type_ident 1) (type_ident 0))
  mapM_ print [
      a
    ]