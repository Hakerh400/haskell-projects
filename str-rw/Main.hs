import Util
import Bit

import Data.List
import Data.Set (Set)
import Data.Map (Map)

import qualified Data.Set as Set
import qualified Data.Map as Map

type Rule = (Bits, Bits)
type Prog = [Rule]

run_aux' :: Prog -> Bits -> (Bits, Prop)
run_aux' prog inp = case findIndex (\(a, _) -> starts_with a inp) prog of
  Just i' -> let
    i = fi i'
    (a, b) = list_get' i prog
    out = b ++ drop (fi # len a) inp
    in (out, i /= len prog - 1)
  Nothing -> case inp of
    [] -> ([], False)
    (b:inp) -> let
      (out, p) = run_aux' prog inp
      in (b:out, p)

run_aux :: Prog -> Bits -> [Bits]
run_aux prog inp = inp : let
  (out, p) = run_aux' prog inp
  in if p then run_aux prog out else
    if out /= inp then [out] else []

run :: Prog -> Bits -> [Bits]
run prog inp = inp : (run_aux prog # [0, 0, 0] ++ inp)

parse_pat :: String -> Bits
parse_pat s = case s of
  ('0':s) -> 0 : parse_pat s
  ('1':s) -> 1 : parse_pat s
  _ -> []

parse_rule :: String -> Rule
parse_rule s = let
  [a, _, b] = words s
  in (parse_pat a, parse_pat b)

main :: IO ()
main = do
  src <- readFile "src.txt"
  inp <- readFile "inp.txt"
  let prog = map parse_rule # lines src
  mapM_ print # run prog # parse_pat inp