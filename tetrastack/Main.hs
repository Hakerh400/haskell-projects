import Data.Char
import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util
import Bit

src_file = "src.txt"
inp_file = "inp.txt"

inp_stack = 0
out_stack = 3

flag_show_out = p 1
flag_show_stacks = p 1
flag_show_insts = p 0

p n = n == 1

type Msg = [String]
type Stack = [N]
type Inst = (N, N)
type Prog = StateT ProgT (Except Msg)

data ProgT = ProgT
  { _prog_stacks :: [Stack]
  , _prog_insts :: [Inst]
  , _prog_ip :: N
  } deriving (Eq, Ord, Show)

main :: IO ()
main = do
  src <- readFile src_file
  inp <- readFile inp_file
  case run_prog src inp of
    Left msg -> putStrLn # concat msg
    Right stacks -> do
      ifm flag_show_stacks # do
        putStrLn # show_stacks stacks
        ifm flag_show_out logb
      ifm flag_show_out # do
        out <- pure # map (chr . fi) # list_get out_stack stacks
        putStrLn out

run_prog :: String -> String -> Either Msg [Stack]
run_prog src inp = do
  insts <- pure # src >>= \c -> do
    c <- pure # toLower c
    True <- pure # is_digit c || (c >= 'a' && c <= 'f')
    let n = read # snoc "0x" c
    return # divMod n 4
  inp <- pure # reverse # (-1) : map (fi . ord) inp
  prog <- runExcept # do
    ifm flag_show_insts # do
      err # pure # unlines' # flip map insts # \inst ->
        concat # map show [fst inst, snd inst]
    execStateT run_prog' # init_prog insts inp
  return # _prog_stacks prog

run_prog' :: Prog ()
run_prog' = do
  insts <- gets _prog_insts
  ip <- gets _prog_ip
  ifm (ip /= len insts) # do
    let inst = list_get ip insts
    exec_inst inst
    run_prog'

exec_inst :: Inst -> Prog ()
exec_inst inst@(from, to) = do
  -- stacks <- gets _prog_stacks
  -- tracem # Lit
  --   [ show_stacks stacks, "\n"
  --   , show # fst inst, " ", show # snd inst, "\n"
  --   ]
  case inst of
    (3, 3) -> do
      push 2 0
      inc_ip
    (3, _) -> do
      n <- pop 3
      start <- gets _prog_ip
      end <- find_loop_end start
      if n /= 0 then push to n >> inc_ip
        else set_ip # end + 1
    (2, 2) -> do
      end <- gets _prog_ip
      start <- find_loop_start end
      set_ip start
    _ -> do
      push_pop from to
      inc_ip

find_loop_end :: N -> Prog N
find_loop_end ip = find_loop 1 1 # ip + 1

find_loop_start :: N -> Prog N
find_loop_start ip = find_loop (-1) 1 # ip - 1

find_loop :: N -> N -> N -> Prog N
find_loop dir nest ip = if nest == 0
  then pure # ip - dir else do
    insts <- gets _prog_insts
    ifm (ip == -1) # err ["Missing loop start"]
    ifm (ip == len insts) # err ["Missing loop end"]
    let inst = list_get ip insts
    dflt <- pure # find_loop dir nest (ip + dir)
    case inst of
      (3, 3) -> dflt
      (3, _) -> find_loop dir (nest + dir) (ip + dir)
      (2, 2) -> find_loop dir (nest - dir) (ip + dir)
      _ -> dflt

inc_ip :: Prog ()
inc_ip = modify # \prog -> prog
  {_prog_ip = _prog_ip prog + 1}

set_ip :: N -> Prog ()
set_ip ip = modify # \prog -> prog {_prog_ip = ip}

push_pop :: N -> N -> Prog ()
push_pop from to = pop from >>= push to

push :: N -> N -> Prog ()
push i n = modify # \prog -> prog
  {_prog_stacks = modify_at i (n:) # _prog_stacks prog}

pop_raw :: N -> Prog N
pop_raw i = do
  stack <- gets # list_get i . _prog_stacks
  case stack of
    [] -> err ["Pop from an empty stack"]
    (n : stack) -> do
      modify # \prog -> prog
        {_prog_stacks = set_at i stack # _prog_stacks prog}
      return n

pop :: N -> Prog N
pop i = do
  n <- pop_raw i
  return # case i of
    0 -> n + 1
    1 -> n - 1
    _ -> n

init_prog :: [Inst] -> Stack -> ProgT
init_prog insts inp = ProgT
  { _prog_stacks = init_stacks inp
  , _prog_insts = insts
  , _prog_ip = 0
  }

init_stacks :: Stack -> [Stack]
init_stacks inp = set_at inp_stack inp # replicate 4 []

show_stacks :: [Stack] -> String
show_stacks stacks = unlines' # zipWith show_stack [0..] stacks

show_stack :: N -> Stack -> String
show_stack i stack = concat
  ["Stack ", show i, ": ", show_list show stack]