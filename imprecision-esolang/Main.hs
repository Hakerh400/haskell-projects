import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Ratio

import Util
import Parser

src_file = "src.txt"
inp_file = "inp.txt"

inp_var_name = "input"
out_var_name = "output"
halt_var_name = "halt"

data ProgT = ProgT
  { _prog_cmds :: [Cmd]
  , _prog_ip :: N
  , _prog_vars :: Map Name Rational
  } deriving (Eq, Ord, Show)

type ProgP = ParserM ProgT
type Prog = StateT ProgT (Except Sctr)

data Cmd = Cmd Name Expr
  deriving (Eq, Ord, Show)

data Expr
  = ExprInt N
  | ExprVar Name
  | ExprAdd Expr Expr
  | ExprSub Expr Expr
  | ExprMul Expr Expr
  | ExprDiv Expr Expr
  deriving (Eq, Ord, Show)

main :: IO ()
main = do
  src <- readFile src_file
  inp <- readFile inp_file
  case parse_and_run_prog src inp of
    Left msg -> putStrLn # concat msg
    Right res -> print res

init_prog :: [Cmd] -> N -> ProgT
init_prog cmds inp = ProgT
  { _prog_cmds = cmds
  , _prog_ip = 0
  , _prog_vars = Map.singleton inp_var_name # inp % 1
  }

parse_expr_int :: ProgP Expr
parse_expr_int = do
  n <- p_int
  return # ExprInt n

parse_expr_var :: ProgP Expr
parse_expr_var = do
  True <- p_query # pure True ++> do
    name <- p_ident
    p_tok "="
    return False
  name <- p_ident
  return # ExprVar name

parse_expr_group :: ProgP Expr
parse_expr_group = p_parens parse_expr

parse_expr_1 :: ProgP Expr
parse_expr_1 = choice
  [ parse_expr_int
  , parse_expr_var
  , parse_expr_group
  ]

parse_expr_aux :: ProgP Expr -> String ->
  [Expr -> Expr -> Expr] -> ProgP Expr
parse_expr_aux p cs fs = do
  (x : xs, ys) <- p_sep p # choice # do
    (c, f) <- zip cs fs
    [p_tok [c] >> pure f]
  return # fold_l (zip xs ys) x #
    \a (b, f) -> f a b

parse_expr' :: ProgP Expr
parse_expr' = parse_expr_aux parse_expr_1 "*/" [ExprMul, ExprDiv]

parse_expr :: ProgP Expr
parse_expr = parse_expr_aux parse_expr' "+-" [ExprAdd, ExprSub]

parse_cmd :: ProgP Cmd
parse_cmd = do
  name <- p_ident
  p_tok "="
  e <- parse_expr
  return # Cmd name e

parse_prog :: N -> ProgP ProgT
parse_prog inp = do
  cmds <- trimmed # p_all' parse_cmd
  return # init_prog cmds inp

eval_op :: (Rational -> Rational -> Rational) ->
  Expr -> Expr -> Prog Rational
eval_op op a b = do
  a <- eval_expr a
  b <- eval_expr b
  return # op a b

eval_expr :: Expr -> Prog Rational
eval_expr e = case e of
  ExprInt n -> pure # n % 1
  ExprVar var -> get_var var
  ExprAdd a b -> eval_op (+) a b
  ExprSub a b -> eval_op (-) a b
  ExprMul a b -> eval_op (*) a b
  ExprDiv a b -> eval_op (/) a b

get_var :: Name -> Prog Rational
get_var var = do
  vars <- gets _prog_vars
  return # Map.findWithDefault 0 var vars

set_var :: Name -> Rational -> Prog ()
set_var var val = modify # \prog -> prog
  {_prog_vars = map_insert var val # _prog_vars prog}

run_cmd :: Cmd -> Prog ()
run_cmd (Cmd var e) = do
  val <- eval_expr e
  set_var var val

run_prog :: Prog N
run_prog = do
  prog <- get
  let cmds = _prog_cmds prog
  let ip = _prog_ip prog
  let Just cmd = list_get' ip cmds
  run_cmd cmd
  halt_val <- get_var halt_var_name
  if halt_val > 0 then get_result else do
    modify # \prog -> prog
      {_prog_ip = mod (ip + 1) # len cmds}
    run_prog

get_result :: Prog N
get_result = do
  val <- get_var out_var_name
  return # round val

parse_and_run_prog :: String -> String -> Either Sctr N
parse_and_run_prog src inp = do
  inp <- parse p_int inp
  prog <- parse (parse_prog inp) src
  runExcept # evalStateT run_prog prog