module Proof
  ( Proof
  , ax_1
  , ax_2
  , mp
  ) where

import Util
import Struct
import Prop

newtype Proof = Proof S
  deriving (Eq, Ord, Show)

instance Rec Proof where
  rec' (Proof val) f = f val

ax_1 :: S -> S -> PropT -> PropT -> Proof
ax_1 a b p1 p2 = rec' p1 # \s1 -> guard (s1 == a) #
  rec' p2 # \s2 -> guard (s2 == b) #
  Proof # P a (P b a)

ax_2 :: S -> S -> S -> PropT -> PropT -> PropT -> Proof
ax_2 a b c p1 p2 p3 = rec' p1 # \s1 -> guard (s1 == a) #
  rec' p2 # \s2 -> guard (s2 == b) #
  rec' p3 # \s3 -> guard (s3 == c) #
  Proof # P (P a # P b c) # P (P a b) # P a c

mp :: S -> S -> Proof -> Proof -> Proof
mp a b p1 p2 = rec' p1 # \s1 -> guard (s1 == P a b) #
  rec' p2 # \s2 -> guard (s2 == a) # Proof b