import Util
import Struct
import Prop
import Proof

mk_prop :: S -> PropT
mk_prop s = case s of
  T "P" -> mk_var
  P a b -> mk_imp a b (mk_prop a) (mk_prop b)

ax_1' :: S -> S -> Proof
ax_1' a b = ax_1 a b (mk_prop a) (mk_prop b)

ax_2' :: S -> S -> S -> Proof
ax_2' a b c = ax_2 a b c (mk_prop a) (mk_prop b) (mk_prop c)

mp' :: Proof -> Proof -> Proof
mp' p1 p2 = rec' p1 # \(P a b) -> mp a b p1 p2

main :: IO ()
main = do
  let p = T "P"
  let pp = P p p
  let pf_1 = ax_1' p p
  let pf_2 = ax_1' p pp
  let pf_3 = ax_2' p pp p
  let pf_4 = mp' pf_3 pf_2
  let pf_5 = mp' pf_4 pf_1
  print pf_5