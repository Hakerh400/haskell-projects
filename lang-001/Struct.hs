module Struct where

import Util

data S
  = T String
  | P S S
  deriving (Eq, Ord)

show_struct :: S -> String
show_struct s = case s of
  T str -> str
  P a b -> put_in_parens #
    concat [show a, " ", show b]

instance Show S where
  show = show_struct

class Rec t where
  rec' :: t -> (S -> a) -> a

guard :: Prop -> a -> a
guard True = id