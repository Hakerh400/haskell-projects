module Prop
  ( PropT
  , mk_var
  , mk_imp
  ) where

import Util
import Struct

newtype PropT = PropT S
  deriving (Eq, Ord, Show)

instance Rec PropT where
  rec' (PropT val) f = f val

mk_var :: PropT
mk_var = PropT # T "P"

mk_imp :: S -> S -> PropT -> PropT -> PropT
mk_imp a b p1 p2 = rec' p1 # \s1 -> guard (s1 == a) #
  rec' p2 # \s2 -> guard (s2 == b) # PropT # P a b