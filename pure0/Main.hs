import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)

import Util
import Bit
import Bimap
import Theory
import Elab
import Expr

main :: IO ()
main = run_elab # do
  let _a = '<'
  let _b = '>'
  let _c = '#'
  
  n <- asgn "NIL" # mk' nil
  n1 <- asgn "n1" # mk' n
  n2 <- asgn "n2" # mk' n1 n
  n3 <- asgn "n3" # mk' n2 n
  n4 <- asgn "n4" # mk' n3 n
  rc <- asgn "REC" # mk' n2
  s <- asgn "S" # mk' n n2
  k <- asgn "K" # mk' n n3
  proof <- asgn "⊢" # mk' n n4
  
  [_0, _1, _2, _3, _4, _5, _6, _7, _8, _9] <-
    mapM <~ ico 10 # \i -> do
      let s = show i
      let s' = show # i - 1
      case i of
        0 -> asgn s # mk' proof proof
        _ -> do
          e <- get_expr s'
          asgn s # mk' e n
  
  i <- asgn "I" # mk' s k k
  b <- asgn "B" # mk' s _a k s _b k
  c <- asgn "C" # mk' s _a b b s _b _a k k _b
  true <- asgn "T" # mk' n2
  false <- asgn "F" # mk' n
  sii <- asgn "SII" # mk' s i i
  w <- asgn "W" # mk' c s i
  
  f <- mk # mk'
    sii _c c _c rc _a k true _b _c b c _c c _a b c _c
    b _a b rc _b _c c sii _b _c k _c k false
  t <- asgn "Test" # mk' s _c k f
  
  e <- mk # mk' proof _c t _c k _c n4
  
  print_expr e

show_expr_aux' :: Prop -> Expr -> Elab String
show_expr_aux' parens e = do
  res <- lift # cs e
  case res of
    Nothing -> pure "."
    Just (a, b) -> do
      sa <- show_expr_aux False a
      (sa, h) <- pure # case sa of
        "⊢" -> (snoc sa space, False)
        _ -> (sa, True)
      sb <- show_expr_aux h b
      return # put_in_parens' parens # unwords [sa, sb]

show_expr_aux :: Prop -> Expr -> Elab String
show_expr_aux parens e = get_name' e >>= \r -> case r of
  Nothing -> show_expr_aux' parens e
  Just name -> pure name

show_expr :: Expr -> Elab String
show_expr = show_expr_aux False

show_expr' :: Expr -> Elab String
show_expr' = show_expr_aux' False

print_expr :: Expr -> Elab ()
print_expr e = show_expr e >>= putStrLn

print_expr' :: Expr -> Elab ()
print_expr' e = show_expr' e >>= putStrLn