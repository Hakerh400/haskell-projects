module Expr
  ( mk'
  , mk
  , asgn
  ) where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)

import Util
import Bimap
import Theory
import Elab

type MkArg = Either Char Expr
type MkArgs = [MkArg]

class MkC a where
  mk_fn :: (MkArgs -> Elab Expr) -> a

instance MkC (Elab Expr) where
  mk_fn f = f []

instance (MkC a) => MkC (Expr -> a) where
  mk_fn f a = mk_fn # \xs -> f # Right a : xs

instance (MkC a) => MkC (Char -> a) where
  mk_fn f c = mk_fn # \xs -> f # Left c : xs

type Mk = StateT MkArgs Elab

mk_get' :: Prop -> Mk (Maybe MkArg)
mk_get' update = do
  args <- get
  case args of
    [] -> return Nothing
    (arg : args) -> do
      ifm update # put args
      return # Just arg

mk_get :: Prop -> Mk MkArg
mk_get update = do
  Just a <- mk_get' update
  return a

mk_exact :: MkArg -> Mk ()
mk_exact arg = do
  arg' <- mk_get True
  asrt # arg == arg'

mk_eof :: Mk Prop
mk_eof = do
  args <- get
  return # null args

mk_eval' :: Maybe Expr -> Mk Expr
mk_eval' ma = do
  arg <- mk_get' False
  mb <- case arg of
    Nothing -> pure Nothing
    Just arg -> case arg of
      Right b -> do
        mk_get' True
        return # Just b
      Left c -> case c of
        '<' -> do
          mk_get' True
          b <- mk_eval' Nothing
          mk_exact # Left '>'
          return # Just b
        '>' -> pure Nothing
        '#' -> do
          mk_get' True
          fmap Just # mk_eval' Nothing
  case (ma, mb) of
    (Just a, Nothing) -> pure a
    (Nothing, Just b) -> mk_eval' # Just b
    (Just a, Just b) -> do
      c <- lift2 # pair a b
      mk_eval' # Just c
    _ -> error ""

mk_eval :: Mk Expr
mk_eval = mk_eval' Nothing

mk_aux :: MkArgs -> Elab Expr
mk_aux = evalStateT mk_eval

mk' :: (MkC a) => a
mk' = mk_fn mk_aux

mk :: Elab Expr -> Elab Expr
mk = id

asgn :: Name -> Elab Expr -> Elab Expr
asgn name f = do
  e <- f
  modify # \elab -> elab
    {_elab_mp_name_e = bimap_insert name e # _elab_mp_name_e elab}
  return e