module Theory
  ( Theory
  , Expr
  , run_th
  , nil
  , pair
  , cs
  ) where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)
import Data.Bifunctor

import Util
import Bimap

type E = Int

newtype Expr = Expr E
  deriving (Eq, Ord, Show)

data TheoryT = TheoryT
  { _th_next_id :: E
  , _th_mp_e_ref :: Bimap E E
  , _th_mp_pair_e :: Bimap (E, E) E
  }

type S = StateT TheoryT IO

newtype Theory a = Theory (S a)
  deriving
    ( Functor, Applicative, Monad
    , MonadState TheoryT, MonadIO
    , MonadFail
    )

ref_mp :: Bimap (E, E) E
ref_mp = bimap_from_list
  [ ((0, 0), 1) -- ..
  , ((1, 0), 2) -- ...
  , ((2, 0), 3) -- ....
  , ((0, 1), 4) -- .(..)
  , ((0, 2), 5) -- .(...)
  , ((0, 3), 6) -- .(....)
  ]

init_th :: TheoryT
init_th = TheoryT
  { _th_next_id = 1
  , _th_mp_e_ref = bimap_singleton 0 0
  , _th_mp_pair_e = bimap_empty
  }

run_th :: Theory a -> IO a
run_th (Theory m) = evalStateT m init_th

next_id :: S E
next_id = do
  i <- gets _th_next_id
  modify # \th -> th {_th_next_id = i + 1}
  return i

e_to_ref' :: E -> S (Maybe E)
e_to_ref' a = gets # bimap_get a . _th_mp_e_ref

e_to_ref :: E -> S E
e_to_ref = from_just_m . e_to_ref'

ref_to_e' :: E -> S (Maybe E)
ref_to_e' a = gets # bimap_get' a . _th_mp_e_ref

ref_to_e :: E -> S E
ref_to_e = from_just_m . ref_to_e'

ref_pair' :: E -> E -> S (Maybe E)
ref_pair' a b = pure # bimap_get (a, b) ref_mp

ref_pair :: E -> E -> S E
ref_pair a b = from_just_m # ref_pair' a b

new_pair :: E -> E -> S E
new_pair a b = do
  c <- next_id
  modify # \th -> th
    {_th_mp_pair_e = bimap_insert (a, b) c # _th_mp_pair_e th}
  
  ra <- e_to_ref' a
  rb <- e_to_ref' b
  case (ra, rb) of
    (Just ra, Just rb) -> do
      rc <- ref_pair' ra rb
      case rc of
        Nothing -> nop
        Just rc -> modify # \th -> th
          {_th_mp_e_ref = bimap_insert c rc # _th_mp_e_ref th}
    _ -> nop

  return c

cs' :: E -> S (Maybe (E, E))
cs' e = gets # bimap_get' e . _th_mp_pair_e

--          ⊢  ..ab.    → a
--          ⊢  ..ab(cd) → bcd
--          ⊢  .(..)abc → ac(bc)
--          ⊢  .(...)ab → a
--  a. → .  ⊢  .(....)a → .

reduce :: E -> E -> S E
reduce a b = do
  dflt <- pure # new_pair a b
  e_to_ref' a >>= \r -> case r of
    Just 6 -> pair' b 0 >>= \r -> case r of
      0 -> pure 0
      _ -> dflt
    _ -> cs' a >>= \r -> case r of
      Nothing -> dflt
      Just (a, c) -> e_to_ref' a >>= \r -> case r of
        Just 5 -> pure c
        _ -> cs' a >>= \r -> case r of
          Nothing -> dflt
          Just (a, d) -> e_to_ref' a >>= \r -> case r of
            Just 1 -> cs' b >>= \r -> case r of
              Nothing -> pure d
              Just (x, y) -> do
                r <- pair' c x
                pair' r y
            Just 4 -> do
              r <- pair' d b
              r1 <- pair' c b
              pair' r r1
            _ -> dflt

pair' :: E -> E -> S E
pair' a b = do
  res <- gets # bimap_get (a, b) . _th_mp_pair_e
  case res of
    Just c -> pure c
    Nothing -> reduce a b

nil :: Expr
nil = Expr 0

pair :: Expr -> Expr -> Theory Expr
pair (Expr a) (Expr b) = Theory # fmap Expr # pair' a b

cs :: Expr -> Theory (Maybe (Expr, Expr))
cs (Expr e) = Theory # (fmap # fmap # bimap Expr Expr) # cs' e