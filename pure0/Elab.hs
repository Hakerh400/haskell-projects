module Elab where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)

import Util
import Bimap
import Theory

data ElabT = ElabT
  { _elab_mp_name_e :: Bimap Name Expr
  }

type Elab = StateT ElabT Theory

elab_init :: ElabT
elab_init = ElabT
  { _elab_mp_name_e = bimap_empty
  }

run_elab :: Elab a -> IO a
run_elab m = run_th # evalStateT m elab_init

get_expr' :: Name -> Elab (Maybe Expr)
get_expr' name = gets # bimap_get name . _elab_mp_name_e

get_expr :: Name -> Elab Expr
get_expr = from_just_m . get_expr'

get_name' :: Expr -> Elab (Maybe Name)
get_name' a = gets # bimap_get' a . _elab_mp_name_e

get_name :: Expr -> Elab Name
get_name = from_just_m . get_name'