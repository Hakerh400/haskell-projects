import qualified Data.Set as Set
import qualified Data.Map as Map

import Util

src_file = "src.txt"

nop_char = ' '
nnp_char = 'G'

data ProgT = ProgT
  { _prog_size :: (N, N)
  , _prog_grid :: Set (N, N)
  , _prog_ip :: (N, N)
  , _prog_dir :: N
  } deriving (Eq, Ord, Show)

type Prog = StateT ProgT (Except Sctr)
type Result = ((N, N), N)

main :: IO ()
main = do
  src <- readFile src_file
  case parse_and_run_prog src of
    Left msg -> putStrLn # concat msg
    Right res -> putStrLn # show_res res

show_dir :: N -> String
show_dir = list_get <~ ["up", "right", "down", "left"]

show_res :: Result -> String
show_res ((x, y), dir) = concat
  [ "x: ", show x, "\n"
  , "y: ", show y, "\n"
  , "dir: ", show_dir dir
  ]

init_prog :: (N, N) -> Set (N, N) -> ProgT
init_prog size grid = ProgT
  { _prog_size = size
  , _prog_grid = grid
  , _prog_ip = (0, 0)
  , _prog_dir = 3
  }

dir_to_dif :: N -> (N, N)
dir_to_dif = list_get <~
  [(0, -1), (1, 0), (0, 1), (-1, 0)]

adv' :: N -> N -> (N, N) -> (N, N)
adv' n dir (x, y) = let (dx, dy) = dir_to_dif dir
  in (x + dx * n, y + dy * n)

adv :: N -> (N, N) -> (N, N)
adv = adv' 1

is_nnp' :: (N, N) -> Prog Prop
is_nnp' (x, y) = do
  (w, h) <- gets _prog_size
  grid <- gets _prog_grid
  return # x < 0 || y < 0 ||
    Set.member (mod x w, mod y h) grid

is_nnp :: Prog Prop
is_nnp = gets _prog_ip >>= is_nnp'

is_nnp_dist :: N -> Prog Prop
is_nnp_dist n = do
  ip <- gets _prog_ip
  dir <- gets _prog_dir
  is_nnp' # adv' n dir ip

turn_left :: Prog ()
turn_left = modify # \p -> p
  {_prog_dir = mod (_prog_dir p + 7) 4}

move :: Prog ()
move = modify # \p -> p
  {_prog_ip = adv (_prog_dir p) # _prog_ip p}

run_prog :: Prog Result
run_prog = do
  (w, h) <- gets _prog_size
  dir <- gets _prog_dir
  res <- mapM is_nnp_dist [0 .. max w h]
  cnd <- pure # (dir == 1 || dir == 2) && not (or res)
  if cnd then get_result else do
    nnp <- is_nnp
    ifm nnp turn_left
    move
    run_prog

get_result :: Prog Result
get_result = gets # \p -> (_prog_ip p, _prog_dir p)

parse_prog :: String -> Prog ()
parse_prog src = do
  let valid_chars = ['\r', '\n', nop_char, nnp_char]
  case find <~ src # \a -> not # elem a valid_chars of
    Just c -> err ["Invalid character: ", show c]
    Nothing -> nop
  let xs = map (map (== nnp_char)) # lines src
  let h = len xs
  ifm (h == 0) # err ["Grid height must be nonzero"]
  let line = head xs
  let w = len line
  ifm (w == 0) # err ["Grid width must be nonzero"]
  ifm (any ((/= w) . len) xs) #
    err ["All lines must have the same length"]
  ifm (or line) # err ["First line must consist of NOPs"]
  grid <- pure # Set.fromList # do
    y <- ico h
    x <- ico w
    True <- pure # list_get x # list_get y xs
    return (x, y)
  put # init_prog (w, h) grid

parse_and_run_prog' :: String -> Prog Result
parse_and_run_prog' src = parse_prog src >> run_prog

parse_and_run_prog :: String -> Either Sctr Result
parse_and_run_prog src = runExcept #
  evalStateT (parse_and_run_prog' src) und