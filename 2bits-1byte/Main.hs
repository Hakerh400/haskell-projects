import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Data.Bits
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util
import Bit

src_file = "src.txt"

flag_test = p 0

p n = n == 1

type Sctr = [String]
type Mem = [N]

data ProgT = ProgT
  { _prog_mem :: Mem
  , _prog_ip :: N
  , _prog_hist :: Set (Mem, N)
  , _prog_done :: Prop
  } deriving (Eq, Ord, Show)

type Prog = State ProgT
type Result = Maybe String

act_list = [3, 2, 0, 1]

main :: IO ()
main = if flag_test then test else do
  src <- readFile src_file
  case parse_and_run_prog src of
    Left msg -> putStrLn # concat msg
    Right res -> putStrLn # show_res res

test :: IO ()
test = mapM_ <~ [0..255] # \n -> do
  let c = chr # fi n
  let Right res = parse_and_run_prog [c]
  let s1 = pad_end 4 space # show_char c
  s2 <- pure # case res of
    Nothing -> show_res res
    Just [c] -> show_char c
  putStrLn # concat [s1, " → ", s2]

show_char :: Char -> String
show_char c = if c >= '!' && c <= '~' then [c] else
  ("\\x" ++) # pad_start 2 '0' # map toUpper # nat_to_hex # fi # ord c

show_res :: Result -> String
show_res = fromMaybe "Does not terminate"

parse_prog :: String -> Either Sctr Mem
parse_prog src = case src of
  [c] -> let n = fi # ord c
    in if n >= 256 then Left ["Non-ASCII character"] else
      pure # reverse # mk_list 4 # \i -> shiftR n (fi i * 2) .&. 3
  _ -> Left ["Source code must be exactly one byte long"]

init_prog :: Mem -> ProgT
init_prog mem = ProgT
  { _prog_mem = mem
  , _prog_ip = 0
  , _prog_hist = Set.empty
  , _prog_done = False
  }

get_word :: Prog N
get_word = do
  prog <- get
  let ip = _prog_ip prog
  let w = list_get ip # _prog_mem prog
  put # prog {_prog_ip = (ip + 1) .&. 3}
  return w

run_inst :: Prog ()
run_inst = do
  w <- get_word
  case w of
    0 -> nop
    1 -> do
      addr <- get_word
      prog <- get
      let mem = _prog_mem prog
      let val = list_get addr mem
      val <- pure # list_get val act_list
      put # prog {_prog_mem = set_at addr val mem}
    2 -> do
      ip <- get_word
      modify # \p -> p {_prog_ip = ip}
    3 -> modify # \p -> p {_prog_done = True}

run_prog' :: Prog Result
run_prog' = do
  run_inst
  prog <- get
  if _prog_done prog then pure # Just # pure # chr # fi #
    (\f -> foldl' f 0 # _prog_mem prog) # \a n -> a * 4 + n
  else do
    let hist = _prog_hist prog
    let st = (_prog_mem prog, _prog_ip prog)
    if Set.member st hist then pure Nothing else do
      put # prog {_prog_hist = Set.insert st hist}
      run_prog'

run_prog :: Mem -> Result
run_prog mem = evalState run_prog' # init_prog mem

parse_and_run_prog :: String -> Either Sctr Result
parse_and_run_prog src = do
  prog <- parse_prog src
  return # run_prog prog