@echo off
cls

cd deps
cabal build --ghc-options="-O2 -rtsopts=ignoreAll" --allow-new --builddir="../build"
cd ..