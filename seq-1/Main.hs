import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util
import Bit
import Parser

main :: IO ()
main = do
  putStrLn # unwords # map show # take 100 n_seq

mk_n_seq :: [N] -> [N]
mk_n_seq xs = run # do
  Just n <- pure # find <~ [0..] # \n -> run # do
    lists <- pure # map (snoc' n) #
      filter (not . null) # tails xs
    return # null # do
      ys <- tails xs
      zs <- lists
      True <- pure # isPrefixOf zs ys
      return ()
  return # n : mk_n_seq (snoc xs n)

n_seq :: [N]
n_seq = mk_n_seq []