import Prelude hiding (putStr, putStrLn, print, log)
import Data.Maybe
import Data.List
import Data.Ratio
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Monad

import Util
import Bit

main :: IO ()
main = do
  print # take 100 rand_seq

char_to_bit :: Char -> Bit
char_to_bit = fi . read . pure

str_to_bits :: String -> [Bit]
str_to_bits = map char_to_bit

sublists_n :: N -> [a] -> [[a]]
sublists_n k xs = do
  let n = len xs
  i <- [0 .. n - k]
  return # take (fi k) # drop (fi i) xs

list_to_mset :: (Ord a) => [a] -> Map a N
list_to_mset = flip foldr Map.empty # Map.alter #
  pure . maybe 1 (+1)

list_to_counts :: (Ord a) => [a] -> [(a, N)]
list_to_counts = Map.toList . list_to_mset

calc_rand_n :: N -> [Bit] -> Rational
calc_rand_n k xs = let
  cnts = map snd # list_to_counts # sublists_n k xs
  total = sum cnts
  exp_k = 2 ^ k
  denom = 1 % exp_k
  expected = (total % 1) * denom
  missing = exp_k - len cnts
  dif1 = sum # map (abs . (expected -) . (% 1)) cnts
  dif2 = (missing % 1) * expected
  dif = dif1 + dif2
  in dif

calc_rand :: [Bit] -> Rational
calc_rand xs = sum # map (flip calc_rand_n xs)
  [1 .. len xs - 1]

mk_seq' :: ([a] -> a) -> [a] -> [a]
mk_seq' f xs = let
  x = f xs
  in x : (mk_seq' f # snoc xs x)

mk_seq :: ([a] -> a) -> [a]
mk_seq f = mk_seq' f []

rand_seq :: [Bit]
rand_seq = mk_seq # \xs -> let
  r0 = calc_rand # snoc xs 0
  r1 = calc_rand # snoc xs 1
  in if r0 <= r1 then 0 else 1