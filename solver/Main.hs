{-# LANGUAGE TypeApplications #-}

import Util
import Bit
import Solver

import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Monad
import Control.Monad.State

import Solver
import Examples.BinStr

main :: IO ()
main = do
  let vars_num = 4
  let lhs = Cons (Var 0) (Var 1)
  let rhs = Cons (Var 2) (Cons (Bit 0) (Var 3))
  res <- flip evalStateT (sol_init vars_num) # do
    sol_add_eq () lhs rhs
    sol_solve @Message
  putStrLn # replicate 100 '=' ++ "\n"
  case res of
    Left errs -> flip mapM_ (nub errs) # \err -> do
      putStrLn # concat # _sol_err_msg err
    Right mp -> do
      let list = take (fi vars_num) # Map.toList mp
      flip mapM_ list # \(k, e) -> do
        putStrLn # show_eq (Var k) e