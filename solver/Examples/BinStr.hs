{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Examples.BinStr where

import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans

import Util
import Bit
import Solver

data Expr
  = Var N
  | Bit Bit
  | Nil
  | Cons Expr Expr
  deriving (Eq, Ord)

expr_type :: Expr -> N
expr_type e = case e of
  Var _ -> 0
  Bit _ -> 1
  Nil -> 2
  Cons _ _ -> 3

is_var :: Expr -> Prop
is_var e = expr_type e == 0

is_bit :: Expr -> Prop
is_bit e = expr_type e == 1

is_nil :: Expr -> Prop
is_nil e = expr_type e == 2

is_cons :: Expr -> Prop
is_cons e = expr_type e == 3

is_elem :: Expr -> Prop
is_elem e = is_var e || is_bit e

show_expr :: Expr -> String
show_expr e = case e of
  Var k -> if k < 26 then [list_get' k ['a'..]] else
    put_in_parens # concat ["a", show # k - 26]
  Bit b -> show b
  Nil -> "/"
  Cons a b -> concat
    [ put_in_parens' (not # is_elem a) # show_expr a
    , show_expr b ]

instance Show Expr where
  show = show_expr

is_cons' :: (Expr -> Prop) -> Expr -> Prop
is_cons' f e = case e of
  Cons a b -> f a
  _ -> False

expr_has_var :: N -> Expr -> Prop
expr_has_var i e = case e of
  Var j -> i == j
  Cons a b -> expr_has_var i a || expr_has_var i b
  _ -> False

type R = Message
type I = ()
type E = Expr
type M = IO

type P = ProcEq R I E M
type S = Solver R I E M

pure_var :: N -> S E
pure_var = liftm Var

reduce :: E -> E
reduce e = case e of
  Cons Nil a -> reduce a
  Cons a Nil -> reduce a
  Cons (Cons a b) c -> reduce # Cons a # Cons b c
  Cons a b -> Cons a # reduce b
  _ -> e

subst :: E -> S (E, Prop)
subst e = case e of
  Var k -> sol_get_var k
  Cons a b -> do
    (a, p1) <- subst a
    (b, p2) <- subst b
    return # (Cons a b, p1 && p2)
  _ -> pure (e, True)

vs_same :: P
vs_same lhs rhs =
  sol_guard (lhs == rhs) # do
    return # Right [([], [])]

var_vs :: P
var_vs lhs rhs =
  sol_guard (is_var lhs) # do
    let Var k = lhs
    pure # if expr_has_var k rhs
      then Left ["Occurs check ", quote # show lhs]
      else Right [([(k, rhs)], [])]

bit_vs_nbil :: P
bit_vs_nbil lhs rhs =
  sol_guard (is_bit lhs && is_nil rhs) # do
    return # Left ["Bit ", show lhs, " vs nil"]

bit_vs_bit :: P
bit_vs_bit lhs rhs =
  sol_guard (is_bit lhs && is_bit rhs) # do
    return # Left ["Bit ", show lhs, " vs bit ", show rhs]

cons_vs_nil :: P
cons_vs_nil lhs rhs =
  sol_guard (is_cons lhs && is_nil rhs) # do
    let Cons a b = lhs
    return # Right [([], [(a, Nil), (b, Nil)])]

cons_vs_bit :: P
cons_vs_bit lhs rhs =
  sol_guard (is_cons lhs && is_bit rhs) # do
    let Cons a b = lhs
    return # Right
      [ ([], [(a, rhs), (b, Nil)])
      , ([], [(a, Nil), (b, rhs)])
      ]

cons_vs_bit_cons :: P
cons_vs_bit_cons lhs rhs =
  sol_guard (is_cons lhs && is_cons' is_bit rhs) # do
    let Cons a b = lhs
    let Cons bit c = rhs
    x <- sol_mk_var
    return # Right
      [ ([], [(a, Nil), (b, rhs)])
      , ([], [(a, Cons bit x), (Cons x b, c)])
      ]

var_cons_vs :: P
var_cons_vs lhs rhs =
  sol_guard (is_cons' is_var lhs) # do
    let Cons (Var a) b = lhs
    x <- sol_mk_var
    return # Right
      [ ([(a, Nil)], [(b, rhs)])
      , ([(a, Cons (Bit 0) x)], [(lhs, rhs)])
      , ([(a, Cons (Bit 1) x)], [(lhs, rhs)])
      ]

procs_eq :: [P]
procs_eq =
  [ vs_same
  , var_vs
  , bit_vs_nbil
  , bit_vs_bit
  , cons_vs_nil
  , cons_vs_bit
  , cons_vs_bit_cons
  , var_cons_vs
  ]

finish :: [(E, E)] -> N -> S E
finish eqs k = pure Nil

show_eq :: E -> E -> String
show_eq lhs rhs = concat
  [show lhs, " - ", show rhs]

show_sys :: SolSys I E -> String
show_sys sys = unlines' #
  map (uncurry show_eq . sol_eq_to_pair) # _sys_eqs sys

display :: S ()
display = do
  sys_list <- gets _sol_systems
  let sep = put_in2 "\n" # replicate 10 '-'
  let str = intercalate sep # map show_sys sys_list
  liftIO # do
    putStrLn str
    -- getLine
    putStrLn ""
    nop

instance Rules R I E M where
  sol_pure_var = pure_var
  sol_reduce = liftm reduce
  sol_subst = subst
  sol_procs_eq = procs_eq
  sol_finish = finish
  sol_dbg = display