{-# LANGUAGE MultiParamTypeClasses #-}

module Solver where

import Util
import Bit

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)

import qualified Data.Set as Set
import qualified Data.Map as Map

data SolEq i e = SolEq
  { _eq_info :: i
  , _eq_lhs :: e
  , _eq_rhs :: e
  } deriving (Eq, Ord, Show)
  
data SolErr r i = SolErr
  { _sol_err_info :: i
  , _sol_err_msg :: r
  } deriving (Eq, Ord, Show)

data SolSys i e = SolSys
  { _sys_eqs :: [SolEq i e]
  , _sys_asgns :: Map N (e, Prop)
  , _sys_vars_num :: N
  , _sys_attempts :: N
  } deriving (Eq, Ord, Show)

data SolverT r i e = SolverT
  { _sol_systems :: [SolSys i e]
  , _sol_errs :: [SolErr r i]
  , _sol_solving :: Prop
  } deriving (Eq, Ord, Show)

type Solver r i e = StateT (SolverT r i e)
type SolRes r i e = Either [SolErr r i] (Map N e)
type ProcEqRes r e = Maybe (Either r [([(N, e)], [(e, e)])])
type ProcEq r i e m = e -> e -> Solver r i e m (ProcEqRes r e)

class (MonadFail m) => Rules r i e m where
  sol_pure_var :: N -> Solver r i e m e
  sol_reduce :: e -> Solver r i e m e
  sol_subst :: e -> Solver r i e m (e, Prop)
  sol_procs_eq :: [ProcEq r i e m]
  sol_finish :: [(e, e)] -> N -> Solver r i e m e
  sol_dbg :: Solver r i e m ()

sol_eq_to_pair :: SolEq i e -> (e, e)
sol_eq_to_pair eq = (_eq_lhs eq, _eq_rhs eq)

sol_pair_to_eq :: i -> (e, e) -> SolEq i e
sol_pair_to_eq info (lhs, rhs) = SolEq
  { _eq_info = info
  , _eq_lhs = lhs
  , _eq_rhs = rhs
  }

sol_mk_err :: i -> r -> SolErr r i
sol_mk_err info msg = SolErr
  { _sol_err_info = info
  , _sol_err_msg = msg
  }

sol_sys_init :: N -> SolSys i e
sol_sys_init vars_num = SolSys
  { _sys_eqs = []
  , _sys_asgns = Map.empty
  , _sys_vars_num = vars_num
  , _sys_attempts = 0
  }

sol_init :: N -> SolverT r i e
sol_init vars_num = SolverT
  { _sol_systems = [sol_sys_init vars_num]
  , _sol_errs = []
  , _sol_solving = False
  }

sol_has_sys :: (Monad m) => Solver r i e m Prop
sol_has_sys = gets # not . null . _sol_systems

sol_get_sys :: (Monad m) => Solver r i e m (SolSys i e)
sol_get_sys = gets # head . _sol_systems

sol_push_sys :: (Monad m) => SolSys i e -> Solver r i e m ()
sol_push_sys sys = modify # \sol -> sol
  {_sol_systems = sys : _sol_systems sol}

sol_pop_sys :: (Monad m) => Solver r i e m ()
sol_pop_sys = modify # \sol -> sol
  {_sol_systems = tail # _sol_systems sol}

sol_put_sys :: (Monad m) => SolSys i e -> Solver r i e m ()
sol_put_sys sys = modify # \sol -> sol
  {_sol_systems = sys : tail (_sol_systems sol)}

sol_modify_sys :: (Monad m) => (SolSys i e -> SolSys i e) -> Solver r i e m ()
sol_modify_sys f = modify # \sol -> let
  (sys : rest) = _sol_systems sol
  in sol {_sol_systems = f sys : rest}

sol_solving :: (Monad m) => Solver r i e m Prop
sol_solving = gets _sol_solving

sol_enter_solving_mode :: (Monad m) => Solver r i e m ()
sol_enter_solving_mode = do
  solving <- sol_solving
  if not solving then nop else error ""
  modify # \sol -> sol {_sol_solving = True}
  sol_modify_sys # \sys -> sys {_sys_eqs = reverse # _sys_eqs sys}

sol_add_eq_aux :: (Monad m) => (SolEq i e -> [SolEq i e] -> [SolEq i e]) ->
  i -> e -> e -> Solver r i e m ()
sol_add_eq_aux f info lhs rhs = do
  sys <- sol_get_sys
  eq <- pure # SolEq
    { _eq_info = info
    , _eq_lhs = lhs
    , _eq_rhs = rhs
    }
  let eqs' = f eq # _sys_eqs sys
  let sys' = sys {_sys_eqs = eqs'}
  sol_put_sys sys'

sol_add_eq :: (Monad m) => i -> e -> e -> Solver r i e m ()
sol_add_eq info lhs rhs = do
  solving <- sol_solving
  let f = if solving then snoc' else (:)
  sol_add_eq_aux f info lhs rhs

sol_add_eqs :: (Monad m) => [(i, e, e)] -> Solver r i e m ()
sol_add_eqs = mapM_ # \(info, lhs, rhs) -> sol_add_eq info lhs rhs

sol_guard :: (Monad m) => Prop -> m a -> m (Maybe a)
sol_guard p m = if p then m >>= liftm Just else pure Nothing

sol_mk_var' :: (Rules r i e m) => Solver r i e m N
sol_mk_var' = do
  sys <- sol_get_sys
  let n = _sys_vars_num sys
  sol_put_sys # sys {_sys_vars_num = n + 1}
  return n

sol_mk_var :: (Rules r i e m) => Solver r i e m e
sol_mk_var = do
  k <- sol_mk_var'
  sol_pure_var k

sol_get_var :: (Rules r i e m) => N -> Solver r i e m (e, Prop)
sol_get_var k = do
  sys <- sol_get_sys
  let asgns = _sys_asgns sys
  case Map.lookup k asgns of
    Nothing -> do
      e <- sol_pure_var k
      return (e, False)
    Just res@(e, p) -> if p then pure res else do
      (e, p) <- sol_subst e
      e <- sol_reduce e
      let asgns' = Map.insert k (e, p) asgns
      sol_put_sys # sys {_sys_asgns = asgns'}
      return res

sol_skip_eq :: (Monad m) => Solver r i e m ()
sol_skip_eq = sol_modify_sys # \sys -> sys
  { _sys_eqs = list_rot_left # _sys_eqs sys
  , _sys_attempts = _sys_attempts sys + 1
  }

sol_add_err :: (Monad m) => i -> r -> Solver r i e m ()
sol_add_err info msg = modify # \sol -> sol
  {_sol_errs = sol_mk_err info msg : _sol_errs sol}

sol_proc_eq' :: (Rules r i e m) => e -> e ->
  [ProcEq r i e m] -> Solver r i e m (ProcEqRes r e)
sol_proc_eq' lhs rhs fs = case fs of
  [] -> pure Nothing
  (f:fs) -> f lhs rhs >>= \res -> case res of
    Just _ -> pure res
    Nothing -> f rhs lhs >>= \res -> case res of
      Just _ -> pure res
      _ -> sol_proc_eq' lhs rhs fs

sol_proc_eq :: (Rules r i e m) => SolEq i e -> Solver r i e m (ProcEqRes r e)
sol_proc_eq eq = sol_proc_eq' (_eq_lhs eq) (_eq_rhs eq) sol_procs_eq

sol_simp :: (Rules r i e m) => e -> Solver r i e m e
sol_simp e = do
  (e, _) <- sol_subst e
  sol_reduce e

sol_simp_eq :: (Rules r i e m) => SolEq i e -> Solver r i e m (SolEq i e)
sol_simp_eq eq = do
  lhs <- sol_simp # _eq_lhs eq
  rhs <- sol_simp # _eq_rhs eq
  return # eq {_eq_lhs = lhs, _eq_rhs = rhs}

sol_extract_vars :: (Rules r i e m) => Solver r i e m (Map N e)
sol_extract_vars = do
  sys <- sol_get_sys
  mapM_ sol_get_var # range # _sys_vars_num sys
  sys <- sol_get_sys
  return # Map.map fst # _sys_asgns sys

sol_solve' :: (Rules r i e m) => Solver r i e m (SolRes r i e)
sol_solve' = do
  has_sys <- sol_has_sys
  if not has_sys then do
    errs <- gets _sol_errs
    return # Left errs
  else do
    sys <- sol_get_sys
    let eqs = _sys_eqs sys
    let attempts = _sys_attempts sys
    if attempts /= len eqs then do
      (eq:eqs) <- pure eqs
      eq <- sol_simp_eq eq

      sol_put_sys # sys {_sys_eqs = eq : eqs}
      sol_dbg

      let info = _eq_info eq
      res <- sol_proc_eq eq
      case res of
        Nothing -> sol_skip_eq
        Just res -> do
          sys <- sol_get_sys
          sol_pop_sys
          case res of
            Left msg -> sol_add_err info msg
            Right xs -> do
              sys <- pure # sys {_sys_attempts = 0}
              let asgns = _sys_asgns sys
              sys_list <- pure # do
                (asgns_new, eqs_new) <- xs
                mp <- pure # Map.map (\a -> (a, False)) #
                  Map.fromList asgns_new
                let eqs' = map (sol_pair_to_eq info) eqs_new
                return # sys
                  { _sys_eqs = eqs ++ eqs'
                  , _sys_asgns = Map.union mp asgns
                  }
              modify # \sol -> sol
                {_sol_systems = sys_list ++ _sol_systems sol} -- DFS
                -- {_sol_systems = _sol_systems sol ++ sys_list} -- BFS
      sol_solve'
    else do
      eqs <- pure # map sol_eq_to_pair eqs
      let asgns = _sys_asgns sys
      indices <- pure # filter (\k -> not # Map.member k asgns) #
        range # _sys_vars_num sys
      asgns <- flip2 foldM asgns indices # \asgns k -> do
        e <- sol_finish eqs k
        return # Map.insert k (e, False) asgns
      sol_put_sys # sys {_sys_asgns = asgns}
      res <- sol_extract_vars
      return # Right res

sol_solve :: (Rules r i e m) => Solver r i e m (SolRes r i e)
sol_solve = do
  sol_enter_solving_mode
  sol_solve'