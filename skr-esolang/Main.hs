import qualified Data.Set as Set
import qualified Data.Map as Map

import Util
import Parser

src_file = "src.txt"
inp_file = "inp.txt"

tok_appr = "#"

data Expr = K | S | R | A Expr Expr
  deriving (Eq, Ord)

instance Show Expr where
  show = show_expr

data ProgT = ProgT
  { _prog_defs :: Map Name Expr
  , _prog_main :: Maybe Expr
  , _prog_inp :: Expr
  } deriving (Eq, Ord, Show)

type ProgP = ParserM ProgT
type Prog = StateT ProgT (Except Sctr)

comb_names :: [(Name, Expr)]
comb_names = [("K", K), ("S", S), ("R", R)]

main :: IO ()
main = do
  src <- readFile src_file
  inp <- readFile inp_file
  case parse_and_run_prog src inp of
    Left msg -> putStrLn # concat msg
    Right res -> print res

is_app :: Expr -> Prop
is_app e = case e of
  A _ _ -> True
  _ -> False

show_expr' :: Prop -> Expr -> String
show_expr' p e = case e of
  A a b -> let
    sa = show_expr' False a
    sb = show_expr' True b
    in if not # is_app b
      then unwords [sa, sb]
      else if p then unwords [sa, tok_appr, sb]
      else unwords [sa, put_in_parens sb]
  _ -> fst # fromJust # find <~ comb_names #
    \(_, c) -> c == e

show_expr :: Expr -> String
show_expr = show_expr' True

app :: Expr -> Expr -> Expr
app f x = case f of
  A K a -> a
  A (A S a) b -> app (app a x) # app b x
  A R a -> case x of
    A x y -> apps [a, S, x, y]
    _ -> app a K
  _ -> A f x

apps :: [Expr] -> Expr
apps = foldl1 app

init_prog :: Expr -> ProgT
init_prog inp = ProgT
  { _prog_defs = Map.fromList comb_names
  , _prog_main = Nothing
  , _prog_inp = inp
  }

parse_expr_ident :: ProgP Expr
parse_expr_ident = do
  True <- p_query # pure True ++> do
    name <- p_ident
    p_tok "="
    return False
  name <- p_ident
  res <- gets # map_get' name . _prog_defs
  case res of
    Nothing -> err ["Undefined identifier ", show name]
    Just e -> pure e

parse_expr_group :: ProgP Expr
parse_expr_group = p_parens parse_expr

parse_expr_appr :: ProgP Expr
parse_expr_appr = do
  p_tok tok_appr
  parse_expr

parse_expr_1 :: ProgP Expr
parse_expr_1 = choice
  [ parse_expr_ident
  , parse_expr_group
  , parse_expr_appr
  ]

parse_expr :: ProgP Expr
parse_expr = do
  es <- p_all parse_expr_1
  return # apps es

parse_inp :: ProgP Expr
parse_inp = do
  put # init_prog und
  parse_expr

parse_comment :: ProgP ()
parse_comment = (p_tok ";" <++) # trimmed # do
  p_tok "--"
  (>> nop) # p_take_while # \c -> not # elem c "\r\n"

parse_def :: ProgP ()
parse_def = parse_comment <++ do
  name <- p_ident
  p_tok "="
  has <- gets # Map.member name . _prog_defs
  ifm has # err ["Redefinition of ", show name]
  e <- parse_expr
  modify # \p -> p
    { _prog_defs = map_insert name e # _prog_defs p
    , _prog_main = Just e
    }

parse_prog :: Expr -> ProgP ProgT
parse_prog inp = do
  put # init_prog inp
  trimmed # p_all' parse_def
  get

run_prog :: Prog Expr
run_prog = do
  e <- gets _prog_main
  case e of
    Nothing -> err ["At least one definition must be provided"]
    Just e -> do
      inp <- gets _prog_inp
      return # app e inp

parse_and_run_prog :: String -> String -> Either Sctr Expr
parse_and_run_prog src inp = do
  inp <- parse parse_inp inp
  prog <- parse (parse_prog inp) src
  runExcept # evalStateT run_prog prog