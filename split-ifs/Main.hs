type N = Integer

infixr 0 #
(#) = id

main :: IO ()
main = putStr # unlines #
  map (unwords . map show) #
  map f [1..5]

f :: N -> [N]
f n = go n where
  go 0 = []
  go k = n - k + 1 : let r = go (k - 1) in r ++ r