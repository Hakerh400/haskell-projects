import Data.List hiding (group)

main :: IO ()
main = putStr $ unlines $ map show $ group pf list

list :: [Int]
list = [1..100]

pf :: Int -> (Int, Int)
pf n = (mod n 2, mod n 7)

group :: (Eq b) => (a -> b) -> [a] -> [[a]]
group f = foldr (\x p -> maybe ([x] : p)
  (\i -> take i p ++ (x : p !! i) : drop (i + 1) p)
  $ findIndex (\ys -> f (head ys) == f x) p) []