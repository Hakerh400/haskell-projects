{-# LANGUAGE DeriveFunctor #-}

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)
import Data.IORef
import Control.Comonad.Store

import Util

-- | A Grid is represented as a Store comonad with position (Integer, Integer) and Bool values.
type Grid = Store (Integer, Integer) Bool

-- | Rule to compute the next state of a cell based on its neighbors.
rule :: Grid -> Bool
rule grid = let
  current = extract grid           -- Current cell's state (alive/dead)
  (x, y) = pos grid                -- Current cell's position
  -- Generate all 8 neighboring positions
  neighbors = [ (x + dx, y + dy) | dx <- [-1, 0, 1], dy <- [-1, 0, 1], (dx, dy) /= (0, 0) ]
  -- Count live neighbors using the grid function
  liveNeighbors = sum [ 1 | p <- neighbors, peek p grid ]
  in case (current, liveNeighbors) of
    -- Conway's Game of Life rules
    (True, 2) -> True    -- Survive with 2 neighbors
    (True, 3) -> True    -- Survive with 3 neighbors
    (False, 3) -> True   -- Birth with exactly 3 neighbors
    _ -> False           -- Die due to underpopulation or overpopulation

-- | Advance the simulation by one generation.
step :: Grid -> Grid
step = extend rule

-- | Initialize a grid with a specific pattern.
-- Example: Blinker oscillator (period 2)
initial_grid :: (Integer, Integer) -> Bool
initial_grid (x, y) = y == 1 && x `elem` [0, 1, 2]

-- | Create the initial grid Store with an arbitrary starting position.
initial_store :: Grid
initial_store = store initial_grid (0, 0)

main :: IO ()
main = do
  grid <- pure # step initial_store
  mapM_ putStrLn # do
    y <- [-2 .. 4]
    return # do
      x <- [-2 .. 4]
      h <- pure # fst (runStore grid) (x, y)
      return # ite h '#' '.'