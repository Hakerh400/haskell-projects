import qualified Data.Set as Set
import qualified Data.Map as Map

import Util

type S = State (Map N N)

main :: IO ()
main = do
  print # f 12308918230918230182031231231293810923

f_aux :: (N -> S N) -> N -> S N
f_aux r n = if n == 0 then pure n else do
  xs <- mapM <~ [2 .. 4] # \i -> r # div n i
  return # max n # sum xs

f' :: N -> S N
f' n = do
  res <- gets # map_get' n
  case res of
    Just x -> pure x
    Nothing -> do
      x <- f_aux f' n
      modify # map_insert n x
      return x

f :: N -> N
f n = evalState (f' n) Map.empty