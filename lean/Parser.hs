module Parser where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.List
import Control.Monad.State
import GHC.Read
import Text.ParserCombinators.ReadPrec

import Base
import ParserBase
import Kernel
import ElaboratorBase

keywords :: Set String
keywords = Set.fromList
  [ "Sort"
  , "let"
  , "in"
  , "def"
  ]

list_to_app :: [Expr] -> Expr
list_to_app = foldl1 App

is_keyword :: String -> Prop
is_keyword s = Set.member s keywords

parse_ident :: ReadPrec String
parse_ident = p_ident' >>= \name -> ite (is_keyword name) pfail (pure name)

is_ident :: Expr -> Prop
is_ident e = case e of
  Ref _ -> True
  Var _ -> True
  _ -> False

is_app :: Expr -> Prop
is_app e = case e of
  App _ _ -> True
  _ -> False

has_var :: N -> Expr -> Prop
has_var n e = case e of
  Var i -> i == n
  Binder _ a b -> has_var n a || has_var (n + 1) b
  App a b -> has_var n a || has_var n b
  _ -> False

instance Show Expr where
  show = show_expr 0

show_expr :: N -> Expr -> String
show_expr n e = case e of
  Sort u -> concat ["Sort ", show u]
  Ref name -> name
  Var i -> show_var (n - i - 1)
  Binder Pi a b -> if (has_var 0 b)
    then show_binder "Π" n a b
    else concat [show_expr'' n a, " → ", show_expr (n + 1) b]
  Binder Lam a b -> show_binder "λ" n a b
  Binder Let a b -> show_binder' "let " " := " " in " n a b
  App a b -> concat [show_expr'' n a, " ", show_expr' n b]

show_expr' :: N -> Expr -> String
show_expr' n e = let
  s = show_expr n e
  in ite (is_ident e) s # put_in_parens s

show_expr'' :: N -> Expr -> String
show_expr'' n e = ite (is_app e) show_expr show_expr' n e

show_binder' :: String -> String -> String -> N -> Expr -> Expr -> String
show_binder' s1 s2 s3 n a b = concat [s1, show_var n, s2, show_expr n a, s3, show_expr (n + 1) b]

show_binder :: String -> N -> Expr -> Expr -> String
show_binder s = show_binder' (concat [s, " ("]) " : " "), "

show_var :: N -> String
show_var n = [['a'..'z'] !! fi n]

instance Read Expr where
  readPrec = parse_expr

parse_expr :: ReadPrec Expr
parse_expr = parse_expr' []

parse_expr' :: [Name] -> ReadPrec Expr
parse_expr' ns = do
  es <- p_all # parse_expr_elem ns
  return # list_to_app es

parse_expr_elem :: [Name] -> ReadPrec Expr
parse_expr_elem ns = trimmed # choice
  [ parse_sort
  , parse_pi ns
  , parse_lam ns
  , parse_let ns
  , parse_term ns
  ]

parse_term :: [Name] -> ReadPrec Expr
parse_term ns = choice
  [ parse_ident_in_ctx ns
  , parse_group ns
  ]

parse_ident_in_ctx :: [Name] -> ReadPrec Expr
parse_ident_in_ctx ns = parse_ident >>= \name -> pure # case elemIndex name ns of
  Just i -> Var # fi i
  Nothing -> Ref name

parse_group :: [Name] -> ReadPrec Expr
parse_group ns = p_parens # parse_expr' ns

parse_sort :: ReadPrec Expr
parse_sort = do
  p_tok "Sort"
  n <- p_nat
  return # Sort n

parse_binder' :: [String] -> [String] -> [String] -> (Expr -> Expr -> Expr) -> [Name] -> ReadPrec Expr
parse_binder' s1 s2 s3 ctor ns = do
  p_toks s1
  name <- parse_ident
  p_toks s2
  a <- parse_expr' ns
  p_toks s3
  b <- parse_expr' (name : ns)
  return # ctor a b

parse_binder :: String -> (Expr -> Expr -> Expr) -> [Name] -> ReadPrec Expr
parse_binder s = parse_binder' [s, "("] [":"] [")", ","]

parse_pi :: [Name] -> ReadPrec Expr
parse_pi ns = parse_binder "Π" (Binder Pi) ns <++ do
  a <- parse_term ns
  p_tok "→" <++ p_tok "->"
  b <- parse_expr' ("" : ns)
  return # Binder Pi a b

parse_lam :: [Name] -> ReadPrec Expr
parse_lam = parse_binder "λ" (Binder Lam)

parse_let :: [Name] -> ReadPrec Expr
parse_let = parse_binder' ["let"] [":="] ["in"] (Binder Let)

instance Show TheoryT where
  show = show_theory

show_theory :: TheoryT -> String
show_theory = evalState show_theory'

show_theory' :: Theory String
show_theory' = do
  idents <- get_idents
  return # unlines' # map show # reverse idents

instance Show IdentInfo where
  show = show_ident_info

show_ident_info :: IdentInfo -> String
show_ident_info info = concat
  [ _name info, " : "
  , show # _type info
  , show_ident_info_val info
  ]

show_ident_info_val :: IdentInfo -> String
show_ident_info_val info = case _val info of
  Nothing -> ""
  Just e -> concat [" := ", show e]

instance Read Cmd where
  readPrec = parse_cmd

parse_cmd :: ReadPrec Cmd
parse_cmd = trimmed # choice'
  [ parse_def
  ]

parse_def :: ReadPrec Cmd
parse_def = do
  p_tok "def"
  name <- parse_ident
  args <- parse_def_args
  p_tok ":"
  t <- parse_expr
  p_tok ":="
  e <- parse_expr
  return # Def name (add_def_args Pi args t) (add_def_args Lam args e)

parse_def_args :: ReadPrec [Arg]
parse_def_args = p_all parse_def_args' >>= pure . concat

parse_def_args' :: ReadPrec [Arg]
parse_def_args' = p_parens # do
  names <- p_all' # trimmed parse_ident
  p_tok ":"
  t <- parse_expr
  return # map (\name -> (name, t)) names

add_def_args :: BinderT -> [Arg] -> Expr -> Expr
add_def_args ctor args e = foldr (add_def_arg ctor) e args

add_def_arg :: BinderT -> Arg -> Expr -> Expr
add_def_arg ctor (name, t) e = Binder ctor t # ref_to_var name e

ref_to_var :: Name -> Expr -> Expr
ref_to_var name = ref_to_var' name 0

ref_to_var' :: Name -> N -> Expr -> Expr
ref_to_var' name n e = case e of
  Sort _ -> e
  Ref name1 -> ite (name1 == name) (Var n) e
  Binder ctor a b -> Binder ctor (ref_to_var' name n a) (ref_to_var' name (n + 1) b)
  App a b -> App (ref_to_var' name n a) (ref_to_var' name n b)
  Var i -> ite (i >= n) (Var (i + 1)) e

instance Read Src where
  readPrec = parse_src

parse_src :: ReadPrec Src
parse_src = trimmed # do
  cmds <- p_all parse_cmd
  return # Src cmds