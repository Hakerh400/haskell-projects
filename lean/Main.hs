import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Control.Monad.State
import GHC.Read
import Text.ParserCombinators.ReadPrec

import Base
import ParserBase
import Parser
import Kernel
import ElaboratorBase
import Elaborator

src_file :: FilePath
src_file = "src/main.txt"

main :: IO ()
main = do
  let t = evalState (calc_type # read "λ (t : Sort 1), λ (z : t), λ (f : Nat -> t), λ (n : Nat), rec t") init_th
  maybe (putStrLn "/") print t
  let t = evalState (calc_type # read "λ (t : Sort 1), λ (z : t), λ (f : Nat -> t), λ (n : Nat), rec t z") init_th
  maybe (putStrLn "/") print t
  -- src <- readFile src_file
  -- let Src cmds = read src
  -- let th = execState (run_cmds cmds) init_th
  -- print th