def Prop : Sort 1 := Sort 0
def Type : Sort 2 := Sort 1

def 0 : Nat := zero
def 1 : Nat := succ 0

def id (t : Type) (a : t) : t := a
def const (t s : Type) (a : t) (b : s) : t := a

def cases (t : Type) (z : t) (f : Nat -> t) (n : Nat) : t :=
rec t z (λ (k : Nat), λ (a : t), f k) n