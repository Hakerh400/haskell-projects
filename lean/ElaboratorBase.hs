module ElaboratorBase where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.List
import Control.Monad.State
import GHC.Read
import Text.ParserCombinators.ReadPrec

import Base
import Kernel

data Cmd =
  Def Name Expr Expr

newtype Src = Src [Cmd]

type Arg = (Name, Expr)