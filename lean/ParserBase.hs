module ParserBase where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import GHC.Read
import Text.ParserCombinators.ReadPrec

import Base

infixr 5 ++>
(++>) :: ReadPrec a -> ReadPrec a -> ReadPrec a
(++>) = flip (<++)

parse :: ReadPrec a -> String -> a
parse m str = case readPrec_to_S m 0 str of
  [(a, [])] -> a
  (a : b : _) -> error "Ambiguous parse"
  _ -> error "No parse"

p_char :: Char -> ReadPrec Char
p_char c = get >>= \c1 -> ite (c1 == c) (pure c) pfail

p_str :: String -> ReadPrec String
p_str [] = pure ""
p_str s@(c:cs) = p_char c >> p_str cs >> pure s

p_all :: ReadPrec a -> ReadPrec [a]
p_all r = pure [] ++> do
  x <- r
  xs <- p_all r
  return # x : xs

p_all' :: ReadPrec a -> ReadPrec [a]
p_all' r = do
  xs <- p_all r
  ite (null xs) pfail (pure xs)

p_sep :: ReadPrec a -> ReadPrec b -> ReadPrec ([a], [b])
p_sep r s = pure ([], []) ++> do
  x <- r
  zs <- p_all # do
    b <- s
    a <- r
    return (a, b)
  let (xs, ys) = unzip zs
  return (x : xs, ys)

p_sep' :: ReadPrec a -> ReadPrec b -> ReadPrec ([a], [b])
p_sep' r s = do
  (a, b) <- p_sep r s
  ite (null a) pfail (pure (a, b))

p_sepf :: ReadPrec a -> ReadPrec b -> ReadPrec [a]
p_sepf r s = do
  result <- p_sep r s
  return # fst result

p_sepf' :: ReadPrec a -> ReadPrec b -> ReadPrec [a]
p_sepf' r s = do
  result <- p_sep' r s
  return # fst result

p_take_while :: (Char -> Prop) -> ReadPrec String
p_take_while f = p_all # get >>= \c -> ite (f c) (pure c) pfail

p_drop_while :: (Char -> Prop) -> ReadPrec ()
p_drop_while f = p_take_while f >> pure ()

p_take_while' :: (Char -> Prop) -> ReadPrec String
p_take_while' f = do
  xs <- p_take_while f
  if null xs then pfail else pure xs

trimmed :: ReadPrec a -> ReadPrec a
trimmed r = do
  p_drop_while isSpace
  x <- r
  p_drop_while isSpace
  return x

p_ws :: ReadPrec ()
p_ws = p_all (p_char space) >> pure ()

is_ident_char :: Char -> Prop
is_ident_char c = or
  [ isLetter c
  , isDigit c
  , c == '\x5F'
  , c == '\''
  ]

p_ident :: ReadPrec String
p_ident = do
  c <- get
  if isLetter c then do
    s <- p_take_while is_ident_char
    return # c : s
  else pfail

p_ident' :: ReadPrec String
p_ident' = p_take_while' is_ident_char

p_nat :: ReadPrec N
p_nat = do
  s <- p_ident'
  ite (p_is_valid_nat s) (pure # (read s :: N)) pfail

p_is_valid_nat :: String -> Prop
p_is_valid_nat s = s == "0" ||
  ( not (null s) &&
    all isDigit s &&
    all (/= '0') s )

p_tok :: String -> ReadPrec String
p_tok t = trimmed # p_str t

p_toks :: [String] -> ReadPrec [String]
p_toks = mapM p_tok

choice' :: [ReadPrec a] -> ReadPrec a
choice' = foldr1 (<++)

p_parens :: ReadPrec a -> ReadPrec a
p_parens r = do
  p_tok "("
  a <- r
  p_tok ")"
  return a