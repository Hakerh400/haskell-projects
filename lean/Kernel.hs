module Kernel
  ( Name
  , BinderT(..)
  , Expr(..)
  , IdentInfo(..)
  , TheoryT
  , Theory
  , init_th
  , get_idents
  , has_ident
  , get_ident_info
  , get_ident_type
  , reduce_step
  , reduce
  , reduce_all
  , expr_eq
  , calc_type
  , check_type
  , add_def
  ) where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.List
import Control.Monad.State

import Base

type Name = String

data BinderT = Pi | Lam | Let
  deriving (Eq, Ord)

data Expr =
  Sort N |
  Ref Name |
  Var N |
  Binder BinderT Expr Expr |
  App Expr Expr
  deriving (Eq, Ord)

data TheoryT = TheoryT
  { _idents_list :: [IdentInfo]
  , _idents :: Map Name IdentInfo
  }

data IdentInfo = IdentInfo
  { _name :: Name
  , _type :: Expr
  , _val :: Maybe Expr
  }

type Theory = State TheoryT

imax :: N -> N -> N
imax u 0 = 0
imax u v = max u v

init_th :: TheoryT
init_th = execState init_th' # TheoryT
  { _idents_list = []
  , _idents = Map.empty
  }

init_th' :: Theory ()
init_th' = do
  add_const "Nat" # Sort 1
  add_const "zero" # Ref "Nat"
  add_const "succ" # Binder Pi (Ref "Nat") (Ref "Nat")
  add_const "rec" # Binder Pi (Sort 1) (Binder Pi (Var 0) (Binder Pi (Binder Pi (Ref "Nat") (Binder Pi (Var 2) (Var 3))) (Binder Pi (Ref "Nat") (Var 3))))
  add_const "ind" # Binder Pi (Binder Pi (Ref "Nat") (Sort 0)) (Binder Pi (App (Var 0) (Ref "zero")) (Binder Pi
    (Binder Pi (Ref "Nat") (Binder Pi (App (Var 2) (Var 0)) (App (Var 3) (App (Ref "succ") (Var 1))))) (Binder Pi (Ref "Nat") (App (Var 3) (Var 0)))))

add_ident :: Name -> Expr -> Maybe Expr -> Theory ()
add_ident name t e = modify # \th -> let
  info = IdentInfo { _name = name, _type = t, _val = e }
  in th
    { _idents_list = info : _idents_list th
    , _idents = Map.insert name info # _idents th }

add_const :: Name -> Expr -> Theory ()
add_const name t = add_ident name t Nothing

get_idents :: Theory [IdentInfo]
get_idents = gets _idents_list

has_ident :: Name -> Theory Prop
has_ident name = gets # \th -> Map.member name # _idents th

get_ident_info :: Name -> Theory (Maybe IdentInfo)
get_ident_info name = gets # \th -> Map.lookup name # _idents th

get_ident_type :: Name -> Theory (Maybe Expr)
get_ident_type name = get_ident_info name >>= \info -> pure # info >>= pure . _type

get_ident_val :: Name -> Theory (Maybe Expr)
get_ident_val name = get_ident_info name >>= \info -> pure # info >>= _val

adapt_arg_vars :: N -> N -> Expr -> Expr
adapt_arg_vars n m arg = case arg of
  Var i -> if i < m then arg else Var (i + n)
  Binder ctor a b -> Binder ctor (adapt_arg_vars n m a) (adapt_arg_vars n (m + 1) b)
  App a b -> App (adapt_arg_vars n m a) (adapt_arg_vars n m b)
  _ -> arg

subst_var :: N -> Expr -> Expr -> Expr
subst_var n arg target = case target of
  Var i -> if i < n then target else if i > n
    then Var (i - n - 1)
    else adapt_arg_vars n 0 arg
  Binder ctor a b -> Binder ctor (subst_var n arg a) (subst_var (n + 1) arg b)
  App a b -> App (subst_var n arg a) (subst_var n arg b)
  _ -> target

reduce_step :: Expr -> Theory Expr
reduce_step e = case e of
  Ref name -> get_ident_val name >>= \val -> pure # fromMaybe e val
  Binder Let arg target -> pure # subst_var 0 arg target
  App (Binder Lam t target) arg -> pure # subst_var 0 arg target
  App (App (App (App (Ref "rec") t) z) f) arg -> reduce_rec t z f arg
  App a b -> reduce_step a >>= \a1 -> pure # App a1 b
  _ -> pure e

reduce_rec :: Expr -> Expr -> Expr -> Expr -> Theory Expr
reduce_rec t z f arg = case arg of
  Ref "zero" -> pure z
  App (Ref "succ") n -> pure # App (App f n) (App (App (App (App (Ref "rec") t) z) f) n)
  _ -> reduce_step arg >>= \a1 -> pure # App (App (App (App (Ref "rec") t) z) f) a1

reduce :: Expr -> Theory Expr
reduce e = reduce_step e >>= \e1 -> ite (e1 == e) (pure e) (reduce e1)

reduce_all :: Expr -> Theory Expr
reduce_all e = reduce e >>= \e -> case e of
  Binder ctor a b -> reduce_all a >>= \a -> reduce_all b >>= \b -> pure # Binder ctor a b
  App a b -> reduce_all b >>= \b -> pure # App a b
  _ -> pure e

reduce_m :: Maybe Expr -> Theory (Maybe Expr)
reduce_m Nothing = pure Nothing
reduce_m (Just e) = reduce e >>= pure . Just

expr_eq :: Expr -> Expr -> Theory Prop
expr_eq a b = ite (a == b) (pure True) (expr_eq' a b)

expr_eq' :: Expr -> Expr -> Theory Prop
expr_eq' a b = reduce a >>= \a -> reduce b >>= \b -> case (a, b) of
  (Binder ctor1 a1 b1, Binder ctor2 a2 b2) -> if ctor1 /= ctor2 then pure False else
    expr_eq a1 a2 >>= \p1 -> if p1 then expr_eq b1 b2 else pure False
  (App a1 b1, App a2 b2) -> expr_eq a1 a2 >>= \p1 -> if p1 then expr_eq b1 b2 else pure False
  (a, b) -> pure # a == b

calc_type :: Expr -> Theory (Maybe Expr)
calc_type = calc_type' []

calc_type' :: [Expr] -> Expr -> Theory (Maybe Expr)
calc_type' ts e = case e of
  Sort u -> pure # Just # Sort (u + 1)
  Ref name -> get_ident_type name
  Var i -> pure # list_get i ts >>= pure . inc_free_vars i
  Binder Pi a b -> calc_type' ts a >>= reduce_m >>= \t1 -> case t1 of
    Just (Sort u) -> calc_type' (a : ts) b >>= reduce_m >>= \t2 -> case t2 of
      Just (Sort v) -> pure # Just # Sort (imax u v)
      _ -> pure Nothing
    _ -> pure Nothing
  Binder Lam a t -> calc_type' ts a >>= reduce_m >>= \t1 -> case t1 of
    Just (Sort _) -> calc_type' (a : ts) t >>= \b -> case b of
      Just b -> pure # Just # Binder Pi a b
      _ -> pure Nothing
    _ -> pure Nothing
  Binder Let t s -> calc_type' ts t >>= \a -> case a of
    Just a -> calc_type' (a : ts) s >>= \b -> case b of
      Just b -> pure # Just # subst_var 0 t b
      _ -> pure Nothing
    _ -> pure Nothing
  App s t -> calc_type' ts s >>= \p -> case p of
    Just (Binder Pi a b) -> calc_type' ts t >>= \a1 -> case a1 of
      Just a1 -> expr_eq a1 a >>= \eq -> if not eq then pure Nothing else
        pure # Just # subst_var 0 t b
      _ -> pure Nothing
    _ -> pure Nothing

inc_free_vars :: N -> Expr -> Expr
inc_free_vars i = inc_free_vars' i 0

inc_free_vars' :: N -> N -> Expr -> Expr
inc_free_vars' i n e = case e of
  Var j -> ite (j >= n) (Var (j + i + 1)) e
  Binder ctor a b -> Binder ctor (inc_free_vars' i n a) (inc_free_vars' i (n + 1) b)
  App a b -> App (inc_free_vars' i n a) (inc_free_vars' i n b)
  _ -> e

check_type :: Expr -> Expr -> Theory Prop
check_type e t = calc_type e >>= \t1 -> case t1 of
  Nothing -> pure False
  Just t1 -> expr_eq t1 t

add_def :: Name -> Expr -> Expr -> Theory Prop
add_def name t e = has_ident name >>= \p -> if p then (pure False) else
  check_type e t >>= \p -> if not p then (pure False) else
    add_ident name t (Just e) >> pure True