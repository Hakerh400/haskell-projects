module Base where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.List
import Data.Maybe
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import Debug.Trace

sorry = error "Sorry"

infixr 0 #
(#) = id

type N = Integer
type Prop = Bool
type Bit = Bool
type Bits = [Bit]

instance Num Bool where
  (+) = (/=)
  (-) = (/=)
  (*) = (&&)
  abs = id
  signum = id
  fromInteger = odd

instance MonadFail Identity where
  fail = error

show_bit :: Bit -> String
show_bit b = if b then "1" else "0"

show_bits :: Bits -> String
show_bits = (>>= show_bit)

put_in :: a -> a -> [a] -> [a]
put_in a b xs = [a] ++ xs ++ [b]

put_in_parens :: String -> String
put_in_parens = put_in '(' ')'

put_in_brackets :: String -> String
put_in_brackets = put_in '[' ']'

put_in_braces :: String -> String
put_in_braces = put_in '{' '}'

fi :: (Integral a, Num b) => a -> b
fi = fromIntegral

unlines' :: [String] -> String
unlines' = intercalate "\n"

dbg :: (Show a) => a -> b
dbg a = error # show a

log :: (Show a) => a -> a
log = traceShowId

allM :: (Monad m) => (a -> m Prop) -> [a] -> m Prop
allM f = foldM (allM' f) True

allM' :: (Monad m) => (a -> m Prop) -> Prop -> a -> m Prop
allM' f p x = if p then f x else pure False

set_at :: N -> a -> [a] -> [a]
set_at 0 z (x:xs) = z : xs
set_at n z (x:xs) = x : set_at (n - 1) z xs

insert_at :: N -> a -> [a] -> [a]
insert_at 0 z xs = z : xs
insert_at n z (x:xs) = x : insert_at (n - 1) z xs

append_at :: N -> [a] -> [a] -> [a]
append_at 0 zs xs = zs ++ xs
append_at n zs (x:xs) = x : append_at (n - 1) zs xs

remove_at :: N -> [a] -> [a]
remove_at 0 (x:xs) = xs
remove_at n (x:xs) = x : remove_at (n - 1) xs

ite :: Prop -> a -> a -> a
ite True a b = a
ite False a b = b

-- assert :: (Monad m) => Prop -> m ()
-- assert True = pure ()

swap :: N -> N -> [a] -> [a]
swap i j xs = set_at i (xs !! fi j) # set_at j (xs !! fi i) xs

lift :: (Monad m) => (a -> a) -> a -> m a
lift f x = pure # f x

get_avail :: (Ord a) => [a] -> Set a -> a
get_avail (x:xs) s = if Set.member x s then get_avail xs s else x

get_avail' :: (Ord a) => [a] -> Set a -> (a, Set a)
get_avail' xs s = let
  x = get_avail xs s
  in (x, Set.insert x s)

nop :: (Monad m) => m ()
nop = pure ()

fix :: (a -> a) -> a
fix f = f (fix f)

space :: Char
space = ' '

lift_idemp :: (a -> a) -> (a -> a) -> a -> a
lift_idemp tr f = tr . f . tr

reversed :: ([a] -> [a]) -> [a] -> [a]
reversed = lift_idemp reverse

trim_left' :: (Eq a) => a -> [a] -> [a]
trim_left' x = dropWhile (x==)

trim_right' :: (Eq a) => a -> [a] -> [a]
trim_right' x = reversed # trim_left' x

trim' :: (Eq a) => a -> [a] -> [a]
trim' x = trim_right' x . trim_left' x

trim_left :: String -> String
trim_left = trim_left' space

trim_right :: String -> String
trim_right = trim_right' space

trim :: String -> String
trim = trim' space

split_adv :: ([a] -> [a] -> Maybe N) -> [a] -> [[a]]
split_adv func list = split_adv' func 0 [] list [] []

split_adv' :: ([a] -> [a] -> Maybe N) -> N -> [a] -> [a] -> [a] -> [[a]] -> [[a]]
split_adv' func skip prev list sub acc = case list of
  [] -> case func prev list of
    Nothing -> reverse (reverse sub : acc)
    _ -> reverse ([] : reverse sub : acc)
  (x:xs) -> case skip of
    0 -> case func prev list of
      Nothing -> split_adv' func 0 (x : prev) xs (x : sub) acc
      Just len -> case len of
        0 -> split_adv' func 0 (x : prev) xs (x : []) (reverse sub : acc)
        n -> split_adv' func (n - 1) (x : prev) xs [] (reverse sub : acc)
    n -> split_adv' func (n - 1) (x : prev) xs sub acc

split_at_elem :: (Eq a) => a -> [a] -> [[a]]
split_at_elem z = split_adv # \xs ys -> case ys of
  [] -> Nothing
  (y:ys) -> ite (y == z) (Just 1) Nothing

split_at_list :: (Eq a) => [a] -> [a] -> [[a]]
split_at_list zs = let
  n = length zs
  jn = Just # fi n
  in split_adv # \xs ys -> let
    (ys1, ys2) = splitAt n ys
    in if ys1 == zs then jn else Nothing

nat_find :: (N -> Prop) -> N
nat_find p = nat_find' p 0

nat_find' :: (N -> Prop) -> N -> N
nat_find' p n = ite (p n) n # nat_find' p (n + 1)

guard' :: (Monad m) => Prop -> String -> m ()
guard' val msg = ite val (pure ()) (error msg)

nodup :: (Eq a) => [a] -> Prop
nodup xs = length xs == length (nub xs)

show_list :: (Show a) => [a] -> String
show_list xs = put_in_brackets # intercalate ", " # map show xs

show_set :: (Show a) => Set a -> String
show_set xs = put_in_braces # intercalate ", " # map show # Set.toList xs

paragraphs_aux :: [a] -> [a] -> [a]
paragraphs_aux s x = ite (null x) [] (s ++ x)

paragraphs' :: [a] -> [[a]] -> [a]
paragraphs' s [] = []
paragraphs' s (x:xs) = x ++ (xs >>= paragraphs_aux s)

paragraphs :: [String] -> String
paragraphs = paragraphs' "\n\n"

len :: [a] -> N
len xs = fi # length xs

list_get :: N -> [a] -> Maybe a
list_get i xs = ite (i >= 0 && i < len xs) (Just # xs !! fi i) Nothing

sanl :: String -> [String]
sanl = split_adv f where
  f _ ('\r':'\n':_) = Just 2
  f _ ('\r':_) = Just 1
  f _ ('\n':_) = Just 1
  f _ _ = Nothing

sanll :: String -> [String]
sanll = split_adv f where
  f _ ('\r':'\n':'\r':'\n':_) = Just 4
  f _ ('\r':'\n':'\r':_) = Just 3
  f _ ('\r':'\n':'\n':_) = Just 3
  f _ ('\r':'\r':'\n':_) = Just 3
  f _ ('\r':'\r':_) = Just 2
  f _ ('\n':'\r':'\n':_) = Just 3
  f _ ('\n':'\r':_) = Just 2
  f _ ('\n':'\n':_) = Just 2
  f _ _ = Nothing