module Elaborator where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe
import Data.List
import Control.Monad.State

import Base
import ParserBase
import Kernel
import Parser
import ElaboratorBase

run_cmd :: Cmd -> Theory Prop
run_cmd cmd = case cmd of
  Def name t e -> add_def name t e

run_cmds :: [Cmd] -> Theory ()
run_cmds [] = nop
run_cmds (cmd:cmds) = do
  True <- run_cmd cmd
  run_cmds cmds