import Prelude hiding (putStr, putStrLn, print, log)
import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Util
import Bit

sublists :: [a] -> [[a]]
sublists xs = case xs of
  [] -> [[]]
  (x:xs) -> do
    ys <- sublists xs
    [ys, x:ys]

dvd :: N -> N -> Prop
dvd k n = n `mod` k == 0

divisors :: N -> [N]
divisors n = filter (`dvd` n) [1 .. n]

is_practical :: N -> Prop
is_practical n = let
  sums = map sum # sublists # divisors n
  in all (`elem` sums) [2 .. n - 1]

main :: IO ()
main = do
  putStrLn # intercalate " " # map show #
    filter is_practical [1 .. 100]