import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Applicative
import Control.Monad
import Control.Monad.State
import System.Random as Rand

import Util
import Sampler

test_dir = "test/"
inp_file = test_dir ++ "inp.txt"
out_file = test_dir ++ "out.txt"

main :: IO ()
main = do
  str <- readFile inp_file
  
  res <- flip evalStateT undefined # do
    sampler_load 10 str
    sampler_gen rand # 10 ^ 4
  
  writeFile out_file res

rand :: N -> IO N
rand n = do
  g <- Rand.getStdGen
  (k, g) <- pure # Rand.random g
  Rand.setStdGen g
  k <- pure # fi # k + (minBound :: Int)
  return # mod k n