module Sampler where

import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Applicative
import Control.Monad
import Control.Monad.State

import Util

type SamplerMap0 a = Map [a] (Map a N)
type SamplerMap a = Map [a] [(a, N)]

data SamplerT a = SamplerT
  { _sampler_n :: N
  , _sampler_mp0 :: SamplerMap0 a
  , _sampler_mp :: SamplerMap a
  }

type Sampler m a = StateT (SamplerT a) m

sampler_get_mp0 :: (Monad m, Ord a) => Sampler m a (SamplerMap0 a)
sampler_get_mp0 = gets _sampler_mp0

sampler_put_mp0 :: (Monad m, Ord a) => SamplerMap0 a -> Sampler m a ()
sampler_put_mp0 mp = modify # \s -> s {_sampler_mp0 = mp}

sampler_get_mp :: (Monad m, Ord a) => Sampler m a (SamplerMap a)
sampler_get_mp = gets _sampler_mp

sampler_put_mp :: (Monad m, Ord a) => SamplerMap a -> Sampler m a ()
sampler_put_mp mp = modify # \s -> s {_sampler_mp = mp}

sampler_finish_mp' :: (Ord a) => Map a N -> [(a, N)]
sampler_finish_mp' mp = reverse # let
  xs = reverse # Map.toList mp
  in (\f -> foldr f [] xs) # \(a, n) xs -> case xs of
    [] -> [(a, n)]
    ((_, n') : _) -> (a, n + n') : xs

sampler_finish_mp :: (Ord a) => SamplerMap0 a -> SamplerMap a
sampler_finish_mp = Map.map sampler_finish_mp'

sampler_load' :: (Monad m, Ord a) => [a] -> Sampler m a ()
sampler_load' xs = do
  n <- gets _sampler_n
  let ys = take (fi # n + 1) xs
  if len ys <= n then nop else do
    let k = take (fi n) ys
    let next = last ys
    mp <- sampler_get_mp0
    insert <- pure # \vs -> sampler_put_mp0 # map_insert k vs mp
    case map_get' k mp of
      Nothing -> insert # Map.singleton next 1
      Just vs -> do
        insert' <- pure # \k -> insert # Map.insert next k vs
        case map_get' next vs of
          Nothing -> insert' 1
          Just k -> insert' # k + 1
    sampler_load' # tail xs

sampler_load :: (Monad m, Ord a) => N -> [a] -> Sampler m a ()
sampler_load n xs = do
  put # SamplerT
    { _sampler_n = n
    , _sampler_mp0 = Map.empty
    , _sampler_mp = undefined
    }
  if null xs then nop else do
    let xs_len = len xs
    let xs' = take (fi # n + xs_len) # concat # repeat xs
    sampler_load' xs'
  modify # \s -> s
    {_sampler_mp = sampler_finish_mp # _sampler_mp0 s}

sampler_gen' :: (Monad m, Ord a, Show a) => (N -> m N) -> N -> [a] -> Sampler m a [a]
sampler_gen' rand n xs = if n == 0 then pure [] else do
  mp <- sampler_get_mp
  let info = map_get xs mp
  let n_max = snd # last info
  i <- lift # rand n_max
  let Just (y, _) = find ((i <) . snd) info
  ys <- sampler_gen' rand (n - 1) # snoc (tail xs) y
  return # y : ys

sampler_gen :: (Monad m, Ord a, Show a) => (N -> m N) -> N -> Sampler m a [a]
sampler_gen rand n = do
  sn <- gets _sampler_n
  mp <- sampler_get_mp
  let ks = Map.keys mp
  let ks_len = len ks
  i <- lift # rand ks_len
  let xs = list_get i ks
  if n < sn then pure # take (fi n) xs else do
    ys <- sampler_gen' rand (n - sn) xs
    return # xs ++ ys