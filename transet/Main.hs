import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util
import Bit
import Parser

src_file = "src.txt"
inp_file = "inp.txt"

data SetT
  = Wcard
  | SetT (Set SetT)
  deriving (Eq, Ord)

show_set_t :: SetT -> String
show_set_t set = case set of
  Wcard -> "*"
  SetT set -> put_in_braces # intercalate ", " #
    map show_set_t # Set.toList set

instance Show SetT where
  show = show_set_t

data Rule = Rule
  { _rule_lhs :: SetT
  , _rule_rhs :: SetT
  , _rule_has_w :: Prop
  } deriving (Eq, Ord)

show_rule :: Rule -> String
show_rule rule = concat
  [ show # _rule_lhs rule, " -> "
  , show # _rule_rhs rule ]

instance Show Rule where
  show = show_rule

data ProgT = ProgT
  { _prog_rules :: [Rule]
  , _prog_set :: SetT
  , _prog_w_tok :: Maybe String
  } deriving (Eq, Ord, Show)

type Prog' = ParserM ProgT
type Prog = StateT ProgT (Except Sctr)

parens :: [(String, String)]
parens = init_parens "{}()[]<>"

w_toks :: [String]
w_toks = map pure # usc' : "?*#"

main :: IO ()
main = do
  src <- readFile src_file
  inp <- readFile inp_file
  case run_prog src inp of
    Left msg -> putStrLn # concat msg
    Right set -> putStrLn # show set

init_parens :: String -> [(String, String)]
init_parens cs = case cs of
  [] -> []
  (c1 : c2 : cs) -> ([c1], [c2]) : init_parens cs

init_prog :: ProgT
init_prog = ProgT
  { _prog_rules = []
  , _prog_set = undefined
  , _prog_w_tok = Nothing
  }

parse_w_tok_ident :: Prog' String
parse_w_tok_ident = trimmed # p_take_while1 is_letter

parse_w_tok :: Prog' String
parse_w_tok = choice (map p_tok' w_toks) <++ parse_w_tok_ident

parse_set_w :: Prop -> Prog' SetT
parse_set_w pw = do
  True <- pure pw
  w_tok <- gets _prog_w_tok
  case w_tok of
    Nothing -> do
      w_tok <- parse_w_tok
      modify # \prog -> prog {_prog_w_tok = Just w_tok}
    Just w_tok -> p_tok w_tok
  return Wcard

parse_set_parens :: Prop -> Prog' SetT
parse_set_parens pw = do
  s2 <- choice # flip map parens # \(s1, s2) -> do
    p_tok s1
    return s2
  xs <- p_sepf' (parse_set' pw) (p_tok ",")
  p_tok s2
  return # SetT # Set.fromList xs

parse_set' :: Prop -> Prog' SetT
parse_set' pw = choice
  [ parse_set_w pw
  , parse_set_parens pw
  ]

parse_set :: Prog' SetT
parse_set = parse_set' False

parse_set_with_w :: Prog' SetT
parse_set_with_w = parse_set' True

set_has_w :: SetT -> Prop
set_has_w set = case set of
  Wcard -> True
  SetT set -> any set_has_w # Set.toList set

parse_rule :: Prog' Rule
parse_rule = do
  lhs <- parse_set_with_w
  let has_w = set_has_w lhs
  p_tok "->"
  rhs <- parse_set_with_w
  rule <- pure # Rule
    { _rule_lhs = lhs
    , _rule_rhs = rhs
    , _rule_has_w = has_w
    }
  ifm (not has_w && set_has_w rhs) # err
    [ "Wildcard appears on the RHS, but not on the LHS:\n\n"
    , show rule ]
  modify # \prog -> prog {_prog_w_tok = Nothing}
  return rule

parse_prog :: Prog' ProgT
parse_prog = do
  rules <- trimmed # p_all' parse_rule
  gets # \prog -> prog {_prog_rules = rules}

parse_inp :: Prog' ProgT
parse_inp = do
  set <- parse_set
  gets # \prog -> prog {_prog_set = set}

set_get_all_sets :: SetT -> Set SetT
set_get_all_sets set@(SetT set') = Set.insert set #
  Set.unions # Set.map set_get_all_sets set'

set_subst_w :: SetT -> SetT -> SetT
set_subst_w x set = case set of
  Wcard -> x
  SetT set -> SetT # Set.map (set_subst_w x) set

apply_rule' :: SetT -> Rule -> Maybe SetT
apply_rule' set rule = do
  let lhs = _rule_lhs rule
  let rhs = _rule_rhs rule
  if _rule_has_w rule then do
    sets <- pure # Set.toList # set_get_all_sets set
    (x : _) <- pure # flip filter sets # \x ->
      set_subst_w x lhs == set
    return # set_subst_w x rhs
  else do
    True <- pure # lhs == set
    return rhs

apply_rule :: SetT -> Rule -> Maybe SetT
apply_rule set rule = case apply_rule' set rule of
  Just set -> pure set
  Nothing -> do
    SetT set <- pure set
    let sets = Set.toList set
    (x, y) <- fst_just # flip map sets # \x -> do
      y <- apply_rule x rule
      return (x, y)
    return # SetT # Set.insert y # Set.delete x set

apply_rules :: SetT -> [Rule] -> Maybe SetT
apply_rules set rules = fst_just # map (apply_rule set) rules

run_prog' :: Prog SetT
run_prog' = do
  rules <- gets _prog_rules
  set <- gets _prog_set
  case apply_rules set rules of
    Nothing -> pure set
    Just set -> do
      modify # \prog -> prog {_prog_set = set}
      run_prog'

run_prog :: String -> String -> Either Sctr SetT
run_prog src inp = do
  prog <- parse parse_prog init_prog src
  prog <- parse parse_inp prog inp
  runExcept # evalStateT run_prog' prog