module Unit
  ( Unit
  , unit
  , u
  ) where

import qualified Prelude as P

data Unit = Unit
  deriving (P.Eq, P.Ord, P.Show)

unit :: Unit
unit = Unit

---------------------------------------------------------------------------------------------------------------------------------------

u :: Unit
u = unit