module Int
  ( Int
  , int_zero
  , int_pred
  , int_succ
  , int_exa
  , int_lit
  ) where

import qualified Prelude as P

import Nat

data Int = IntNat Nat | IntNeg Nat
  deriving (P.Eq, P.Ord, P.Show)

int_zero :: Nat -> Int
int_zero = IntNat nat_zero

int_pred :: Nat -> Int
int_pred (IntNat n) = nat_exa

int_exa :: (Nat -> a) -> (Nat -> a) -> Int -> a
int_exa f g (IntNat n) = f n
int_exa f g (IntNeg n) = g n

int_lit :: (P.Num a, P.Ord a) => a -> Int
int_lit n = if (P.>=) n 0
  then int_mk_nat (nat_lit n)
  else int_mk_neg (nat_lit ((P.-) (P.abs n) 1))

---------------------------------------------------------------------------------------------------------------------------------------