module Func
  ( Func
  , comb_k
  , comb_s
  , id
  , const
  , (.)
  , flip
  ) where

import qualified Prelude as P

type Func = (->)

comb_k :: a -> b -> a
comb_k a b = a

comb_s :: (a -> b -> c) -> (a -> b) -> a -> c
comb_s a b c = a c (b c)

---------------------------------------------------------------------------------------------------------------------------------------

id :: a -> a
id a = a

const :: a -> b -> a
const = comb_k

infixr 9 .
(.) :: (b -> c) -> (a -> b) -> a -> c
(.) f g a = f (g a)

flip :: (a -> b -> c) -> b -> a -> c
flip f a b = f b a
