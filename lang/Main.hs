{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}

import Data.Char

import PList

import qualified Func
import qualified Nat
import qualified List
import qualified Char
import qualified String

import qualified Examples.P00001.Program as Prog
import qualified Examples.P00001.Test as Test

main :: IO ()
main = do
  if run_tests Prog.main Test.test
    then putStrLn "ok"
    else putStrLn "ERROR"

class CTest a b where
  run_test :: a -> b -> Bool

instance (Eq a, Show a) => CTest a (PList '[a]) where
  run_test p (PCons t _) = if t == p
    then True
    else error $ unlines ["", show t, show p]

instance (CTest b (PList c)) => CTest (a -> b) (PList (a ': c)) where
  run_test p (PCons t r) = run_test (p t) r

run_tests :: (CTest a b) => a -> [b] -> Bool
run_tests = all . run_test