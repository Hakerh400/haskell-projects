{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module PList
  ( PList(..)
  , (#)
  ) where

data PList a where
  PNil  :: PList '[]
  PCons :: a -> PList b -> PList (a ': b)

infixr 5 #
(#) :: a -> PList b -> PList (a ': b)
(#) = PCons