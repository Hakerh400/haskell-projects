import qualified Data.Set as Set
import qualified Data.Map as Map

import Util
import Parser

src_file = "src.txt"

data ProgT = ProgT
  { _prog_mem :: Map N N
  , _prog_cmds :: [Cmd]
  , _prog_ip :: N
  } deriving (Eq, Ord, Show)

type ProgP = ParserM ProgT
type Prog = StateT ProgT (Except Sctr)

data Cmd
  = CmdNil Value
  | CmdInc Value
  | CmdLab Value
  | CmdJmp Value Value Value
  deriving (Eq, Ord, Show)

data Value = Value
  { _val_ptrs :: N
  , _val_addr :: N
  } deriving (Eq, Ord, Show)

main :: IO ()
main = do
  src <- readFile src_file
  case parse_and_run_prog src of
    Left msg -> putStrLn # concat msg
    Right res -> putStrLn # unwords # map show res

init_prog :: [Cmd] -> ProgT
init_prog cmds = ProgT
  { _prog_mem = Map.empty
  , _prog_cmds = cmds
  , _prog_ip = 0
  }

parse_val :: ProgP Value
parse_val = do
  ptrs <- p_all' # p_tok "*"
  addr <- p_nat
  return # Value (len ptrs) addr

parse_ptr :: ProgP Value
parse_ptr = do
  val <- parse_val
  True <- pure # _val_ptrs val /= 0
  return val

parse_cmd_nil :: ProgP Cmd
parse_cmd_nil = do
  val <- parse_ptr
  p_tok "-"
  return # CmdNil val

parse_cmd_inc :: ProgP Cmd
parse_cmd_inc = do
  val <- parse_ptr
  p_tok "+"
  return # CmdInc val

parse_cmd_lab :: ProgP Cmd
parse_cmd_lab = do
  val <- parse_val
  p_tok ";"
  return # CmdLab val

parse_cmd_jmp :: ProgP Cmd
parse_cmd_jmp = do
  [x, y, lab] <- replicateM 3 parse_val
  p_tok "?"
  return # CmdJmp x y lab

parse_cmd :: ProgP Cmd
parse_cmd = trimmed # choice
  [ parse_cmd_nil
  , parse_cmd_inc
  , parse_cmd_lab
  , parse_cmd_jmp
  ]

parse_prog :: ProgP ProgT
parse_prog = do
  cmds <- trimmed # p_all' parse_cmd
  return # init_prog cmds

get_result :: Prog [N]
get_result = do
  mem <- gets _prog_mem
  mem <- pure # Map.filter (/= 0) mem
  let mx = inc # last' (-1) # Map.keys mem
  return # mk_list mx # Map.findWithDefault 0 <~ mem

inc_ip :: Prog ()
inc_ip = modify # \prog -> prog
  {_prog_ip = _prog_ip prog + 1}

mem_get :: N -> Prog N
mem_get addr = do
  mem <- gets _prog_mem
  return # Map.findWithDefault 0 addr mem

mem_set :: N -> N -> Prog ()
mem_set addr val = do
  mem <- gets _prog_mem
  modify # \prog -> prog
    {_prog_mem = map_insert addr val mem}

mem_modify :: N -> (N -> N) -> Prog ()
mem_modify addr f = do
  val <- mem_get addr
  mem_set addr # f val

deref :: N -> N -> N -> Prog N
deref target ptrs addr =
  if ptrs == target then pure addr else do
    addr <- mem_get addr
    deref target (ptrs - 1) addr

get_val :: Value -> Prog N
get_val (Value ptrs addr) = deref 0 ptrs addr

get_addr :: Value -> Prog N
get_addr (Value ptrs addr) = deref 1 ptrs addr

get_lab_ip' :: [(N, Value)] -> N -> Prog N
get_lab_ip' xs n = case xs of
  [] -> err ["No dynamic label with value ", show n]
  ((i, val) : xs) -> do
    n' <- get_val val
    if n == n' then pure i else get_lab_ip' xs n

get_lab_ip :: N -> Prog N
get_lab_ip n = do
  cmds <- gets _prog_cmds
  xs <- pure # do
    (i, CmdLab lab) <- zip [0..] cmds
    return (i, lab)
  get_lab_ip' xs n

run_cmd :: Cmd -> Prog ()
run_cmd cmd = inc_ip >> case cmd of
  CmdNil ptr -> do
    addr <- get_addr ptr
    mem_set addr 0
  CmdInc ptr -> do
    addr <- get_addr ptr
    mem_modify addr inc
  CmdJmp x y lab -> do
    [x, y, lab] <- mapM get_val [x, y, lab]
    ifm (x == y) # do
      ip <- get_lab_ip lab
      modify # \prog -> prog {_prog_ip = ip}
  _ -> nop

run_prog :: Prog [N]
run_prog = do
  prog <- get
  let cmds = _prog_cmds prog
  let ip = _prog_ip prog
  if ip == len cmds then get_result else do
    let Just cmd = list_get' ip cmds
    run_cmd cmd
    run_prog

parse_and_run_prog :: String -> Either Sctr [N]
parse_and_run_prog src = do
  prog <- parse parse_prog src
  runExcept # evalStateT run_prog prog