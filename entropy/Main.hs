import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)
import Data.IORef

import Util
import Bit
import Rand
import Entropy

main :: IO ()
main = do
  let z = (dflt :: Entropy)
  
  let a = entropy_fst z
  let b = entropy_snd z
  let c = entropy_pair a b
  
  let sz = take 100 # show z
  let sa = take 100 # show a
  let sb = take 100 # show b
  let sc = take 100 # show c
  
  s <- pure # take 100 # do
    (c1, c2) <- zip sa sb
    [c1, c2]
  
  putStrLn sz
  putStrLn sa
  putStrLn sb
  
  putStrLn ""
  putStrLn sz
  putStrLn sc
  putStrLn s
  
  putStrLn ""
  print # c == z