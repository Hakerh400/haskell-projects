module Entropy
  ( Entropy
  , entropy_bit
  , entropy_pair
  , entropy_fst
  , entropy_snd
  ) where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine, succ)
import System.IO hiding (print, putStrLn, getLine)

import Util
import Bit
import Rand

data E
  = Z
  | A N E
  | P E E
  | L E
  | R E
  deriving (Eq, Ord, Show)

newtype Entropy' a = Entropy a
  deriving (Functor, Eq)

type Entropy = Entropy' E

simp :: E -> E
simp e = case e of
  A 0 e -> e
  A n (A m e) -> A (n + m) e
  A n (P x y) -> let k = div n 2 in simp # if even n
    then P (add k x) (add k y)
    else P (add k y) (add (k + 1) x)
  A n (L x) -> simp # L # add (n * 2) x
  A n (R x) -> simp # R # add (n * 2) x
  P (L x) (R x') -> ite (x == x') x e
  L (P x y) -> x
  R (P x y) -> y
  _ -> e

add :: N -> E -> E
add n e = simp # A n e

succ :: E -> E
succ = add 1

to_nat :: E -> N
to_nat e = case e of
  Z -> 0
  A n e -> n + to_nat e
  P a b -> to_nat a
  L e -> to_nat e
  R e -> 1 + to_nat e

get_bit :: E -> (Bit, E)
get_bit e = (rand_bit # to_nat e, succ e)

entropy_bit :: Entropy -> (Bit, Entropy)
entropy_bit (Entropy e) = case get_bit e of
  (b, e) -> (b, Entropy e)

entropy_pair :: Entropy -> Entropy -> Entropy
entropy_pair (Entropy a) (Entropy b) = Entropy # simp # P a b

entropy_fst :: Entropy -> Entropy
entropy_fst (Entropy e) = Entropy # simp # L e

entropy_snd :: Entropy -> Entropy
entropy_snd (Entropy e) = Entropy # simp # R e

to_list :: E -> [Bit]
to_list e = case get_bit e of
  (b, e) -> b : to_list e

instance Inhabited Entropy where
  dflt = Entropy Z

instance Show Entropy where
  show (Entropy e) = show # to_list e

instance Ord Entropy where
  compare (Entropy a) (Entropy b) =
    ite (a == b) EQ # compare (to_list a) (to_list b)