module Rand
  ( rand_bit
  ) where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)
import Data.IORef

import Util
import Bit

h_dev_rand :: Handle
h_dev_rand = unsafePerformIO #
  openFile "/dev/random" ReadMode

mp_ref :: (Ord a) => IORef (Map a Bit)
mp_ref = unsafePerformIO # newIORef Map.empty

rand_bit' :: (Ord a) => a -> IO Bit
rand_bit' a = do
  () <- seq a # pure ()
  (byte : _) <- fmap BS.unpack # BS.hGet h_dev_rand 1
  return # prop_to_bit # odd byte

rand_bit :: (Ord a) => a -> Bit
rand_bit a = unsafePerformIO # do
  mp <- readIORef mp_ref
  case map_get a mp of
    Just b -> pure b
    Nothing -> do
      b <- rand_bit' a
      writeIORef mp_ref # map_insert a b mp
      return b