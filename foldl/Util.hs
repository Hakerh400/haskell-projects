{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Util
  ( module Util
  , module Set
  , module Map
  , module Data.Char
  , module Data.List
  , module Data.Maybe
  , module Data.Either
  , module Data.Bits
  , module Data.Word
  , module Data.Functor.Identity
  , module Control.Applicative
  , module Control.Monad
  , module Control.Monad.State
  , module Control.Monad.Except
  , module System.IO.Unsafe
  ) where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.Trans.Maybe
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import System.IO.Unsafe

infixr 0 #
(#) = id

infixr 1 <~
(<~) = flip

type N = Integer
type Prop = Bool
type Name = String
type Byte = Word8
type Buf = BS.ByteString

instance MonadFail Identity where
  fail = error

-- escape_char :: Char -> String
-- escape_char c = '.' : show (ord c)
-- 
-- escape_str :: String -> String
-- escape_str str = str --str >>= escape_char
-- 
-- putStr :: String -> IO ()
-- putStr str = Prelude.putStr # escape_str str
-- 
-- putStrLn :: String -> IO ()
-- putStrLn str = putStr # str ++ "\n"
-- 
-- print :: (Show a) => a -> IO ()
-- print a = putStrLn # show a

tab_size :: N
tab_size = 2

tab :: String
tab = replicate (fi tab_size) ' '

indent :: N -> String -> String
indent n str = concat (replicate (fi n) tab) ++ str

indent' :: N -> String
indent' n = indent n ""

put_in' :: a -> a -> [a] -> [a]
put_in' a b xs = [a] ++ xs ++ [b]

put_in :: [a] -> [a] -> [a] -> [a]
put_in a b xs = a ++ xs ++ b

put_in2 :: [a] -> [a] -> [a]
put_in2 a xs = put_in a a xs

put_in_list :: [[a]] -> [a] -> [a]
put_in_list list xs = concat
  [concat list, xs, concat # reverse list]

put_in_parens :: String -> String
put_in_parens = put_in' '(' ')'

put_in_brackets :: String -> String
put_in_brackets = put_in' '[' ']'

put_in_braces :: String -> String
put_in_braces = put_in' '{' '}'

put_in_parens' :: Prop -> String -> String
put_in_parens' p s = if p then put_in_parens s else s

fi :: (Integral a, Num b) => a -> b
fi = fromIntegral

unlines' :: [String] -> String
unlines' = intercalate "\n"

dbg :: (Show a) => a -> b
dbg a = trace' a # error ""

allM :: (Monad m) => (a -> m Prop) -> [a] -> m Prop
allM f = foldM (allM' f) True

allM' :: (Monad m) => (a -> m Prop) -> Prop -> a -> m Prop
allM' f p x = if p then f x else pure False

set_at :: N -> a -> [a] -> [a]
set_at 0 z (x:xs) = z : xs
set_at n z (x:xs) = x : set_at (n - 1) z xs

modify_at :: N -> (a -> a) -> [a] -> [a]
modify_at 0 f (x:xs) = f x : xs
modify_at n f (x:xs) = x : modify_at (n - 1) f xs

insert_at :: N -> a -> [a] -> [a]
insert_at 0 z xs = z : xs
insert_at n z (x:xs) = x : insert_at (n - 1) z xs

append_at :: N -> [a] -> [a] -> [a]
append_at 0 zs xs = zs ++ xs
append_at n zs (x:xs) = x : append_at (n - 1) zs xs

remove_at :: N -> [a] -> [a]
remove_at 0 (x:xs) = xs
remove_at n (x:xs) = x : remove_at (n - 1) xs

list_move :: N -> N -> [a] -> [a]
list_move i j xs = let
  (xs1, x : xs2) = splitAt (fi i) xs
  ys = xs1 ++ xs2
  (ys1, ys2) = splitAt (fi j) ys
  in ys1 ++ x : ys2

ite :: Prop -> a -> a -> a
ite True a b = a
ite False a b = b

ite' :: Prop -> a -> a -> a
ite' False a b = a
ite' True a b = b

-- assert :: (Monad m) => Prop -> m ()
-- assert True = pure ()

swap :: (a, b) -> (b, a)
swap (a, b) = (b, a)

list_swap :: N -> N -> [a] -> [a]
list_swap i j xs = set_at i (xs !! fi j) # set_at j (xs !! fi i) xs

liftm :: (Monad m) => (a -> b) -> a -> m b
liftm f x = pure # f x

liftm2 :: (Monad m) => (a -> b -> c) -> a -> b -> m c
liftm2 f x y = pure # f x y

set_insert :: (Ord k) => k -> Set k -> Set k
set_insert = Set.insert

set_insert' :: (Ord k, MonadError e m) => k -> Set k -> e -> m (Set k)
set_insert' k set msg = if Set.member k set
  then raise msg
  else pure # set_insert k set

map_insert :: (Ord k) => k -> v -> Map k v -> Map k v
map_insert = Map.insert

map_insert' :: (Ord k) => k -> v -> Map k v -> Maybe (Map k v)
map_insert' k v mp = do
  False <- pure # Map.member k mp
  return # map_insert k v mp

get_avail :: (Ord a) => [a] -> Set a -> a
get_avail (x:xs) s = if Set.member x s then get_avail xs s else x

get_avail' :: (Ord a) => [a] -> Set a -> (a, Set a)
get_avail' xs s = let
  x = get_avail xs s
  in (x, set_insert x s)

nop :: (Monad m) => m ()
nop = pure ()

fix :: (a -> a) -> a
fix f = f (fix f)

space :: Char
space = ' '

lift_idemp :: (a -> a) -> (a -> a) -> a -> a
lift_idemp tr f = tr . f . tr

reversed :: ([a] -> [a]) -> [a] -> [a]
reversed = lift_idemp reverse

trim_left' :: (Eq a) => a -> [a] -> [a]
trim_left' x = dropWhile (x==)

trim_right' :: (Eq a) => a -> [a] -> [a]
trim_right' x = reversed # trim_left' x

trim' :: (Eq a) => a -> [a] -> [a]
trim' x = trim_right' x . trim_left' x

trim_left :: String -> String
trim_left = trim_left' space

trim_right :: String -> String
trim_right = trim_right' space

trim :: String -> String
trim = trim' space

split_adv' :: ([a] -> [a] -> Maybe N) -> N -> [a] -> [a] -> [a] -> [[a]] -> [[a]]
split_adv' func skip prev list sub acc = case list of
  [] -> case func prev list of
    Nothing -> reverse (reverse sub : acc)
    _ -> reverse ([] : reverse sub : acc)
  (x:xs) -> case skip of
    0 -> case func prev list of
      Nothing -> split_adv' func 0 (x : prev) xs (x : sub) acc
      Just len -> case len of
        0 -> split_adv' func 0 (x : prev) xs (x : []) (reverse sub : acc)
        n -> split_adv' func (n - 1) (x : prev) xs [] (reverse sub : acc)
    n -> split_adv' func (n - 1) (x : prev) xs sub acc

split_adv :: ([a] -> [a] -> Maybe N) -> [a] -> [[a]]
split_adv func list = split_adv' func 0 [] list [] []

split_at_elem :: (Eq a) => a -> [a] -> [[a]]
split_at_elem z = split_adv # \xs ys -> case ys of
  [] -> Nothing
  (y:ys) -> ite (y == z) (Just 1) Nothing

split_at_list :: (Eq a) => [a] -> [a] -> [[a]]
split_at_list zs = let
  n = length zs
  jn = Just # fi n
  in split_adv # \xs ys -> let
    (ys1, ys2) = splitAt n ys
    in if ys1 == zs then jn else Nothing

nat_find :: (N -> Prop) -> N
nat_find p = nat_find' p 0

nat_find' :: (N -> Prop) -> N -> N
nat_find' p n = ite (p n) n # nat_find' p (n + 1)

guard' :: (Monad m) => Prop -> String -> m ()
guard' val msg = ite val (pure ()) (error msg)

nodup :: (Eq a) => [a] -> Prop
nodup xs = length xs == length (nub xs)

show_list :: (a -> String) -> [a] -> String
show_list f xs = put_in_brackets # intercalate ", " # map f xs

show_list' :: (a -> String) -> [a] -> String
show_list' f xs = case xs of
  [a] -> f a
  _ -> show_list f xs

show_set :: (a -> String) -> Set a -> String
show_set f xs = put_in_braces # intercalate ", " # map f # Set.toList xs

-- paragraphs_aux :: [a] -> [a] -> [a]
-- paragraphs_aux s x = ite (null x) [] (s ++ x)
-- 
-- paragraphs' :: [a] -> [[a]] -> [a]
-- paragraphs' s [] = []
-- paragraphs' s (x:xs) = x ++ (xs >>= paragraphs_aux s)

-- paragraphs :: [String] -> String
-- paragraphs = paragraphs' "\n\n"

paragraphs :: [String] -> String
paragraphs xs = intercalate "\n\n" # filter (not . null) xs

len :: (Integral n) => [a] -> n
len xs = fi # length xs

list_get' :: N -> [a] -> Maybe a
list_get' _ [] = Nothing
list_get' 0 (x:xs) = Just x
list_get' i (x:xs) = list_get' (i - 1) xs

list_get :: N -> [a] -> a
list_get i xs = fromJust # list_get' i xs

map_get' :: (Ord k) => k -> Map k v -> Maybe v
map_get' = Map.lookup

map_get :: (Ord k) => () -> k -> Map k v -> v
map_get u k mp = case map_get' k mp of
  Nothing -> case u of
    () -> error ""
  Just v -> v

sanl :: String -> [String]
sanl = split_adv f where
  f _ ('\r':'\n':_) = Just 2
  f _ ('\r':_) = Just 1
  f _ ('\n':_) = Just 1
  f _ _ = Nothing

sanll :: String -> [String]
sanll = split_adv f where
  f _ ('\r':'\n':'\r':'\n':_) = Just 4
  f _ ('\r':'\n':'\r':_) = Just 3
  f _ ('\r':'\n':'\n':_) = Just 3
  f _ ('\r':'\r':'\n':_) = Just 3
  f _ ('\r':'\r':_) = Just 2
  f _ ('\n':'\r':'\n':_) = Just 3
  f _ ('\n':'\r':_) = Just 2
  f _ ('\n':'\n':_) = Just 2
  f _ _ = Nothing

show_maybe :: (Show a) => Maybe a -> String
show_maybe = maybe "/" show

is_letter :: Char -> Prop
is_letter c = let c1 = toLower c in c1 >= 'a' && c1 <= 'z'

is_digit :: Char -> Prop
is_digit = isDigit

is_sub_digit :: Char -> Prop
is_sub_digit c = c >= '₀' && c <= '₉'

greek_alpha_low :: Char
greek_alpha_low = '\945'

greek_omega_low :: Char
greek_omega_low = '\969'

is_greek :: Char -> Prop
is_greek c = let
  c1 = toLower c
  in c1 >= greek_alpha_low && c1 <= greek_omega_low

greek_letters_low :: String
greek_letters_low = [greek_alpha_low .. greek_omega_low]

mapi :: (N -> a -> b) -> [a] -> [b]
mapi f = zipWith f [0..]

show_either :: (a -> String) -> (b -> String) -> Either a b -> String
show_either f g a = case a of
  Left a -> f a
  Right a -> g a

show_either' :: (Show a, Show b) => Either a b -> String
show_either' = show_either show show

from_left :: Either a b -> a
from_left (Left a) = a

from_right :: Either a b -> b
from_right (Right a) = a

mk_pair :: a -> b -> (a, b)
mk_pair a b = (a, b)

mk_pair' :: b -> a -> (a, b)
mk_pair' = flip mk_pair

usc' :: Char
usc' = '\x5F'

usc :: String
usc = [usc']

either_to_maybe :: Either a b -> Maybe b
either_to_maybe e = case e of
  Left _ -> Nothing
  Right a -> Just a

find_indices :: (a -> Prop) -> [a] -> [N]
find_indices f xs = map snd # filter fst # mapi (\i x -> (f x, i)) xs

mk_list' :: (N -> a) -> N -> N -> [a]
mk_list' f n i = if i == n then [] else
  f i : mk_list' f n (i + 1)

mk_list :: N -> (N -> a) -> [a]
mk_list n f = mk_list' f n 0

scanM :: (Monad m) => (b -> a -> m b) -> b -> [a] -> m [b]
scanM f z xs = case xs of
  [] -> pure []
  (x:xs) -> do
    y <- f z x
    ys <- scanM f y xs
    return # y : ys

for :: (a -> a) -> a -> N -> a
for f z n = if n == 0 then z else f # for f z # n - 1

seq_to_list' :: (N -> a) -> N -> [a]
seq_to_list' f n = f n : seq_to_list' f (n + 1)

seq_to_list :: (N -> a) -> [a]
seq_to_list f = seq_to_list' f 0

get_avail_of_set' :: (Ord a) => [a] -> Set a -> a
get_avail_of_set' (x:xs) set = if Set.member x set
  then get_avail_of_set' xs set else x

get_avail_of_set :: (Ord a) => (N -> a) -> Set a -> a
get_avail_of_set f set = get_avail_of_set' (seq_to_list f) set

cons_head :: a -> [[a]] -> [[a]]
cons_head x (y:ys) = (x : y) : ys

pad_start :: N -> a -> [a] -> [a]
pad_start n z xs = let n' = len xs in
  if n' < n then replicate (fi # n - n') z ++ xs else xs

pad_end :: N -> a -> [a] -> [a]
pad_end n z xs = let n' = len xs in
  if n' < n then xs ++ replicate (fi # n - n') z else xs

split_into_groups :: [N] -> [a] -> [[a]]
split_into_groups ns xs = case ns of
  [] -> if null xs then [] else undefined
  (n:ns) -> if n == 0
    then [] : split_into_groups ns xs
    else case xs of
      [] -> undefined
      (x:xs) -> cons_head x # split_into_groups (n - 1 : ns) xs

split_into_same_groups' :: ([a] -> b) -> N -> [a] -> [b]
split_into_same_groups' f n xs = if null xs then [] else
  case splitAt (fi n) xs of
    (xs, ys) -> f xs : split_into_same_groups' f n ys

split_into_same_groups :: N -> [a] -> [[a]]
split_into_same_groups = split_into_same_groups' id

split_into_same_groups_pad_start :: N -> a -> [a] -> [[a]]
split_into_same_groups_pad_start n z xs =
  split_into_same_groups' (pad_start n z) n xs

split_into_same_groups_pad_end :: N -> a -> [a] -> [[a]]
split_into_same_groups_pad_end n z xs =
  split_into_same_groups' (pad_end n z) n xs

iter_st :: (a -> (b, a)) -> a -> [b]
iter_st f z = let (x, y) = f z in
  x : iter_st f y

iter_st_until :: (a -> Prop) -> (a -> (b, a)) -> a -> [b]
iter_st_until p f z = if p z then [] else let (x, y) = f z in
  x : iter_st_until p f y

common_prefix :: (Eq a) => [a] -> [a] -> ([a], [a], [a])
common_prefix xs0@(x:xs) ys0@(y:ys) = if x == y
  then let (a, b, c) = common_prefix xs ys in (x : a, b, c)
  else ([], xs0, ys0)
common_prefix xs ys = ([], xs, ys)

raise :: (MonadError e m) => e -> m a
raise = throwError

try :: (MonadError e m) => m a -> m (Either e a)
try m = catchError (m >>= liftm Right) # liftm Left

maximum' :: (Num a, Ord a) => [a] -> a
maximum' xs = maximum # 0 : xs

fst_just' :: Maybe a -> Maybe a -> Maybe a
fst_just' m1 m2 = case m1 of
  Nothing -> m2
  _ -> m1

fst_just :: [Maybe a] -> Maybe a
fst_just xs = case xs of
  [] -> Nothing
  (x:xs) -> case x of
    Just _ -> x
    Nothing -> fst_just xs

equiv_classes' :: (a -> a -> Prop) -> [a] -> [[a]]
equiv_classes' f xs = case xs of
  [] -> []
  (x:xs) -> let
    cs = equiv_classes' f xs
    (cs1, cs2) = partition (any # f x) cs
    in (x : concat cs1) : cs2

equiv_classes :: (Ord a) => (a -> a -> Prop) -> Set a -> Set (Set a)
equiv_classes f set = Set.fromList # map Set.fromList #
  equiv_classes' f # Set.toList set

head' :: a -> [a] -> a
head' z xs = if null xs then z else head xs

last' :: a -> [a] -> a
last' z xs = if null xs then z else last xs

tail' :: [a] -> [a]
tail' xs = if null xs then [] else tail xs

init' :: [a] -> [a]
init' xs = if null xs then [] else init xs

headm :: [a] -> Maybe a
headm xs = case xs of
  [] -> Nothing
  (x:_) -> Just x

lastm :: [a] -> Maybe a
lastm xs = if null xs then Nothing else Just # last xs

-- set_to_map :: (Ord k, Ord a) => (a -> k) -> Set a -> Map k a
-- set_to_map f set = Map.fromList # map (\a -> (f a, a)) # Set.toList set

set_fst' :: (Ord a) => Set a -> a
set_fst' = Set.elemAt 0

set_fst :: (Ord a) => Set a -> Maybe a
set_fst set = if Set.null set then Nothing else
  Just # set_fst' set

map_fst' :: (Ord k) => Map k a -> (k, a)
map_fst' = Map.elemAt 0

map_fst :: (Ord k) => Map k a -> Maybe (k, a)
map_fst mp = if Map.null mp then Nothing else
  Just # map_fst' mp

map_fst_key' :: (Ord k) => Map k a -> k
map_fst_key' = fst . Map.elemAt 0

map_fst_key :: (Ord k) => Map k a -> Maybe k
map_fst_key mp = if Map.null mp then Nothing else
  Just # map_fst_key' mp

map_fst_val' :: (Ord k) => Map k a -> a
map_fst_val' = snd . Map.elemAt 0

map_fst_val :: (Ord k) => Map k a -> Maybe a
map_fst_val mp = if Map.null mp then Nothing else
  Just # map_fst_val' mp

remove_empty_lists :: [[a]] -> [[a]]
remove_empty_lists = filter # not . null

is_line_empty :: String -> Prop
is_line_empty line = case line of
  "" -> True
  (' ':line) -> is_line_empty line
  _ -> False

remove_empty_lines :: String -> String
remove_empty_lines str = unlines' # filter (not . is_line_empty) # lines str

trim_start :: (Eq a) => a -> [a] -> [a]
trim_start z xs = case xs of
  [] -> []
  (x:xs') -> if x == z then trim_start z xs' else xs

trim_end :: (Eq a) => a -> [a] -> [a]
trim_end z xs = case xs of
  [] -> []
  (x:xs') -> case trim_end z xs' of
    [] -> if x == z then [] else [x]
    ys -> x : ys

class ShallowOrd a where
  shallow_compare :: a -> a -> Ordering

mk_sorted_pair :: (Ord a) => a -> a -> (a, a)
mk_sorted_pair a b = if compare a b /= GT then (a, b) else (b, a)

mk_shallow_sorted_pair :: (ShallowOrd a) => a -> a -> (a, a)
mk_shallow_sorted_pair a b = if shallow_compare a b /= GT then (a, b) else (b, a)

newtype Lit = Lit [String]

instance Show Lit where
  show (Lit xs) = concat xs

class Inhabited a where
  dflt :: a

instance Inhabited (Maybe a) where
  dflt = Nothing

instance Inhabited (Set a) where
  dflt = Set.empty

instance Inhabited (Map a b) where
  dflt = Map.empty

from_maybe :: (Inhabited a) => Maybe a -> a
from_maybe = fromMaybe dflt

show_map' :: (Ord k) => (k -> String) -> (v -> String) -> k -> v -> (String, String)
show_map' f g k v = (f k, g v)

show_map :: (Ord k) => (k -> String) -> (v -> String) -> Map k v -> String
show_map f g mp = let
  ps = map (uncurry # show_map' f g) # Map.toList mp
  n = maximum' # map (len . fst) ps
  in unlines' # flip map ps # \(sk, sv) -> concat
    [pad_end n space sk, " ---> ", sv]

trace_str :: String -> a -> a
trace_str str a = unsafePerformIO # do
  putStrLn str
  return a

trace_show :: (Show a) => a -> b -> b
trace_show a b = unsafePerformIO # do
  print a
  return b

trace' :: (Show a) => a -> b -> b
trace' = trace_show

trace :: (Show a) => a -> a
trace a = trace' a a

tracem :: (Monad m, Show a) => a -> m ()
tracem a = do
  () <- trace' a nop
  nop

tracem' :: (Monad m, Show a) => m a -> m a
tracem' m = do
  a <- m
  tracem a
  return a

trace_with :: (a -> String) -> a -> a
trace_with f a = trace_str (f a) a

replace_with_map :: (Ord a) => Map a a -> a -> a
replace_with_map mp a = Map.findWithDefault a a mp

replace_with_map_in_list :: (Ord a) => Map a a -> [a] -> [a]
replace_with_map_in_list mp xs = map (replace_with_map mp) xs

snd_just :: Maybe a -> Maybe a -> Maybe a
snd_just m1 m2 = case m2 of
  Nothing -> m1
  _ -> m2

maybe_to_list :: Maybe a -> [a]
maybe_to_list x = case x of
  Nothing -> []
  Just x -> [x]

list_to_maybe :: [a] -> Maybe a
list_to_maybe xs = case xs of
  [] -> Nothing
  (x : _) -> Just x

flip_pair :: (a, b) -> (b, a)
flip_pair (a, b) = (b, a)

flip_map :: (Ord a, Ord b) => Map a b -> Map b a
flip_map mp = Map.fromList # map flip_pair # Map.toList mp

quote :: String -> String
quote = put_in2 "\""

show_name :: Name -> String
show_name = quote

lift2 :: (MonadTrans t1, MonadTrans t2, Monad m, Monad (t2 m)) =>
  m a -> t1 (t2 m) a
lift2 = lift . lift

starts_with :: (Eq a) => [a] -> [a] -> Prop
starts_with [] _ = True
starts_with (x:xs) (y:ys) = x == y && starts_with xs ys
starts_with _ _ = False

list_to_map :: [a] -> Map N a
list_to_map xs = Map.fromList # zip [0..] xs

list_to_map_inv :: (Ord a) => [a] -> Map a N
list_to_map_inv xs = Map.fromList # zip xs [0..]

map_insert_list :: (Ord k) => k -> v -> Map k [v] -> Map k [v]
map_insert_list k v mp = let
  vs = v : fromMaybe [] (Map.lookup k mp)
  in map_insert k vs mp

list_to_map_list :: (Ord k) => [(k, v)] -> Map k [v]
list_to_map_list xs = case xs of
  [] -> Map.empty
  ((k, v) : xs) -> map_insert_list k v #
    list_to_map_list xs

snoc :: [a] -> a -> [a]
snoc xs x = xs ++ [x]

snoc' :: a -> [a] -> [a]
snoc' = flip snoc

list_rot_left :: [a] -> [a]
list_rot_left (x:xs) = snoc xs x

list_rot_right :: [a] -> [a]
list_rot_right xs = last xs : init xs

flip2 :: (a -> b -> c -> d) -> b -> c -> a -> d
flip2 f a b c = f c a b

range :: N -> [N]
range n = [0 .. n - 1]

ord_to_dif :: Ordering -> N
ord_to_dif ord = case ord of
  LT -> -1
  EQ -> 0
  GT -> 1

digits :: [Char]
digits = ['0'..'9']

letters_low :: [Char]
letters_low = ['a'..'z']

letters_up :: [Char]
letters_up = ['A'..'Z']

letters :: [Char]
letters = letters_low ++ letters_up

all_combs :: [a] -> [[a]]
all_combs xs = [] : do
  ys <- all_combs xs
  x <- xs
  return # snoc ys x

all_combs' :: [a] -> [[a]]
all_combs' xs = tail # all_combs xs

mk_combs :: [[a]] -> [[a]]
mk_combs xs = case xs of
  [] -> [[]]
  (x:xs) -> do
    y <- x
    ys <- mk_combs xs
    return # y : ys

mk_avail_getter' :: (Ord a) => Set a -> [a] -> a
mk_avail_getter' set (x:xs) = if Set.member x set
  then mk_avail_getter' set xs else x

mk_avail_getter :: (Ord a) => [a] -> Set a -> (a, Set a)
mk_avail_getter xs set = let
  x = mk_avail_getter' set xs
  in (x, set_insert x set)

set_size :: (Num a, Ord v) => Set v -> a
set_size s = fi # Set.size s

map_size :: (Num a, Ord k) => Map k v -> a
map_size mp = fi # Map.size mp

pair_map_fst :: (a -> c) -> (a, b) -> (c, b)
pair_map_fst f (a, b) = (f a, b)

pair_map_snd :: (b -> c) -> (a, b) -> (a, c)
pair_map_snd f (a, b) = (a, f b)

pair_map :: (a -> c) -> (b -> d) -> (a, b) -> (c, d)
pair_map f g (a, b) = (f a, g b)

pair_map' :: (a -> b) -> (a, a) -> (b, b)
pair_map' f = pair_map f f

pair_map_rev :: (b -> d) -> (a -> c) -> (a, b) -> (c, d)
pair_map_rev = flip pair_map

pair_map_fst_m :: (Monad m) => (a -> m c) -> (a, b) -> m (c, b)
pair_map_fst_m f (a, b) = f a >>= \a -> pure (a, b)

pair_map_snd_m :: (Monad m) => (b -> m c) -> (a, b) -> m (a, c)
pair_map_snd_m f (a, b) = f b >>= \b -> pure (a, b)

pair_map_m :: (Monad m) => (a -> m c) -> (b -> m d) -> (a, b) -> m (c, d)
pair_map_m f g (a, b) = f a >>= \a -> g b >>= \b -> pure (a, b)

either_map :: (a -> c) -> (b -> d) -> Either a b -> Either c d
either_map f g x = case x of
  Left a -> Left # f a
  Right b -> Right # g b

either_map_m :: (Monad m) => (a -> m c) -> (b -> m d) ->
  Either a b -> m (Either c d)
either_map_m f g x = case x of
  Left a -> f a >>= liftm Left
  Right b -> g b >>= liftm Right

logb' :: (MonadError e m) => Char -> m ()
logb' c = tracem # Lit [put_in2 "\n" # replicate 100 c]

logb :: (MonadError e m) => m ()
logb = logb' '='

maybe_to_pair :: Maybe a -> (Prop, a)
maybe_to_pair m = case m of
  Just a -> (True, a)
  Nothing -> (False, undefined)

set_to_map_unit :: (Ord a) => Set a -> Map a ()
set_to_map_unit s = Map.fromList # map (mk_pair' ()) # Set.toList s

-- class (Monad m) => VarCnt m where
--   cnt_next :: m N

map_push :: a -> Map N a -> Map N a
map_push x mp = map_insert (map_size mp) x mp

map_push_inv :: (Ord a) => a -> Map a N -> Map a N
map_push_inv x mp = map_insert x (map_size mp) mp

maybe_either :: Maybe a -> Maybe b -> Maybe (Either a b)
maybe_either ma mb = case ma of
  Just a -> Just (Left a)
  Nothing -> case mb of
    Just b -> Just (Right b)
    Nothing -> Nothing

map_inv :: (Ord a, Ord b) => Map a b -> Map b a
map_inv mp = Map.fromList # map swap # Map.toList mp

map_dif_set :: (Ord k) => Map k v -> Set k -> Map k v
map_dif_set mp s = foldr Map.delete mp # Set.toList s

list_dif :: (Eq a) => [a] -> [a] -> [a]
list_dif xs ys = filter (\x -> not # elem x ys) xs

infixr 5 <|>

class OrElse a where
  (<|>) :: a -> a -> a

digit_zero_n :: N
digit_zero_n = 48

sub_digit_zero_n :: N
sub_digit_zero_n = 8320

digit_to_char :: N -> Char
digit_to_char d = chr # fi # digit_zero_n + d

char_to_digit :: Char -> N
char_to_digit c = fi (ord c) - digit_zero_n

sub_digits :: String
sub_digits = mk_list 10 # \i -> chr # fi # sub_digit_zero_n + i

digit_to_sub :: N -> Char
digit_to_sub d = chr # fi # sub_digit_zero_n + d

sub_to_digit :: Char -> N
sub_to_digit c = fi (ord c) - sub_digit_zero_n

digit_char_to_sub :: Char -> Char
digit_char_to_sub c = digit_to_sub # char_to_digit c

sub_to_digit_char :: Char -> Char
sub_to_digit_char c = digit_to_char # sub_to_digit c

nat_to_digits' :: N -> [N]
nat_to_digits' n = if n == 0 then [] else
  mod n 10 : nat_to_digits' (div n 10)

nat_to_digits :: N -> [N]
nat_to_digits n = if n == 0 then [0] else reverse # nat_to_digits' n

digits_to_nat :: [N] -> N
digits_to_nat = flip foldl' 0 # \acc d -> acc * 10 + d

nat_to_sub :: N -> String
nat_to_sub n = map digit_to_sub # nat_to_digits n

sub_to_nat :: String -> N
sub_to_nat s = digits_to_nat # map sub_to_digit s

list_find_dup' :: (Eq a) => [a] -> [a] -> Maybe a
list_find_dup' xs ys = case xs of
  [] -> Nothing
  (x:xs) -> case ys of
    [] -> pure x
    (y:ys) -> if x /= y then pure x else
      list_find_dup' xs ys

list_find_dup :: (Eq a) => [a] -> Maybe a
list_find_dup xs = list_find_dup' xs # nub xs

inc :: N -> N
inc n = n + 1

dec :: N -> N
dec n = n - 1

ifm :: (Monad m) => Prop -> m a -> m ()
ifm p m = ite p (m >> nop) nop

ifmn :: (Monad m) => Prop -> m a -> m ()
ifmn p = ifm # not p

ifu :: Prop -> a -> a
ifu p a = ite p a undefined

ifmu :: (Monad m) => Prop -> m a -> m a
ifmu p m = ite p m # pure undefined

is_printable :: Char -> Prop
is_printable c = c >= ' ' && c <= '~'

nat_base_digits :: String
nat_base_digits = digits ++ letters_low

nat_to_base' :: N -> N -> String
nat_to_base' b n = if n == 0 then "" else let
  (n', d) = divMod n b
  in list_get d nat_base_digits : nat_to_base' b n'

nat_to_base :: N -> N -> String
nat_to_base b n = if n == 0 then "0" else reverse # nat_to_base' b n

nat_to_hex :: N -> String
nat_to_hex = nat_to_base 16

prop_xor :: Prop -> Prop -> Prop
prop_xor = (/=)

prop_xors :: [Prop] -> Prop
prop_xors = foldr prop_xor False

iter :: (Integral n) => n -> a -> (a -> a) -> a
iter n z f = if n == 0 then z else iter (n - 1) (f z) f

iterM :: (Integral n, Monad m) => n -> a -> (a -> m a) -> m a
iterM n z f = if n == 0 then pure z else do
  z <- f z
  iterM (n - 1) z f

show_pair :: (a -> String) -> (b -> String) -> (a, b) -> String
show_pair f g (a, b) = put_in_parens # concat [f a, ", ", g b]

run :: Identity a -> a
run = runIdentity

sort_by' :: (a -> a -> Ordering) -> [a] -> [a] -> [a]
sort_by' f ys xs = if null xs then ys else
  case findIndex <~ xs # \x -> all <~ xs # \y -> f x y /= GT of
    Just i -> sort_by' f (xs !! i : ys) # remove_at (fi i) xs

sort_by :: (a -> a -> Ordering) -> [a] -> [a]
sort_by f xs = reverse # sort_by' f [] xs

from_either_same :: Either a a -> a
from_either_same x = case x of
  Left a -> a
  Right b -> b

query :: (Monad m) => StateT s m a -> StateT s m a
query m = do
  s <- get
  a <- m
  put s
  return a

ifm_just :: (Monad m) => Maybe a -> (a -> m ()) -> m ()
ifm_just v f = case v of
  Nothing -> nop
  Just v -> f v

ifm_just_m :: (Monad m) => m (Maybe a) -> (a -> m ()) -> m ()
ifm_just_m m f = do
  v <- m
  ifm_just v f

min_b :: (Foldable t, Ord a, Bounded a) => t a -> a
min_b xs = if null xs then maxBound else minimum xs

max_b :: (Foldable t, Ord a, Bounded a) => t a -> a
max_b xs = if null xs then minBound else maximum xs

map_set_insert :: (Ord k, Ord v) => k -> v -> Map k (Set v) -> Map k (Set v)
map_set_insert k v = Map.alter <~ k # \s -> Just # case s of
  Nothing -> Set.singleton v
  Just s -> Set.insert v s

map_set_delete :: (Ord k, Ord v) => k -> v -> Map k (Set v) -> Map k (Set v)
map_set_delete k v = Map.alter <~ k # \s -> do
  s <- s
  s <- pure # Set.delete v s
  False <- pure # Set.null s
  return s

map_set_get :: (Ord k, Ord v) => k -> Map k (Set v) -> Set v
map_set_get = Map.findWithDefault Set.empty

swap_ite' :: Prop -> a -> a -> (a, a)
swap_ite' p a b = ite p (a, b) (b, a)

swap_ite :: (Monad m) => Prop -> a -> a -> m (a, a)
swap_ite p a b = pure # swap_ite' p a b

zip' :: [a] -> [b] -> [(a, b)]
zip' xs ys = case (xs, ys) of
  ([], []) -> []
  ((x : xs), (y : ys)) -> (x, y) : zip' xs ys
  _ -> error "zip"

zip_with' :: (a -> b -> c) -> [a] -> [b] -> [c]
zip_with' f xs ys = case (xs, ys) of
  ([], []) -> []
  ((x : xs), (y : ys)) -> f x y : zip_with' f xs ys
  _ -> error "zip_with"

zip_with :: [a] -> [b] -> (a -> b -> c) -> [c]
zip_with xs ys f = zip_with' f xs ys

fold_r :: [a] -> b -> (a -> b -> b) -> b
fold_r xs z f = foldr f z xs

fold_l :: [a] -> b -> (b -> a -> b) -> b
fold_l xs z f = foldl' f z xs

map_fold_r :: (Ord k) => Map k v -> b -> (k -> v -> b -> b) -> b
map_fold_r mp z f = Map.foldrWithKey f z mp

map_fold_l :: (Ord k) => Map k v -> b -> (b -> k -> v -> b) -> b
map_fold_l mp z f = Map.foldlWithKey f z mp

foldm_r :: (Monad m) => [a] -> b -> (a -> b -> m b) -> m b
foldm_r xs z f = foldrM f z xs

foldm_l :: (Monad m) => [a] -> b -> (b -> a -> m b) -> m b
foldm_l xs z f = foldlM f z xs

ico :: (Integral n) => n -> [n]
ico n = [0 .. n - 1]

-- set_disj_union' :: (Ord a) => Set a -> Set a -> Either a (Set a)
-- set_disj_union' set1 set2 = (\f -> Set.foldrM f (Just set2) set1) #
--   \x mset -> do
--     set <- mset
--     False <- pure # Set.member x set
--     return # Set.insert x set
-- 
-- set_disj_unions' :: (Ord a) => [Set a] -> Either a (Set a)
-- set_disj_unions' sets = fold_r sets (Just Set.empty) #
--   \set1 mset2 -> do
--     set2 <- mset2
--     set_disj_union' set1 set2
-- 
-- set_disj_union :: (Ord a) => Set a -> Set a -> Set a
-- set_disj_union set1 set2 = fromJust # set_disj_union' set1 set2
-- 
-- set_disj_unions :: (Ord a) => [Set a] -> Set a
-- set_disj_unions sets = fromJust # set_disj_unions' sets

map_disj_union' :: (Ord k) => Map k v -> Map k v -> Either k (Map k v)
map_disj_union' mp1 mp2 = foldm_r (Map.toList mp2) mp1 #
  \(k, v) mp -> do
    ifm (Map.member k mp) # Left k
    return # Map.insert k v mp

map_disj_unions' :: (Ord k) => [Map k v] -> Either k (Map k v)
map_disj_unions' mps = foldm_r mps Map.empty map_disj_union'

map_disj_union :: (Ord k) => Map k v -> Map k v -> Map k v
map_disj_union mp1 mp2 = from_right # map_disj_union' mp1 mp2

map_disj_unions :: (Ord k) => [Map k v] -> Map k v
map_disj_unions mps = from_right # map_disj_unions' mps

from_maybe_t :: (Monad m) => MaybeT m a -> m Prop
from_maybe_t m = do
  res <- runMaybeT m
  return # isJust res

map_find :: (Ord k) => (v -> Bool) -> Map k v -> Maybe v
map_find f mp = find f # Map.elems mp