import qualified Data.Set as Set
import qualified Data.Map as Map

import Util

main :: IO ()
main = do
  fr <- pure # \x xs -> put_in_parens # x : xs
  fl <- pure # \xs x -> put_in_parens # snoc xs x
  let s = ['a' .. 'e']
  putStrLn # fold_right "" fr s
  putStrLn # fold_left "" fl s

fold_right :: b -> (a -> b -> b) -> [a] -> b
fold_right z f xs = case xs of
  [] -> z
  (x : xs) -> f x # fold_right z f xs

fold_left :: b -> (b -> a -> b) -> [a] -> b
fold_left z f xs = (# z) # fold_right id <~ xs #
  \x fn z -> fn # f z x