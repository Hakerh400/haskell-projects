import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import Prelude hiding (print, putStr, putStrLn, getLine)
import System.IO hiding (print, putStr, putStrLn, getLine)

import Util
import Bit
import DfltMap
import Bimap
import Sctr
import Parser
import Rand

src_file = "src.txt"
word_len_bounds = (2, 5)
var_len_bounds = (1, 4)
arr_val_bounds = (-(2 ^ 31), 2 ^ 31 - 1)
arr_len = 2 ^ 14

type Val = N
type StackElem = Val
type Stack = [StackElem]
type Arr = DfltMap N N
type CoreChars = [Char]

data VarAct = VarPush | VarPop | VarTop
  deriving (Eq, Ord, Show)

data Cmd
  = CmdWordRef Name
  | CmdChar Char
  | CmdBytes [N]
  | CmdVarRef VarAct Name
  | CmdCore CoreChars
  | CmdComment
  | CmdWordDef Name CoreChars
  | CmdVarDef Name Val
  | CmdStr String
  deriving (Eq, Ord, Show)

type Line = [Cmd]

data ProgT = ProgT
  { _prog_stacks :: DfltMap N Stack
  , _prog_arr :: Arr
  , _prog_words :: Map Name CoreChars
  , _prog_core_vars :: DfltMap N N
  , _prog_vars :: Map Name N
  , _prog_lines :: Map N Line
  , _prog_line :: N
  , _prog_cmds :: Maybe [Cmd]
  , _prog_rand_seed :: N
  } deriving (Eq, Ord, Show)

type Prog = ParserM ProgT IO

main :: IO ()
main = do
  mapM_ (hSetBuffering <~ NoBuffering) [stdin, stdout, stderr]
  src <- fmap lf # readFile src_file
  res <- parse prog_main undefined src
  case res of
    Left msg -> putStrLn # concat msg
    Right _ -> nop

( (_ :: (Prog Val, Val -> Prog ()))
  : (t_0, t_1)
  : (tf_0, tf_1)
  : (tg_0, tg_1)
  : (th_0, th_1)
  : (ti_0, ti_1)
  : (tk_0, tk_1)
  : (tl_0, tl_1)
  : _) = (undefined :) # map <~ [0..] # \i -> mk_pair
    (gets # dflt_map_get i . _prog_core_vars) #
    \v -> modify # \p -> p
      {_prog_core_vars = dflt_map_insert i v # _prog_core_vars p}

parse_core_char :: Prog Char
parse_core_char = do
  c <- pget
  asrt # Map.member c core_chars_mp
  return c

parse_hex :: Prog N
parse_hex = p_digit <++ do
  c <- fmap toLower pget
  asrt # is_hex c
  return # hex_char_to_nat c

parse_byte :: Prog N
parse_byte = do
  [a, b] <- replicateM 2 parse_hex
  return # a * 16 + b

parse_bytes :: Prog [N]
parse_bytes = p_all parse_byte

asrt_ascii :: Char -> Prog ()
asrt_ascii c = ifm (c > '\xFF') # err ["Non-ASCII character ", show c]

chk_boundary :: ([Char] -> Prop -> Prog ()) -> (N, N) -> String -> Prog ()
chk_boundary chk b name =
  chk (concat ["made up of ", show_bounds b, " characters"]) #
    is_len_bound b name

parse_var_name :: Prog Name
parse_var_name = do
  name <- p_take_while not_ws
  chk <- pure # \s h -> ifmn h # err
    ["Variable must be ", s, ", got ", show name]
  chk_boundary chk var_len_bounds name
  chk "made up of printable ASCII characters" # all is_printable name
  return name

parse_cmd :: Prog Cmd
parse_cmd = trimmed # do
  c <- pget
  case c of
    '\'' -> do
      c <- pget
      asrt_ascii c
      return # CmdChar c
    '+' -> fmap CmdBytes parse_bytes
    '<' -> fmap (CmdVarRef VarPush) parse_var_name
    '>' -> fmap (CmdVarRef VarPop) parse_var_name
    '=' -> fmap (CmdVarRef VarTop) parse_var_name
    '!' -> fmap CmdCore # p_all parse_core_char
    ';' -> do
      s <- p_take_while not_ws
      ifm (null s) # err ["Expected a comment word"]
      return CmdComment
    ':' -> do
      name <- p_take_while not_ws
      chk <- pure # \s h -> ifmn h # err
        ["Word must be ", s, ", got ", show name]
      chk_boundary chk word_len_bounds name
      (c : _) <- pure name
      chk "start with a capital letter" # is_upper c
      chk "made up of printable ASCII characters" # all is_printable name
      p_char space
      cs <- p_all parse_core_char
      return # CmdWordDef name cs
    '$' -> do
      name <- parse_var_name
      p_char space
      val <- parse_byte
      return # CmdVarDef name val
    '"' -> do
      s <- p_take_while (/= '\n')
      mapM_ asrt_ascii s
      return # CmdStr s
    '#' -> do
      p_take_while (/= '\n')
      return CmdComment
    _ -> if is_upper c
      then do
        name <- p_take_while is_printable
        return # CmdWordRef # c : name
      else do
        pfail

parse_line :: Prog Line
parse_line = trimmed # do
  p_neof
  cmds <- p_all' parse_cmd
  p_eof <++ p_new_line
  return cmds

prog_main :: Prog ()
prog_main = do
  lines <- p_all' parse_line
  p_eof <++ do
    s <- p_rest
    err ["Syntax error:\n\n", s]
  put # ProgT
    { _prog_stacks = dflt_map_empty []
    , _prog_arr = dflt_map_empty 0
    , _prog_words = Map.empty
    , _prog_core_vars = dflt_map_empty 0
    , _prog_vars = Map.empty
    , _prog_lines = Map.fromList # zip [1..] lines
    , _prog_line = 1
    , _prog_cmds = Nothing
    , _prog_rand_seed = 0
    }
  run_prog

run_prog :: Prog ()
run_prog = do
  cmds <- gets _prog_cmds
  case cmds of
    Just cmds -> run_cmds cmds
    Nothing -> do
      line <- get_cur_line
      case line of
        Nothing -> nop
        Just cmds -> run_cmds cmds

run_cmd :: Cmd -> Prog ()
run_cmd cmd = case cmd of
  CmdWordRef name -> do
    cs <- get_word name
    run_core_chars cs
  CmdChar c -> push 1 # fi # ord c
  CmdBytes bs -> push 1 # fold_l bs 0 # \a b -> a * 256 + b
  CmdVarRef act name -> case act of
    VarPush -> get_var name >>= push 1
    VarPop -> pop 1 >>= set_var name
    VarTop -> top 1 >>= set_var name
  CmdCore cs -> run_core_chars cs
  CmdComment -> nop
  CmdWordDef name cs -> set_word name cs
  CmdVarDef name v -> set_var name v
  CmdStr s -> mapM_ <~ reverse s # push 1 . fi . ord

run_cmds :: [Cmd] -> Prog ()
run_cmds cmds = do
  case cmds of
    [] -> goto_next_line
    (cmd : cmds) -> do
      modify # \p -> p {_prog_cmds = Just cmds}
      run_cmd cmd
  run_prog

get_word :: Name -> Prog CoreChars
get_word name = do
  res <- gets # map_get name . _prog_words
  case res of
    Nothing -> err ["Undefined word ", show name]
    Just cs -> pure cs

set_word :: Name -> CoreChars -> Prog ()
set_word name cs = modify # \p -> p
  {_prog_words = map_insert name cs # _prog_words p}

get_var :: Name -> Prog Val
get_var name = do
  res <- gets # map_get name . _prog_vars
  case res of
    Nothing -> err ["Undefined variable ", show name]
    Just v -> pure v

set_var :: Name -> Val -> Prog ()
set_var name v = modify # \p -> p
  {_prog_vars = map_insert name v # _prog_vars p}

run_core_char :: Char -> Prog ()
run_core_char c = do
  Just fn <- pure # map_get c # core_chars_mp
  fn

run_core_chars :: CoreChars -> Prog ()
run_core_chars cs = case cs of
  [] -> nop
  (c : cs) -> do
    h <- gets # isJust . _prog_cmds
    ifm h # do
      run_core_char c
      run_core_chars cs

get_line :: N -> Prog (Maybe Line)
get_line i = do
  lines <- gets _prog_lines
  case map_get i lines of
    Just line -> pure # Just line
    Nothing -> if i == map_size lines + 1 then pure Nothing
      else err ["Cannot jump to line ", show i]

get_cur_line :: Prog (Maybe Line)
get_cur_line = gets _prog_line >>= get_line

goto_line :: N -> Prog ()
goto_line i = modify # \p -> p
  { _prog_core_vars = dflt_map_empty 0
  , _prog_vars = Map.empty
  , _prog_line = i
  , _prog_cmds = Nothing
  }

goto_next_line :: Prog ()
goto_next_line = do
  i <- gets _prog_line
  goto_line # i + 1

get_stack :: N -> Prog Stack
get_stack i = gets # dflt_map_get i . _prog_stacks

push :: N -> Val -> Prog ()
push i v = do
  ss <- gets _prog_stacks
  s <- pure # v : dflt_map_get i ss
  modify # \p -> p {_prog_stacks = dflt_map_insert i s ss}

pop' :: Prop -> N -> Prog Val
pop' update i = do
  ss <- gets _prog_stacks
  case dflt_map_get i ss of
    [] -> err ["Stack ", show i, " is empty"]
    (v : s) -> do
      ifm update # modify # \p -> p
        {_prog_stacks = dflt_map_insert i s ss}
      return v

pop :: N -> Prog Val
pop = pop' True

top :: N -> Prog Val
top = pop' False

halt :: Prog ()
halt = do
  lines <- gets _prog_lines
  goto_line # map_size lines + 1

exit_t :: Prog ()
exit_t = do
  t_0 >>= \t -> ifm (t /= 0) # err ["Exit code ", show t]
  halt

chk_arr_index :: N -> Prog ()
chk_arr_index i = ifm (i >= arr_len) #
  err ["Array index out of bounds: ", show i]

arr_get :: N -> Prog N
arr_get i = do
  chk_arr_index i
  gets # dflt_map_get i . _prog_arr

arr_set :: N -> N -> Prog ()
arr_set i v = do
  chk_arr_index i
  ifmn (is_bound arr_val_bounds v) #
    err ["Array value is not a 32-bit signed integer: ", show v]
  modify # \p -> p
    {_prog_arr = dflt_map_insert i v # _prog_arr p}

arr_get_str :: N -> N -> Prog String
arr_get_str i n = do
  es <- pure "Cannot extract string from array: "
  ifm (n < 0) # err [es, "negative length ", show n]
  mapM <~ ico n # \j -> do
    v <- arr_get # i + j
    ifm (is_byte v) # err [es, "invalid value ", show v]
    return # chr # fi v

arr_get_char :: N -> Prog Char
arr_get_char i = do
  (c : _) <- arr_get_str i 1
  return c

arr_get_core_chars :: N -> Prog CoreChars
arr_get_core_chars i = if i == arr_len then pure [] else do
  c <- arr_get_char i
  if not # Map.member c core_chars_mp then pure [] else do
    cs <- arr_get_core_chars # i + 1
    return # c : cs

avail_aux :: String -> (Char -> String -> String) -> Bimap N Name
avail_aux s f = bimap_from_list # zip [fst arr_val_bounds ..] # do
  c <- s
  n <- [1 .. 4]
  s <- replicateM n ['!' .. '~']
  return # f c s

avail_words :: Bimap N Name
avail_words = avail_aux ['A' .. 'Z'] (:)

avail_vars :: Bimap N Name
avail_vars = avail_aux [undefined] # const id

encode_word_name :: Name -> Prog N
encode_word_name name = pure # fromJust # bimap_get' name avail_words

decode_word_name :: N -> Prog Name
decode_word_name n = pure # fromJust # bimap_get n avail_words

encode_var_name :: Name -> Prog N
encode_var_name name = pure # fromJust # bimap_get' name avail_vars

decode_var_name :: N -> Prog Name
decode_var_name n = pure # fromJust # bimap_get n avail_vars

core_chars_mp :: Map Char (Prog ())
core_chars_mp = let pr = mk_pair in Map.fromList
  [ pr 's' # pop 1 >>= t_1
  , pr 'S' # t_0 >>= push 1
  , pr 't' # pop 2 >>= t_1
  , pr 'T' # t_0 >>= push 2
  , pr '.' # get_stack 1 >>= t_1 . len
  , pr ':' # get_stack 2 >>= t_1 . len
  , pr 'f' # tf_0 >>= t_1
  , pr 'F' # t_0 >>= tf_1
  , pr 'g' # tg_0 >>= t_1
  , pr 'G' # t_0 >>= tg_1
  , pr 'h' # th_0 >>= t_1
  , pr 'H' # t_0 >>= th_1
  , pr 'i' # ti_0 >>= t_1
  , pr 'I' # t_0 >>= ti_1
  , pr 'k' # tk_0 >>= t_1
  , pr 'K' # t_0 >>= tk_1
  , pr 'l' # tl_0 >>= t_1
  , pr 'L' # t_0 >>= tl_1
  , pr 'z' # t_0 >>= tk_1 . from_prop . (0 ==)
  , pr 'Z' # t_0 >>= tk_1 . from_prop . (0 <)
  , pr 'j' # tk_0 >>= \tk -> ifm (tk /= 0) # do
    i <- gets _prog_line
    t <- t_0
    goto_line # i + t
  , pr 'J' # tk_0 >>= \tk -> ifm (tk /= 0) # t_0 >>= goto_line
  , pr 'q' # exit_t
  , pr 'Q' # tk_0 >>= \tk -> ifm (tk == 0) exit_t
  , pr 'n' # tk_0 >>= tk_1 . prop_lift not
  , pr 'N' # tl_0 >>= tl_1 . prop_lift not
  , pr 'a' # tk_0 >>= \tk -> tl_0 >>= \tl -> tk_1 # prop_lift2 (&&) tk tl
  , pr 'A' # tk_0 >>= \tk -> tl_0 >>= \tl -> tl_1 # prop_lift2 (&&) tk tl
  , pr 'o' # tk_0 >>= \tk -> tl_0 >>= \tl -> tk_1 # prop_lift2 (||) tk tl
  , pr 'O' # tk_0 >>= \tk -> tl_0 >>= \tl -> tl_1 # prop_lift2 (||) tk tl
  , pr 'x' # tk_0 >>= \tk -> tl_0 >>= \tl -> tk_1 # prop_lift2 (/=) tk tl
  , pr 'X' # tk_0 >>= \tk -> tl_0 >>= \tl -> tl_1 # prop_lift2 (/=) tk tl
  , pr '\x5F' # t_1 0
  , pr '0' # t_0 >>= t_1 . (2 *)
  , pr '1' # t_0 >>= t_1 . inc . (2 *)
  , pr '|' # t_0 >>= t_1 . abs
  , pr '-' # t_0 >>= t_1 . negate
  , pr '+' # t_0 >>= \t -> tl_0 >>= \tl -> t_1 # t + tl
  , pr '*' # t_0 >>= \t -> tl_0 >>= \tl -> t_1 # t * tl
  , pr '/' # t_0 >>= \t -> tl_0 >>= \tl -> t_1 # div t tl
  , pr '%' # t_0 >>= \t -> tl_0 >>= \tl -> t_1 # mod t tl
  , pr 'p' # t_0 >>= \t -> tl_0 >>= \tl -> t_1 # t ^ tl
  , pr 'P' # t_0 >>= \t -> tl_0 >>= \tl -> do
    ifmn (t > 0 && tl > 0) # err
      ["Cannot compute log(", show t, ", ", show tl, ")"]
    t_1 # int_log t tl
  , pr 'b' # tl_0 >>= \tl -> arr_get tl >>= t_1
  , pr 'B' # tl_0 >>= \tl -> t_0 >>= arr_set tl
  , pr 'c' # do
    tl <- tl_0
    n <- fmap <~ t_0 # \t -> mod t 4 + 2
    s <- arr_get_str tl n
    encode_word_name s >>= t_1
  , pr 'C' # do
    tl <- tl_0
    n <- fmap <~ t_0 # \t -> mod t 4 + 1
    s <- arr_get_str tl n
    encode_var_name s >>= t_1
  , pr 'd' # do
    t <- t_0
    v <- arr_get t
    name <- decode_word_name v
    cs <- arr_get_core_chars # t + 1
    set_word name cs
  , pr 'D' # do
    t <- t_0
    v <- arr_get t
    name <- decode_word_name v
    v <- arr_get # t + 1
    set_var name v
  , pr 'u' # liftIO get_time >>= t_1
  , pr 'U' # liftIO get_time_ns >>= t_1 . (mod <~ 10 ^ 9)
  , pr 'r' # do
    t <- t_0
    ifm (t <= 0) # err
      ["Upper bound of random interval must be positive, got ", show t]
    (v, s) <- gets # (rand_nat <~ t) . _prog_rand_seed
    modify # \p -> p {_prog_rand_seed = s}
    t_1 v
  , pr 'R' # t_0 >>= \t -> modify # \p -> p {_prog_rand_seed = abs t}
  , pr 'w' # do
    c <- liftIO getChar
    t_1 # fi # ord c
  , pr 'W' # do
    s <- getLine
    ifmn (all is_digit s) # err ["Invalid base-10 number ", show s]
    t_1 # read s
  , pr 'y' # do
    v <- t_0
    ifmn (is_byte v) # err
      ["Cannot output character with char code ", show v]
    liftIO # putChar # chr # fi v
  , pr 'Y' # t_0 >>= putStrLn . show
  , pr 'm' # do
    s <- getLine
    ifmn (all is_hex s) # err ["Invalid base-16 number ", show s]
    t_1 # fold_l s 0 # \a c -> a * 16 + hex_char_to_nat c
  , pr 'M' # t_0 >>= putStrLn . nat_to_hex
  , pr 'e' # do
    t <- t_0
    name <- decode_word_name t
    cs <- get_word name
    run_core_chars cs
    t_1 0
  , pr 'E' # do
    t <- t_0
    name <- decode_var_name t
    get_var name >>= t_1
  ]