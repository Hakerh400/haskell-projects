module Rand
  ( rand_bit
  , rand_nat'
  , rand_nat
  ) where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)

import Util
import Bit

type Rand = State N

a = 1664525
c = 1013904223
m = 2 ^ 32

rand_bit :: Rand Bit
rand_bit = do
  v <- get
  put # mod (v * a + c) m
  return # prop_to_bit # odd # read # reverse # show v

rand_nat' :: N -> Rand N
rand_nat' n = do
  let bn = bits_num n
  bs <- replicateM (fi bn) rand_bit
  k <- pure # fold_l bs 0 # \a b -> a * 2 + fi b
  if k <= n then pure k else rand_nat' n

rand_nat :: N -> N -> (N, N)
rand_nat seed n = runState (rand_nat' n) seed