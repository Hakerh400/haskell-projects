module Sctr where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)

import Util

type Sctr1 = String
type Sctr = [Sctr1]

class (MonadError Sctr m) => MonadShow a m where
  m_show :: a -> m String

scope_dflt = ""
scope_err = ""

dfsc = id

err :: (MonadError Sctr m) => Sctr -> m a
err msg = raise # "[Error] " : msg

show_sctr :: Sctr -> String
show_sctr = concat

sctr_add_char :: Name -> Char -> Sctr -> Sctr
sctr_add_char scope c sctr = case sctr of
  [] -> [[c]]
  (str0 : sctr') -> (c : str0) : sctr'

sctr_add_str :: Name -> String -> Sctr -> Sctr
sctr_add_str scope s0 sctr = let
  s = reverse s0
  in case sctr of
    [] -> [s]
    (str0 : sctr') -> (s ++ str0) : sctr'