import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)
import Data.IORef

import Util

type Algebra f a = f a -> a
type Coalgebra f a = a -> f a

data Fix f = Fix {unfix :: f (Fix f)}

cata :: (Functor f) => Algebra f a -> Fix f -> a
cata alg = alg . fmap (cata alg) . unfix

ana :: (Functor f) => Coalgebra f a -> a -> Fix f
ana coa = Fix . fmap (ana coa) . coa

data NatF a = ZeroF | SuccF a
  deriving (Functor)

instance Num (Fix NatF) where
  a + Fix b = case b of
    ZeroF -> a
    SuccF b -> Fix # SuccF # a + b
  a * Fix b = case b of
    ZeroF -> 0
    SuccF b -> a + a * b
  abs = id
  signum (Fix a) = case a of
    ZeroF -> 0
    SuccF _ -> 1
  fromInteger n = if n < 0 then error "" else
    if n == 0 then Fix ZeroF
    else Fix # SuccF # fromInteger # n - 1
  negate = undefined

fib :: Algebra NatF (N, N)
fib fa = case fa of
  ZeroF -> (0, 1)
  SuccF (a, b) -> (b, a + b)

data StreamF e a = StreamF e a
  deriving (Functor)

fix_stream_to_list :: Fix (StreamF a) -> [a]
fix_stream_to_list (Fix (StreamF x xs)) =
  x : fix_stream_to_list xs

primes :: Coalgebra (StreamF N) [N]
primes xs = case xs of
  [] -> error ""
  (x : xs) -> StreamF x # filter <~ xs # \y -> mod y x /= 0

main :: IO ()
main = do
  print # take 1000 # fix_stream_to_list # ana primes [2..]