import Data.Char
import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Util
import Prog

src_file = "src.txt"
inp_file = "inp.txt"

main :: IO ()
main = do
  src <- read_src_file src_file
  inp <- read_inp_file inp_file
  out <- run src inp
  putStrLn # show_out out

read_src_file :: FilePath -> IO [N]
read_src_file pth = do
  str <- readFile pth
  str <- pure # flip filter str # \c -> is_digit c || elem c "-."
  return # if null str then [] else
    map read # split_at_elem '.' str

read_inp_file :: FilePath -> IO [N]
read_inp_file pth = do
  str <- readFile pth
  return # map (fi . ord) str

show_out :: [N] -> String
show_out = map # chr . fi