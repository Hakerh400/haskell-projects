module Prog (run) where

import Data.List
import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map
import Control.Monad
import Control.Monad.State

import Util
import DfltMap

type Mem = DfltMap N N

data ProgT = ProgT
  { _prog_mem :: Mem
  , _prog_inp :: [N]
  , _prog_out :: [N]
  } deriving (Eq, Ord, Show)

type Prog = StateT ProgT IO

dflt_val :: N
dflt_val = 0

init_mem :: [N] -> Mem
init_mem src = map_to_dflt_map dflt_val #
  Map.fromList # zip [0..] src

init_prog :: [N] -> [N] -> ProgT
init_prog src inp = ProgT
  { _prog_mem = init_mem src
  , _prog_inp = inp
  , _prog_out = []
  }

mem_get :: N -> Prog N
mem_get i = gets # dflt_map_lookup i . _prog_mem

mem_set :: N -> N -> Prog ()
mem_set i v = modify # \p -> p
  {_prog_mem = dflt_map_insert i v # _prog_mem p}

prog_inp :: Prog N
prog_inp = do
  inp <- gets _prog_inp
  case inp of
    [] -> pure dflt_val
    (v:inp) -> do
      modify # \p -> p {_prog_inp = inp}
      return v

prog_out :: N -> Prog ()
prog_out i = modify # \p -> p
  {_prog_out = i : _prog_out p}

prog_get_out :: Prog [N]
prog_get_out = gets # \p -> reverse # _prog_out p

mem_addr' :: Set N -> N -> Prog (Maybe N)
mem_addr' set i = do
  let direct = i >= 0
  let j = ite direct i # -i - 1
  if Set.member i set then pure Nothing else do
    v <- mem_get j
    if direct then pure # Just j else
      mem_addr' (Set.insert i set) v

mem_addr :: N -> Prog (Maybe N)
mem_addr = mem_addr' Set.empty

mem_read :: N -> Prog N
mem_read i = mem_addr i >>= \i -> case i of
  Just i -> mem_get i
  Nothing -> prog_inp

mem_read_i :: N -> Prog N
mem_read_i i = do
  j <- mem_read i
  mem_read j

mem_write :: N -> N -> Prog ()
mem_write i v = mem_addr i >>= \i -> case i of
  Just i -> mem_set i v
  Nothing -> prog_out v

show_mem :: Mem -> String
show_mem mem = let
  mp = dflt_map_to_map mem
  n = maximum' # Map.keys mp
  in unwords # flip map [0..n] # \i ->
    pad_start 3 ' ' # show # dflt_map_lookup i mem

display_mem :: Prog ()
display_mem = do
  mem <- gets _prog_mem
  liftIO # putStrLn # show_mem mem

run_inst :: Prog ()
run_inst = do
  ip <- mem_read 0
  dest <- mem_read ip
  op1 <- mem_read_i # ip + 1
  op2 <- mem_read_i # ip + 2
  mem_write 0 # ip + 3
  mem_write dest # op1 - op2

run_prog :: Prog [N]
run_prog = do
  run_inst
  out <- gets _prog_out
  case out of
    (n:out) -> if n /= dflt_val then run_prog else do
      modify # \p -> p {_prog_out = out}
      prog_get_out
    _ -> run_prog

run :: [N] -> [N] -> IO [N]
run src inp = evalStateT run_prog # init_prog src inp