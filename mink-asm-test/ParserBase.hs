module ParserBase where

import Data.Char
import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import GHC.Read
import Text.ParserCombinators.ReadPrec

import Base

infixr 5 ++>
(++>) :: ReadPrec a -> ReadPrec a -> ReadPrec a
(++>) = flip (<++)

p_char :: Char -> ReadPrec ()
p_char c = get >>= \c1 ->
  if c1 == c then pure () else pfail

p_str :: String -> ReadPrec ()
p_str [] = pure ()
p_str (c:cs) = p_char c >> p_str cs

p_all :: ReadPrec a -> ReadPrec [a]
p_all r = pure [] ++> do
  x <- r
  xs <- p_all r
  return # x : xs

p_sep :: ReadPrec a -> ReadPrec b -> ReadPrec ([a], [b])
p_sep r s = pure ([], []) ++> do
  x <- r
  zs <- p_all # do
    b <- s
    a <- r
    return (a, b)
  let (xs, ys) = unzip zs
  return (x : xs, ys)

p_sep' :: ReadPrec a -> ReadPrec b -> ReadPrec [a]
p_sep' r s = do
  result <- p_sep r s
  return # fst result

p_take_while :: (Char -> Prop) -> ReadPrec String
p_take_while f = p_all # get >>= \c -> ite (f c) (pure c) pfail

p_drop_while :: (Char -> Prop) -> ReadPrec ()
p_drop_while f = p_take_while f >> pure ()

p_take_while1 :: (Char -> Prop) -> ReadPrec String
p_take_while1 f = do
  xs <- p_take_while f
  if null xs then pfail else pure xs

trimmed :: ReadPrec a -> ReadPrec a
trimmed r = do
  p_drop_while isSpace
  x <- r
  p_drop_while isSpace
  return x

p_ws :: ReadPrec ()
p_ws = p_all (p_char space) >> pure ()

is_ident_char :: Char -> Prop
is_ident_char c = or
  [ isDigit c
  , isLetter c
  , c == '\x5F'
  , c == '\''
  ]