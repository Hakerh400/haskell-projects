import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util
import Bit
import Parser

src_file = "src.txt"
inp_file = "inp.txt"

type Mem = ([N], [N])

data Inst
  = InstMove Bit
  | InstModify Bit
  | InstIO Bit
  | InstLoop [Inst]
  deriving (Eq, Ord, Show)

data ProgT = ProgT
  { _prog_mem :: Mem
  , _prog_insts :: [Inst]
  , _prog_inp :: String
  , _prog_out :: String
  } deriving (Eq, Ord, Show)

type Prog' = ParserM ProgT
type Prog = StateT ProgT (Except Sctr)

main :: IO ()
main = do
  src <- readFile src_file
  inp <- readFile inp_file
  case run_prog src inp of
    Left msg -> putStrLn # concat msg
    Right out -> putStrLn out

init_mem :: Mem
init_mem = let xs = repeat 0 in (xs, xs)

init_prog :: String -> ProgT
init_prog inp = ProgT
  { _prog_mem = init_mem
  , _prog_insts = []
  , _prog_inp = inp
  , _prog_out = ""
  }

parse_loop :: Prog' Inst
parse_loop = do
  '[' <- pget
  insts <- parse_insts
  ']' <- pget
  return # InstLoop insts

parse_loop_m :: Prog' (Maybe Inst)
parse_loop_m = parse_loop >>= liftm Just

parse_inst_m :: Prog' (Maybe Inst)
parse_inst_m = parse_loop_m <++ do
  c <- pget
  let ret = pure . Just
  case c of
    '<' -> ret # InstMove 0
    '>' -> ret # InstMove 1
    '+' -> ret # InstModify 1
    '-' -> ret # InstModify 0
    ',' -> ret # InstIO 0
    '.' -> ret # InstIO 1
    '[' -> pfail
    ']' -> pfail
    _ -> pure Nothing

parse_insts :: Prog' [Inst]
parse_insts = do
  insts <- p_all' parse_inst_m
  return # do
    Just inst <- insts
    return inst

parse_prog :: Prog' ProgT
parse_prog = do
  insts <- parse_insts
  gets # \prog -> prog {_prog_insts = insts}

get_mem :: Prog Mem
get_mem = gets _prog_mem

put_mem :: Mem -> Prog ()
put_mem mem = modify # \prog -> prog {_prog_mem = mem}

get_cell :: Prog N
get_cell = do
  ((n:_), _) <- get_mem
  return n

put_cell :: N -> Prog ()
put_cell n = do
  ((_:xs'), ys) <- get_mem
  put_mem (n:xs', ys)

modify_cell :: (N -> N) -> Prog ()
modify_cell f = do
  n <- get_cell
  put_cell # f n

prog_read :: Prog N
prog_read = do
  inp <- gets _prog_inp
  case inp of
    "" -> pure 0
    (c:cs) -> do
      let n = fi # ord c
      ifm (n >= 256) # err [[c]]
      modify # \prog -> prog {_prog_inp = cs}
      return n

prog_write :: N -> Prog ()
prog_write n = do
  let c = chr # fi n
  modify # \prog -> prog {_prog_out = c : _prog_out prog}

run_loop :: [Inst] -> Prog ()
run_loop insts = do
  n <- get_cell
  ifm (n /= 0) # do
    run_insts insts
    run_loop insts

run_inst :: Inst -> Prog ()
run_inst inst = case inst of
  InstMove dir -> do
    (xs@(x:xs'), ys@(y:ys')) <- get_mem
    put_mem # case dir of
      0 -> (xs', x:ys)
      1 -> (y:xs, ys')
  InstModify dif -> modify_cell # \n -> let
    n' = n + case dif of
      0 -> -1
      1 -> 1
    in case n' of
      -1 -> 255
      256 -> 0
      n -> n
  InstIO k -> case k of
    0 -> prog_read >>= put_cell
    1 -> get_cell >>= prog_write
  InstLoop insts -> run_loop insts

run_insts :: [Inst] -> Prog ()
run_insts = mapM_ run_inst

run_prog' :: Prog String
run_prog' = do
  insts <- gets _prog_insts
  run_insts insts
  gets # reverse . _prog_out

run_prog :: String -> String -> Either Sctr String
run_prog src inp = do
  prog <- parse parse_prog (init_prog inp) src
  runExcept # evalStateT run_prog' prog