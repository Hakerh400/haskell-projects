module Util where

import qualified Data.Set as Set
import Data.Set (Set)
import Data.List
import Data.Maybe
import Control.Monad.Except hiding (fix)

infixr 0 #
(#) = id

type N = Integer
type Prop = Bool
type ErrorType = [String]

put_in :: a -> a -> [a] -> [a]
put_in a b xs = [a] ++ xs ++ [b]

put_in_parens :: String -> String
put_in_parens = put_in '(' ')'

fi :: (Integral a, Num b) => a -> b
fi = fromIntegral

unlines' :: [String] -> String
unlines' = intercalate "\n"

dbg :: (Show a) => a -> b
dbg a = error # show a

set_at :: N -> a -> [a] -> [a]
set_at 0 z (x:xs) = z : xs
set_at n z (x:xs) = x : set_at (n - 1) z xs

modify_at :: N -> (a -> a) -> [a] -> [a]
modify_at 0 f (x:xs) = f x : xs
modify_at n f (x:xs) = x : modify_at (n - 1) f xs

nop :: (Monad m) => m ()
nop = pure ()

space :: Char
space = ' '

split_adv' :: ([a] -> [a] -> Maybe N) -> N -> [a] -> [a] -> [a] -> [[a]] -> [[a]]
split_adv' func skip prev list sub acc = case list of
  [] -> case func prev list of
    Nothing -> reverse (reverse sub : acc)
    _ -> reverse ([] : reverse sub : acc)
  (x:xs) -> case skip of
    0 -> case func prev list of
      Nothing -> split_adv' func 0 (x : prev) xs (x : sub) acc
      Just len -> case len of
        0 -> split_adv' func 0 (x : prev) xs (x : []) (reverse sub : acc)
        n -> split_adv' func (n - 1) (x : prev) xs [] (reverse sub : acc)
    n -> split_adv' func (n - 1) (x : prev) xs sub acc

split_adv :: ([a] -> [a] -> Maybe N) -> [a] -> [[a]]
split_adv func list = split_adv' func 0 [] list [] []

len :: [a] -> N
len xs = fi # length xs

list_get :: N -> [a] -> Maybe a
list_get 0 [] = Nothing
list_get 0 (x:xs) = Just x
list_get i (x:xs) = list_get (i - 1) xs

list_get' :: N -> [a] -> a
list_get' i xs = fromJust # list_get i xs

sanl :: String -> [String]
sanl = split_adv f where
  f _ ('\r':'\n':_) = Just 2
  f _ ('\r':_) = Just 1
  f _ ('\n':_) = Just 1
  f _ _ = Nothing

mapi :: (a -> N -> b) -> [a] -> [b]
mapi f xs = zipWith f xs [0..]

pad_end :: N -> a -> [a] -> [a]
pad_end n z xs = let n' = len xs in
  if n' < n then xs ++ replicate (fi # n - n') z else xs

err :: (MonadError e m) => e -> m a
err = throwError

maximum' :: [N] -> N
maximum' xs = maximum # 0 : xs

get_fst_just :: [Maybe a] -> Maybe a
get_fst_just xs = case xs of
  [] -> Nothing
  (x:xs) -> case x of
    Just _ -> x
    Nothing -> get_fst_just xs

equiv_classes' :: (a -> a -> Prop) -> [a] -> [[a]]
equiv_classes' f xs = case xs of
  [] -> []
  (x:xs) -> let
    cs = equiv_classes' f xs
    (cs1, cs2) = partition (any # f x) cs
    in (x : concat cs1) : cs2

equiv_classes :: (Ord a) => (a -> a -> Prop) -> Set a -> Set (Set a)
equiv_classes f set = Set.fromList # map Set.fromList #
  equiv_classes' f # Set.toList set

show_pair :: (Show a, Show b) => (a, b) -> String
show_pair (a, b) = put_in_parens # concat [show a, ", ", show b]