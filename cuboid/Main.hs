import qualified Data.Set as Set
import Data.Set (Set)

import Data.Char
import Data.List
import Control.Monad.State

import Util

src_file = "src.txt"

main :: IO ()
main = do
  src <- readFile src_file
  case run_prog src of
    Left msg -> putStrLn # concat msg
    Right str -> putStrLn str

type Row a = [a]
type Mat a = [Row a]

data Cub = Cub
  { _top :: N
  , _left :: N
  , _bottom :: N
  , _dims :: (N, N, N)
  } deriving (Eq, Ord, Show)

data GridT = GridT
  { _w :: N
  , _h :: N
  , _mat :: Mat Char
  , _cubs :: Set Cub
  } deriving (Eq, Ord)

show_mat :: Mat Char -> String
show_mat = unlines'

show_grid :: GridT -> String
show_grid grid = "\n" ++ show_mat (_mat grid)

instance Show GridT where
  show = show_grid

type Grid = StateT GridT (Either ErrorType)

dbg_grid :: Grid a
dbg_grid = get >>= dbg

map_row :: (a -> N -> b) -> Row a -> Row b
map_row = mapi

map_mat :: (a -> N -> N -> b) -> Mat a -> Mat b
map_mat f = mapi # \row y -> flip map_row row # \a x -> f a x y

find_cub_char_fn :: Char -> N -> N -> Maybe (N, N)
find_cub_char_fn c x y =
  if c /= space then Just (x, y) else Nothing

find_cub_char_in_mat :: Mat Char -> Maybe (N, N)
find_cub_char_in_mat mat = get_fst_just # concat #
  map_mat find_cub_char_fn mat

assert_has :: N -> N -> Grid ()
assert_has x y = do
  w <- gets _w
  h <- gets _h
  let ok = x >= 0 && y >= 0 && x < w && y < h
  if ok then nop else
    err ["Out of bounds: ", show_pair (x, y)]

get_char :: N -> N -> Grid Char
get_char x y = gets _mat >>= \mat -> do
  assert_has x y
  return # list_get' x # list_get' y mat

set_char :: N -> N -> Char -> Grid ()
set_char x y c = gets _mat >>= \mat -> do
  assert_has x y
  modify # \grid -> grid
    {_mat = modify_at y (set_at x c) mat}

clear_char :: N -> N -> Grid ()
clear_char x y = set_char x y space

consume_char :: N -> N -> Grid Char
consume_char x y = do
  c <- get_char x y
  clear_char x y
  return c

assert_eq :: (Eq a, Show a) => a -> a -> Grid ()
assert_eq a b = if a == b then nop else
  err ["Expected ", show a, ", but got ", show b]

consume_line :: Char -> N -> N -> N -> N -> Grid N
consume_line c0 dx dy x y = do
  c <- get_char x y
  if c == '+' then pure 0 else do
    consume_assert x y c0
    n <- consume_line c dx dy (x + dx) (y + dy)
    return # n + 1

consume_hor_line :: N -> N -> Grid N
consume_hor_line = consume_line '-' 1 0

consume_ver_line :: N -> N -> Grid N
consume_ver_line = consume_line '|' 0 1

consume_dia_line :: N -> N -> Grid N
consume_dia_line = consume_line '/' (-1) 1

get_assert :: N -> N -> Char -> Grid ()
get_assert x y c = get_char x y >>= assert_eq c

assert_empty :: N -> N -> Grid ()
assert_empty x y = get_assert x y space

consume_assert :: N -> N -> Char -> Grid ()
consume_assert x y c = consume_char x y >>= assert_eq c

add_cub :: Cub -> Grid ()
add_cub cub = modify # \grid -> grid
  {_cubs = Set.insert cub # _cubs grid}

assert_empty_line :: N -> N -> N -> N -> N -> Grid ()
assert_empty_line dx dy x y n = if n == 0 then nop else
  assert_empty x y >> assert_empty_line dx dy (x + dx) (y + dy) (n - 1)

assert_empty_hor_line :: N -> N -> N -> Grid ()
assert_empty_hor_line = assert_empty_line 1 0

assert_empty_ver_line :: N -> N -> N -> Grid ()
assert_empty_ver_line = assert_empty_line 0 1

consume_cub :: N -> N -> Grid ()
consume_cub x y = do
  cub_len <- consume_hor_line (x + 1) y
  let x1 = x + cub_len + 1
  cub_height <- consume_ver_line x1 (y + 1)
  let y1 = y + cub_height + 1
  cub_width <- consume_dia_line (x - 1) (y + 1)
  consume_dia_line (x1 - 1) (y + 1) >>= assert_eq cub_width
  consume_dia_line (x1 - 1) (y1 + 1) >>= assert_eq cub_width
  let x2 = x - cub_width - 1
  let y2 = y + cub_width + 1
  let x3 = x2 + cub_len + 1
  let y3 = y2 + cub_height + 1
  consume_hor_line (x2 + 1) y2 >>= assert_eq cub_len
  consume_hor_line (x2 + 1) y3 >>= assert_eq cub_len
  consume_ver_line x2 (y2 + 1) >>= assert_eq cub_height
  consume_ver_line x3 (y2 + 1) >>= assert_eq cub_height
  mapM_ (\(x, y) -> consume_assert x y '+')
    [(x, y), (x1, y), (x1, y1), (x2, y2), (x3, y2), (x2, y3), (x3, y3)]
  mapM_ (\i -> assert_empty_hor_line (x - i + 1) (y + i) cub_len) [1..cub_width]
  mapM_ (\i -> assert_empty_ver_line (x1 - i) (y + i + 1) cub_height) [1..cub_width]
  mapM_ (\i -> assert_empty_hor_line (x2 + 1) (y2 + i) cub_len) [1..cub_height]
  add_cub # Cub
    { _top = y
    , _left = x2
    , _bottom = y3
    , _dims = (cub_len, cub_width, cub_height)
    }

consume_cubs :: Grid ()
consume_cubs = gets _mat >>= \mat ->
  case find_cub_char_in_mat mat of
    Nothing -> nop
    Just (x, y) -> do
      consume_cub x y
      consume_cubs

cubs_same_row :: Cub -> Cub -> Prop
cubs_same_row c1 c2 =
  _top c1 <= _bottom c2 && _top c2 <= _bottom c1

cub_to_char :: Cub -> Char
cub_to_char cub = let (a, b, c) = _dims cub in
  chr # fi # a * b * c

cmp_cubs_in_row :: Cub -> Cub -> Ordering
cmp_cubs_in_row cub1 cub2 = case compare (_left cub1) (_left cub2) of
  EQ -> compare (_top cub1) (_top cub2)
  a -> a

cub_row_to_str :: [Cub] -> String
cub_row_to_str row = map cub_to_char #
  sortBy cmp_cubs_in_row row

cub_rows_to_str :: [[Cub]] -> String
cub_rows_to_str rows = rows >>= cub_row_to_str

proc_grid :: Grid String
proc_grid = do
  consume_cubs
  cubs <- gets _cubs
  rows <- pure # map Set.toList # Set.toList #
    equiv_classes cubs_same_row cubs
  return # cub_rows_to_str rows

run_prog :: String -> Either ErrorType String
run_prog src = do
  let mat = sanl src
  let w = maximum' # map len mat
  let h = len mat
  mat <- pure # map (pad_end w space) mat
  evalStateT proc_grid # GridT
    { _w = w
    , _h = h
    , _mat = mat
    , _cubs = Set.empty
    }