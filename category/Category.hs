module Category where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative
import Control.Monad

import Util
import Bit

data Cat obj morph = Cat
  { _cat_objs :: Set obj
  , _cat_morphs :: Set morph
  , _cat_mp_obj_left :: Map obj (Set morph)
  , _cat_mp_obj_right :: Map obj (Set morph)
  , _cat_mp_morph_left :: Map morph obj
  , _cat_mp_morph_right :: Map morph obj
  , _cat_mp_id :: Map obj morph
  , _cat_mp_comp :: Map (morph, morph) morph
  }

mk_cat :: (Ord obj, Ord morph) => Map morph (obj, obj) ->
  Map (morph, morph) morph -> Maybe (Cat obj morph)
mk_cat mp mp_comp = do
  mp_list <- pure # Map.toList mp
  morphs_list <- pure # Map.keys mp
  morphs <- pure # Set.fromList morphs_list
  asrt # and # do
    ((m1, m2), m3) <- Map.toList mp_comp
    m <- [m1, m2, m3]
    return # Set.member m morphs
  objs <- pure # Set.fromList # do
    (o1, o2) <- Map.elems mp
    [o1, o2]
  objs_list <- pure # Set.toList objs
  mk_mp <- pure # foldr <~ Map.empty # uncurry map_set_insert
  mp_obj_left <- pure # mk_mp # do
    (m1, (o1, _)) <- mp_list
    return (o1, m1)
  mp_obj_right <- pure # mk_mp # do
    (m1, (_, o2)) <- mp_list
    return (o2, m1)
  mp_morph_left <- pure # Map.map fst mp
  mp_morph_right <- pure # Map.map snd mp
  -- asrt # and # do
  --   [o1, o2, o3] <- replicateM 3 objs_list
  --   undefined
  -- asrt # and # do
  --   [m1, m2, m3] <- replicateM 3 morphs_list
  --   undefined
  mp_id_list <- pure # do
    o1 <- objs_list
    return # mk_pair o1 # do
      Just left <- pure # fmap Set.toList # map_get o1 mp_obj_left
      Just right <- pure # fmap Set.toList # map_get o1 mp_obj_right
      m_id <- left
      asrt # and # do
        m1 <- left
        Just m1' <- pure # map_get (m1, m_id) mp_comp
        return # m1 == m1'
      asrt # and # do
        m1 <- right
        Just m1' <- pure # map_get (m_id, m1) mp_comp
        return # m1 == m1'
      return m_id
  asrt # all <~ mp_id_list # \(_, xs) -> len xs == 1
  mp_id <- pure # Map.fromList # map <~ mp_id_list #
    \(o1, [m1]) -> (o1, m1)
  return # Cat
    { _cat_objs = objs
    , _cat_morphs = morphs
    , _cat_mp_obj_left = mp_obj_left
    , _cat_mp_obj_right = mp_obj_right
    , _cat_mp_morph_left = mp_morph_left
    , _cat_mp_morph_right = mp_morph_right
    , _cat_mp_id = mp_id
    , _cat_mp_comp = mp_comp
    }