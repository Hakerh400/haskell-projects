module Parser where

import Util

type Sctr = [String]
type ParserScope = (Name, N, N)

data ParserT = ParserT
  { _par_str :: String
  , _par_index :: N
  , _par_scopes :: [ParserScope]
  }

type Parser = StateT ParserT Maybe

instance Foldable Parser where
  foldr f z m = foldr f z # evalStateT m undefined

type ParserM s = StateT s (ExceptT Sctr Parser)

-----

par_get_str :: Parser String
par_get_str = gets _par_str

par_put_str :: String -> Parser ()
par_put_str str = modify # \st -> st {_par_str = str}

par_fail :: Parser a
par_fail = lift Nothing

par_is_eof :: Parser Prop
par_is_eof = do
  st <- get
  return # null # _par_str st

par_eof :: Parser ()
par_eof = do
  True <- par_is_eof
  nop

par_get :: Parser Char
par_get = do
  st <- get
  (c : str) <- pure # _par_str st
  put # st
    { _par_str = str
    , _par_index = _par_index st + 1
    }
  return c

par_scope :: Name -> Parser a -> Parser a
par_scope name m = do
  i <- gets _par_index
  a <- m
  j <- gets _par_index
  modify # \st -> st
    {_par_scopes = (name, i, j) : _par_scopes st}
  return a

par_try :: Parser a -> Parser (Maybe a)
par_try m = do
  st <- get
  case runStateT m st of
    Just (a, st) -> do
      put st
      return # Just a
    Nothing -> pure Nothing

par_save :: Parser ParserT
par_save = get

par_restore :: ParserT -> Parser ()
par_restore = put

instance OrElse (Parser a) where
  m1 <|> m2 = do
    res <- par_try m1
    case res of
      Just a -> pure a
      Nothing -> m2

-----

parse_raw :: Parser a -> String -> Maybe (a, String, [ParserScope])
parse_raw m str = do
  st <- pure # ParserT
    { _par_str = str
    , _par_index = 0
    , _par_scopes = []
    }
  (a, st) <- runStateT m st
  return (a, _par_str st, reverse # _par_scopes st)

parse :: ParserM s a -> String -> Either Sctr a
parse m str = do
  let st = undefined
  pt <- pure # ParserT
    { _par_str = str
    , _par_index = 0
    , _par_scopes = []
    }
  case runStateT (runExceptT # runStateT m st) pt of
    Nothing -> Left ["No parse"]
    Just (res, pt) -> case res of
      Left msg -> Left msg
      Right (a, _) -> do
        let rest = drop (fi # _par_index pt) str
        if null rest then Right a else
          Left ["Syntax error\n\n" ++ rest]

-- parse' :: Parser a -> String -> Either Sctr a
-- parse' m str = case filter (null . snd) # parse_raw m str of
--   [(a, "")] -> Right a
--   (a : b : _) -> Left ["Ambiguous parse"]
--   _ -> Left ["No parse"]
-- 
-- parse :: Parser a -> String -> a
-- parse m str = case parse' m str of
--   Left msg -> error # concat msg
--   Right a -> a

-----

pget :: ParserM s Char
pget = lift2 par_get

pfail :: ParserM s a
pfail = lift2 par_fail

plift :: Parser a -> ParserM s a
plift = lift2

-- type PliftSig0 s m e a = m (Either e (a, s))
-- type PliftSig1 s m e a = StateT s (ExceptT e m) a
-- type PliftSig2 f s m1 m2 m3 e1 e2 e3 a1 a2 a3 =
--   f s m1 e1 a1 -> f s m2 e2 a2 -> f s m3 e3 a3
-- type PliftSig3 f1 f2 s m1 m2 m3 e1 e2 e3 a1 a2 a3 =
--   PliftSig2 f1 s m1 m2 m3 e1 e2 e3 a1 a2 a3 ->
--   PliftSig2 f2 s m1 m2 m3 e1 e2 e3 a1 a2 a3

-- plift2 :: PliftSig3 PliftSig0 PliftSig1 s m1 m2 m3 e1 e2 e3 a1 a2 a3
plift2 f a b = StateT # \s -> ExceptT # f
  (runExceptT # runStateT a s) (runExceptT # runStateT b s)

infixr 5 <++
(<++) :: ParserM s a -> ParserM s a -> ParserM s a
(<++) = plift2 (<|>)

infixr 5 ++>
(++>) :: ParserM s a -> ParserM s a -> ParserM s a
(++>) = flip (<++)

p_save :: ParserM s ParserT
p_save = plift par_save

p_restore :: ParserT -> ParserM s ()
p_restore s = plift # par_restore s

p_query :: ParserM s a -> ParserM s a
p_query m = do
  s <- p_save
  res <- m
  p_restore s
  return res

p_char :: Char -> ParserM s ()
p_char c = pget >>= \c1 ->
  ite (c1 == c) nop pfail

p_char' :: Char -> ParserM s ()
p_char' c = p_char c >> pure ()

p_str :: String -> ParserM s ()
p_str [] = nop
p_str s@(c:cs) = p_char c >> p_str cs

p_scope :: Name -> ParserM s a -> ParserM s a
p_scope name m = do
  i <- lift # gets _par_index
  a <- m
  j <- lift # gets _par_index
  lift # modify # \st -> st
    {_par_scopes = (name, i, j) : _par_scopes st}
  return a

p_all' :: ParserM s a -> ParserM s [a]
p_all' r = pure [] ++> do
  x <- r
  xs <- p_all' r
  return # x : xs

p_all :: ParserM s a -> ParserM s [a]
p_all r = do
  xs <- p_all' r
  ite (null xs) pfail (pure xs)

p_sep' :: ParserM s a -> ParserM s b -> ParserM s ([a], [b])
p_sep' r s = pure ([], []) ++> do
  x <- r
  zs <- p_all' # do
    b <- s
    a <- r
    return (a, b)
  let (xs, ys) = unzip zs
  return (x : xs, ys)

p_sep :: ParserM s a -> ParserM s b -> ParserM s ([a], [b])
p_sep r s = do
  (a, b) <- p_sep' r s
  ite (null a) pfail (pure (a, b))

p_sep1 :: ParserM s a -> ParserM s b -> ParserM s ([a], [b])
p_sep1 r s = do
  (a, b) <- p_sep r s
  ite (null # tail a) pfail (pure (a, b))

p_sepf' :: ParserM s a -> ParserM s b -> ParserM s [a]
p_sepf' r s = do
  result <- p_sep' r s
  return # fst result

p_sepf :: ParserM s a -> ParserM s b -> ParserM s [a]
p_sepf r s = do
  result <- p_sep r s
  return # fst result

p_take_while :: (Char -> Prop) -> ParserM s String
p_take_while f = p_all' # pget >>= \c ->
  ite (f c) (pure c) pfail

p_drop_while :: (Char -> Prop) -> ParserM s ()
p_drop_while f = p_take_while f >> pure ()

p_take_while1 :: (Char -> Prop) -> ParserM s String
p_take_while1 f = do
  xs <- p_take_while f
  if null xs then pfail else pure xs

p_digit :: ParserM s N
p_digit = do
  c <- pget
  True <- pure # is_digit c
  return # fi # ord c - 48

is_ident_char :: Char -> Prop
is_ident_char c = or
  [ is_letter c
  , is_digit c
  , is_sub_digit c
  , is_greek c
  , c == '\x5F'
  , c == '\''
  ]

p_ident :: ParserM s String
p_ident = trimmed # do
  c <- pget
  if c == usc' || is_letter c || is_greek c then do
    s <- p_take_while is_ident_char
    return # c : s
  else pfail

p_ident' :: ParserM s String
p_ident' = trimmed # p_take_while1 is_ident_char

p_is_valid_nat :: String -> Prop
p_is_valid_nat s = s == "0" ||
  ( not (null s) &&
    all is_digit s &&
    any (/= '0') s )

p_nat :: ParserM s N
p_nat = do
  s <- trimmed # p_take_while1 is_digit
  ite (p_is_valid_nat s) (pure # (read s :: N)) pfail

p_nat' :: ParserM s N
p_nat' = p_maybe' 0 p_nat

p_int :: ParserM s N
p_int = trimmed # do
  k <- choice
    [ p_char '+' >> pure 1
    , p_char '-' >> pure (-1)
    , pure 1
    ]
  n <- p_nat
  return # n * k

p_surrounded :: String -> String -> ParserM s a -> ParserM s a
p_surrounded s1 s2 r = do
  p_tok s1
  a <- r
  p_tok s2
  return a

p_parens :: ParserM s a -> ParserM s a
p_parens = p_surrounded "(" ")"

p_brackets :: ParserM s a -> ParserM s a
p_brackets = p_surrounded "[" "]"

p_braces :: ParserM s a -> ParserM s a
p_braces = p_surrounded "{" "}"

p_either :: ParserM s a -> ParserM s b -> ParserM s (Either a b)
p_either a b = (a >>= liftm Left) <++ (b >>= liftm Right)

-- p_either' :: ParserM s a -> ParserM s b -> ParserM s (Either a b)
-- p_either' a b = do
--   s <- get
--   f1 <- pure # do
--     res <- runExceptT # runStateT a s
--     case res of
--       Left _ -> par_fail
--       Right a -> pure # Just a
--   res <- lift2 # par_orelse f1 # pure Nothing
--   case res of
--     Nothing -> b >>= liftm Right
--     Just (x, s) -> do
--       put s
--       return # Left x

p_sp :: ParserM s ()
p_sp = p_char' space

p_rest :: ParserM s String
p_rest = p_take_while # const True

p_eof :: ParserM s ()
p_eof = plift par_eof

choice :: [ParserM s a] -> ParserM s a
choice xs = if null xs then pfail else foldr1 (<++) xs

p_new_line' :: ParserM s ()
p_new_line' = p_str "\r\n" <++ choice [p_str "\r", p_str "\n"]

p_new_line :: ParserM s ()
p_new_line = p_new_line' >> nop

p_ws_aux :: Prop -> ParserM s ()
p_ws_aux one = do
  lf <- pure True
  let f = ite one p_all p_all'
  let p = choice [p_sp, p_new_line]
  f p >> nop

p_ws :: ParserM s ()
p_ws = p_ws_aux False

p_ws1 :: ParserM s ()
p_ws1 = p_ws_aux True

trimmed_aux :: ParserM s () -> ParserM s a -> ParserM s a
trimmed_aux ws r = do
  ws
  x <- r
  ws
  return x

trimmed :: ParserM s a -> ParserM s a
trimmed = trimmed_aux p_ws

p_tok :: String -> ParserM s ()
p_tok t = trimmed # p_str t

p_tok' :: String -> ParserM s String
p_tok' t = trimmed (p_str t) >> pure t

p_toks :: [String] -> ParserM s ()
p_toks = mapM_ p_tok

p_maybe :: ParserM s a -> ParserM s (Maybe a)
p_maybe p = (p >>= liftm Just) <++ pure Nothing

p_maybe' :: a -> ParserM s a -> ParserM s a
p_maybe' z p = do
  res <- p_maybe p
  return # fromMaybe z res

p_assert :: Prop -> ParserM s ()
p_assert p = if p then nop else pfail

parse_word :: Name -> ParserM s ()
parse_word name = do
  name' <- p_ident
  if name' == name then nop else pfail

p_usc :: ParserM s ()
p_usc = parse_word usc >> nop

-----

p_list_aux' :: String -> String -> Prop -> Prop -> ParserM s a -> ParserM s [a]
p_list_aux' tok1 tok2 empty trailing m = do
  p_tok tok1
  let sep = p_tok ","
  let f = ite empty p_sepf' p_sepf
  xs <- f m sep
  if trailing then p_maybe sep >> nop else nop
  p_tok tok2
  return xs

p_list_aux :: String -> String -> Prop -> Prop -> Prop -> ParserM s a -> ParserM s [a]
p_list_aux tok1 tok2 empty single trailing m = do
  let m1 = p_list_aux' tok1 tok2 empty trailing m
  if single then m1 <++ (m >>= liftm pure) else m1

p_list_parens :: Prop -> Prop -> Prop -> ParserM s a -> ParserM s [a]
p_list_parens = p_list_aux "(" ")"

p_list_brackets :: Prop -> Prop -> Prop -> ParserM s a -> ParserM s [a]
p_list_brackets = p_list_aux "[" "]"

p_list_braces :: Prop -> Prop -> Prop -> ParserM s a -> ParserM s [a]
p_list_braces = p_list_aux "{" "}"

p_list :: ParserM s a -> ParserM s [a]
p_list = p_list_brackets False False False

p_list' :: ParserM s a -> ParserM s [a]
p_list' = p_list_brackets False True False

p_list0 :: ParserM s a -> ParserM s [a]
p_list0 = p_list_brackets True False False

p_list0' :: ParserM s a -> ParserM s [a]
p_list0' = p_list_brackets True True False

p_prop :: ParserM s a -> ParserM s Prop
p_prop p = pure False ++> (p >> pure True)