import qualified Data.Set as Set
import qualified Data.Map as Map

import Util
import Parser

src_file = "src.txt"
inp_file = "inp.txt"

hidden_var_name = usc

data ProgT = ProgT
  { _prog_exprs :: [Expr]
  , _prog_expr :: Expr
  } deriving (Eq, Ord, Show)

type ProgP = ParserM ProgT
type Prog = StateT ProgT (Except Sctr)

newtype Expr = Expr (Map Name N)
  deriving (Eq, Ord)

show_expr :: Expr -> String
show_expr (Expr mp) = run # do
  xs <- pure # filter <~ Map.toList mp # \(s, n) ->
    s /= hidden_var_name && n /= 0
  xs <- pure # sortBy <~ xs # \(s1, n1) (s2, n2) ->
    if signum n1 == signum n2
      then compare s1 s2 else compare n1 n2
  s <- pure # tail' # do
    (s, n) <- xs
    let na = abs n
    let sna = ite (na == 1) "" # show na
    let sn = ' ' : ite (n > 0) '+' '-' : ' ' : sna
    concat [sn, s]
  return # case s of
    "" -> "/"
    ('+' : ' ' : s) -> s
    ('-' : ' ' : s) -> '-' : s

instance Show Expr where
  show = show_expr

main :: IO ()
main = do
  src <- readFile src_file
  inp <- readFile inp_file
  case parse_and_run_prog src inp of
    Left msg -> putStrLn # concat msg
    Right res -> print res

expr_is_pos :: Expr -> Prop
expr_is_pos (Expr mp) = isNothing # map_find (< 0) mp

init_prog :: [Expr] -> Expr -> ProgT
init_prog exprs inp = ProgT
  { _prog_exprs = exprs
  , _prog_expr = inp
  }

parse_expr_term :: ProgP (Name, N)
parse_expr_term = do
  n <- pure 1 ++> p_nat
  s <- p_ident
  return (s, n)

parse_expr :: ProgP Expr
parse_expr = do
  ((s, n) : xs, ks) <- p_sep parse_expr_term # choice
    [ p_tok "+" >> pure 1
    , p_tok "-" >> pure (-1) ]
  let e = Just # Map.singleton s n
  Just e <- pure # fold_l (zip ks xs) e #
    \e (k, (s, n)) -> e >>= map_insert' s (k * n)
  return # Expr e

parse_expr_pos :: ProgP Expr
parse_expr_pos = do
  e <- parse_expr
  True <- pure # expr_is_pos e
  return e

parse_prog :: Expr -> ProgP ProgT
parse_prog inp = do
  exprs <- trimmed # p_all' parse_expr
  return # init_prog exprs inp

run_prog' :: Map Name N -> [Expr] -> Prog Expr
run_prog' e xs = case xs of
  [] -> get_result
  Expr mp : xs -> do
    x <- pure # Expr # map_fold_r mp e # Map.insertWith (+)
    if not # expr_is_pos x then run_prog' e xs else do
      modify # \prog -> prog {_prog_expr = x}
      run_prog

run_prog :: Prog Expr
run_prog = do
  Expr e <- gets _prog_expr
  xs <- gets _prog_exprs
  run_prog' e xs

get_result :: Prog Expr
get_result = gets _prog_expr

parse_and_run_prog :: String -> String -> Either Sctr Expr
parse_and_run_prog src inp = do
  inp <- parse parse_expr_pos inp
  prog <- parse (parse_prog inp) src
  runExcept # evalStateT run_prog prog