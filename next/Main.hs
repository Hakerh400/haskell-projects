import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util
import Bit
import Nxt

main :: IO ()
main = do
  let inp = 5
  print # run_prog prog inp

prog :: N -> Nxt N
prog inp = do
  set_var "x" 5
  x <- next "x" 1
  set_var "x" (x + 7)
  return x