module Nxt where

import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.State
import Control.Monad.Trans.Cont

import Util
import Bit

data NxtState = NxtState
  { _nxt_ticks :: Maybe N
  , _nxt_asgns :: Map Name N
  } deriving (Eq, Ord, Show)

type Nxt' = State NxtState
type Nxt = ContT N Nxt'

init_nxt_state :: NxtState
init_nxt_state = NxtState
  { _nxt_ticks = Nothing
  , _nxt_asgns = Map.empty
  }

get_var' :: Name -> Nxt' N
get_var' name = do
  mp <- gets _nxt_asgns
  return # Map.findWithDefault 0 name mp

get_var :: Name -> Nxt N
get_var name = lift # get_var' name

set_var :: Name -> N -> Nxt ()
set_var name n = do
  ticks <- lift # gets _nxt_ticks
  case ticks of
    Nothing -> nop
    Just n -> if n > 0
      then lift # modify # \st -> st
        {_nxt_ticks = Just # n - 1}
      else do
        st <- lift get
        ContT # \f -> do
          put st
          return 0
  mp <- lift # gets _nxt_asgns
  mp <- pure # if n == 0
    then Map.delete name mp
    else Map.insert name n mp
  lift # modify # \st -> st {_nxt_asgns = mp}

next :: Name -> N -> Nxt N
next name n = ContT # \f -> do
  v0 <- get_var' name
  st <- get
  put # st {_nxt_ticks = Just n}
  f v0
  v <- get_var' name
  put st
  f v

run_prog' :: (N -> Nxt N) -> N -> (N, NxtState)
run_prog' prog inp = runState <~ init_nxt_state #
  evalContT # prog inp

run_prog :: (N -> Nxt N) -> N -> N
run_prog prog inp = fst # run_prog' prog inp