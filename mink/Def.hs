module Def
  ( def0
  , def1
  , def2
  , def3
  ) where

import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Base
import Kernel
import Tactic

rc :: Expr
rc = Var "__rec"

def0 :: Name -> Expr -> Theory Expr
def0 name body = do
  def <- define name [] body
  return # call [Ref name]

def1 :: Name -> Expr -> Theory (Expr -> Expr)
def1 name body = do
  def <- define name ["a"] body
  return # \a -> call [Ref name, a]

def2 :: Name -> Expr -> Theory (Expr -> Expr -> Expr)
def2 name body = do
  def <- define name ["a", "b"] body
  return # \a b -> call [Ref name, a, b]

def3 :: Name -> Expr -> Theory (Expr -> Expr -> Expr -> Expr)
def3 name body = do
  def <- define name ["a", "b", "c"] body
  return # \a b c -> call [Ref name, a, b, c]