import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import Text.Read
import System.IO

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Base
import Kernel
import Tactic
import Def

src_dir = "src/"
defs_file = src_dir ++ "defs.txt"
proofs_file = src_dir ++ "proofs.txt"

main :: IO ()
main = do
  mapM_ (`hSetBuffering` NoBuffering) [stdin, stdout, stderr]
  defs_src <- readFile defs_file
  proofs_src <- readFile proofs_file
  th <- repl # th_add_defs defs_src th_init
  -- clear_screen
  putStrLn "\n---\n"
  print th

repl :: TheoryT -> IO TheoryT
repl th = do
  lemma <- read_lemma
  case lemma of
    Nothing -> do
      putStrLn "Syntax error"
      -- repl th
      return th
    Just lemma -> do
      proof <- pure # init_proof lemma th
      proof <- pure # apply_tac simp_plus proof
      (tacs, proof) <- proof_mode proof
      th <- pure # th_add_proof proof
      -- return # build_proof_t lemma proof th
      return th

proof_mode :: ProofT -> IO ([String], ProofT)
proof_mode = proof_mode' []

proof_mode' :: [String] -> ProofT -> IO ([String], ProofT)
proof_mode' tacs proof = do
  putStrLn ""
  print proof
  if null # _goals proof
    then return (reverse tacs, proof)
    else do
      line <- read_line
      case readMaybe line of
        Nothing -> do
          putStrLn "Unknown tactic"
          proof_mode' tacs proof
        Just (Tactic tac) -> do
          proof <- pure # apply_tac (tac >> simp_plus) proof
          proof_mode' (line : tacs) proof

read_lemma :: IO (Maybe Lemma)
read_lemma = do
  -- line <- read_line
  line <- pure "lem a b c : Prop a -> Prop b -> Prop c -> iff (iff a (iff b c)) (iff (iff a b) c)"
  return # readMaybe line

read_line :: IO String
read_line = do
  putStr "\n>>> "
  str <- getLine
  clear_screen
  return str

clear_screen :: IO ()
clear_screen = putStr "\ESC[2J\ESC[0;0H\ESCc"