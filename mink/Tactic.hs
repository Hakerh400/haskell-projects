module Tactic where

import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import Debug.Trace
import GHC.Read
import Text.ParserCombinators.ReadPrec
import Prelude hiding (log)

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Base
import ParserBase
import Kernel

do_simp :: N
do_simp = 1

simp_after :: Proof () -> Proof ()
simp_after tac = do
  goals1 <- gets _goals
  let ids = tail # map _id goals1
  tac
  goals2 <- gets _goals
  simp_goals # filter (\goal -> not # elem (_id goal) ids) goals2

refl :: Proof ()
refl = tac_refl

sp :: Proof ()
sp = simp_after tac_split

exact :: N -> Proof ()
exact i = do
  Equality p a b <- get_asm i
  if not p then cpos i >> exact i else do
    tac_nth_rewrite_goal i 0
    tac_refl

clear :: N -> Proof ()
clear = tac_clear

get_last_asm_i :: Proof N
get_last_asm_i = do
  n <- get_asms_num
  return # n - 1

symmetry :: Proof ()
symmetry = simp_after # do
  eq <- get_goal_eq
  tac_have # flip_eq eq
  focus_nth 1
  let cnt = count_expr (_rhs eq) (_lhs eq)
  n <- get_last_asm_i
  tac_nth_rewrite_goal n cnt

symmetry_at :: N -> Proof ()
symmetry_at n = simp_after # do
  eq <- get_asm n
  tac_have # flip_eq eq
  let cnt = count_expr (_lhs eq) (_rhs eq)
  tac_nth_rewrite_goal n cnt
  refl
  k <- get_last_asm_i
  tac_swap_asms n k
  clear k

nth_rw_at :: N -> N -> N -> Proof ()
nth_rw_at src_i asm_i n = tac_nth_rewrite_asm src_i asm_i n

nth_rw :: N -> N -> Proof ()
nth_rw src_i n = tac_nth_rewrite_goal src_i n

rw_at :: N -> N -> Proof ()
rw_at src_i asm_i = tac_rewrite_asm src_i asm_i

rw :: N -> Proof ()
rw src_i = tac_rewrite_goal src_i

unfold_at :: Name -> N -> Proof ()
unfold_at name n = simp_after #
  tac_nth_unfold_asm name n 0

unfold :: Name -> Proof ()
unfold name = simp_after #
  tac_nth_unfold_goal name 0

unf_at :: N -> Proof ()
unf_at n = do
  eq <- get_asm n
  let Just name = get_unf_target_name # _lhs eq
  unfold_at name n

unf :: Proof ()
unf = do
  eq <- get_goal_eq
  let Just name = get_unf_target_name # _lhs eq
  unfold name

get_unf_target_name :: Expr -> Maybe Name
get_unf_target_name expr = do
  let (target, args) = get_call_info expr
  case target of
    Ref name -> pure name
    _ -> msum # map get_unf_target_name args

dsimp_at :: N -> Proof ()
dsimp_at = tac_dsimp_asm

dsimp :: Proof ()
dsimp = tac_dsimp_goal

have :: Equality -> Proof ()
have eq = simp_after # tac_have eq

cpos :: N -> Proof ()
cpos i = tac_contrapose i

cases :: N -> Proof ()
cases i = simp_after # tac_cases i

contra :: Proof ()
contra = do
  tac_have # mk_prop_n 1
  tac_have # mk_eq Nil Nil
  refl
  tac_contrapose 0
  tac_cases 0
  n <- get_last_asm_i
  cpos n

triv :: Proof ()
triv = refl

not_false :: Proof ()
not_false = do
  contra
  n <- get_last_asm_i
  tac_cases n

elim :: N -> Proof ()
elim n = do
  contra
  exact n

norm_at :: N -> Proof ()
norm_at = tac_norm_asm

norm :: Proof ()
norm = tac_norm_goal

norm_all :: Proof ()
norm_all = do
  n <- get_last_asm_i
  mapM_ norm_at [0..n]
  norm

purge :: Proof ()
purge = tac_purge_vars

subst :: N -> Proof ()
subst n = simp_after # do
  subst' n
  clear n

subst' :: N -> Proof ()
subst' n = do
  num <- get_last_asm_i
  mapM_ (\k -> ite (k /= n) (rw_at n k) (pure ())) [0..num]
  rw n

ind_get_arg_names :: [Expr] -> Either Expr [Name]
ind_get_arg_names [] = Right []
ind_get_arg_names (expr@(Var name):xs) = do
  names <- ind_get_arg_names xs
  ite (elem name names) (Left expr) (Right # name : names)
ind_get_arg_names (expr:xs) = Left expr

induct :: N -> Maybe [[Expr]] -> Proof ()
induct n sp_lists = simp_after # do
  goal <- get_goal
  Equality True lhs Nil <- get_asm n
  let (target, args) = get_call_info lhs
  case ind_get_arg_names args of
    Left expr -> do
      tac_let' expr
      i <- get_last_asm_i
      tac_nth_rewrite_asm i n # ite (expr == target) 1 0
      induct n sp_lists
    Right names -> do
      let vars = _goal_vars goal
      let spec_vars = Set.toList # Set.difference vars # Set.fromList names
      let spec_lists = maybe (map (\a -> [Var a]) spec_vars) id sp_lists
      tac_induction n spec_lists

cases_ite :: N -> Proof ()
cases_ite i = simp_after # tac_ite i

focus :: Goal -> Proof ()
focus goal = tac_focus_goal # _id goal

simp_n :: N -> Proof ()
simp_n num = do
  goals <- gets _goals
  simp_goals # take (fi num) goals

simp_goals :: [Goal] -> Proof ()
simp_goals = mapM_ simp_goal

is_ite :: Expr -> Prop
is_ite (Call (Call (Call Nil _) _) _) = True
is_ite _ = False

simp_rules :: [(Prop -> Expr -> Expr -> Prop, N -> Proof ())]
simp_rules =
  [ (\p a b -> p && a == b, \i -> clear i >> simp)
  , (\p a b -> p && is_var a && not (expr_has_var (get_name a) b), subst)
  , (\p a b -> p && is_var b && not (expr_has_var (get_name b) a), \i -> symmetry_at i >> simp)
  , (\p a b -> p && is_ctor a && is_ctor b, cases)
  , (\p a b -> not p && is_ctor a && is_ctor b && not (same_ctor a b), \i -> clear i >> simp)
  , (\p a b -> p && is_ite a && is_ctor b, cases_ite)
  , (\p a b -> not p && a == b, \i -> cpos i >> refl)
  ]

simp :: Proof ()
simp = do
  goals <- gets _goals  
  if null goals then nop else get_goal >>= simp_goal

simp_goal :: Goal -> Proof ()
simp_goal goal = if do_simp == 0 then nop else do
  h <- has_goal goal
  if not h then nop else do
    focus goal
    norm_all
    tac_nub_asms
    tac_purge_vars
    asms <- get_asms
    eq@(Equality p a b) <- get_goal_eq
    if p && a == b then refl else
      if p && is_pair a && is_pair b then sp else
        case elemIndex eq asms of
          Just i -> exact # fi i
          Nothing -> case findIndex (\eq -> elem (negate_eq eq) asms) asms of
            Just i -> cpos # fi i
            Nothing -> do
              if not p && is_ctor a && is_ctor b && not (same_ctor a b) then not_false else do
                asms <- get_asms
                maybe nop id # msum # flip map simp_rules #
                  \(f, m) -> findIndex (\(Equality p a b) -> f p a b) asms >>= \i -> Just # m # fi i

simp_plus :: Proof ()
simp_plus = do
  simp
  tac_sort_goals

has_goal :: Goal -> Proof Prop
has_goal goal = do
  goals <- gets _goals
  return # elem goal goals

call :: [Expr] -> Expr
call = exprs_to_call

cref :: Name -> [Expr] -> Expr
cref name args = call # Ref name : args

have_expr :: Expr -> Proof ()
have_expr expr = have # mk_prop expr

have_n :: Expr -> Proof ()
have_n expr = have # mk_prop_n expr

focus_nth :: N -> Proof ()
focus_nth n = do
  goals <- gets _goals
  focus # goals !! fi n

by_cases :: Equality -> Proof ()
by_cases eq = simp_after # tac_by_cases eq

newtype Tactic = Tactic (Proof ())

instance Read Tactic where
  readPrec = parse_tac

parse_tac :: ReadPrec Tactic
parse_tac = trimmed # choice
  [ parse_tac_0 "simp" simp
  , parse_tac_0 "unf" unf
  , parse_tac_0 "ctr" contra
  , parse_tac_0 "sym" symmetry
  , parse_tac_0 "sorry" tac_sorry
  , parse_tac_0 "refl" tac_refl
  , parse_tac_1n "unf" unf_at
  , parse_tac_1n "ite" cases_ite
  , parse_tac_1n "sym" symmetry_at
  , parse_tac_1n "rw" rw
  , parse_tac_2n "rw" rw_at
  , parse_tac_2n "rwn" nth_rw
  , parse_tac_3n "rwn" nth_rw_at
  , parse_tac_1n "sub" subst
  , parse_tac_1n "cpos" cpos
  , parse_tac_1n "clear" clear
  , parse_tac_1n "exact" exact
  , parse_tac_unf_name_at
  , parse_tac_unf_name
  , parse_tac_nth_unf_name_at
  , parse_tac_nth_unf_name
  , parse_tac_have
  , parse_tac_let
  , parse_tac_let'
  , parse_tac_ind
  , parse_tac_ind_gen
  , parse_tac_by_cases
  ]

parse_tac_0 :: Name -> Proof () -> ReadPrec Tactic
parse_tac_0 name tac = p_str name >> pure (Tactic tac)

parse_tac_1n :: Name -> (N -> Proof ()) -> ReadPrec Tactic
parse_tac_1n name tac = do
  p_str name
  n <- trimmed parse_nat
  return # Tactic # tac n

parse_tac_2n :: Name -> (N -> N -> Proof ()) -> ReadPrec Tactic
parse_tac_2n name tac = do
  p_str name
  n1 <- trimmed parse_nat
  n2 <- trimmed parse_nat
  return # Tactic # tac n1 n2

parse_tac_3n :: Name -> (N -> N -> N -> Proof ()) -> ReadPrec Tactic
parse_tac_3n name tac = do
  p_str name
  n1 <- trimmed parse_nat
  n2 <- trimmed parse_nat
  n3 <- trimmed parse_nat
  return # Tactic # tac n1 n2 n3

parse_tac_have :: ReadPrec Tactic
parse_tac_have = do
  p_str "have"
  eq <- parse_eq
  return # Tactic # do
    goal <- get_goal
    let vars = _goal_vars goal
    have # put_vars_eq vars eq

parse_tac_let :: ReadPrec Tactic
parse_tac_let = do
  p_str "let"
  expr <- parse_expr
  return # Tactic # tac_let expr

parse_tac_let' :: ReadPrec Tactic
parse_tac_let' = do
  p_str "let'"
  expr <- parse_expr
  return # Tactic # tac_let' expr

parse_tac_unf_name_at :: ReadPrec Tactic
parse_tac_unf_name_at = do
  p_str "unf"
  asm_i <- trimmed parse_nat
  name <- trimmed parse_name
  return # Tactic # tac_nth_unfold_asm name asm_i 0

parse_tac_unf_name :: ReadPrec Tactic
parse_tac_unf_name = do
  p_str "unf"
  name <- trimmed parse_name
  return # Tactic # tac_nth_unfold_goal name 0

parse_tac_nth_unf_name_at :: ReadPrec Tactic
parse_tac_nth_unf_name_at = do
  p_str "unf"
  asm_i <- trimmed parse_nat
  name <- trimmed parse_name
  n <- trimmed parse_nat
  return # Tactic # tac_nth_unfold_asm name asm_i n

parse_tac_nth_unf_name :: ReadPrec Tactic
parse_tac_nth_unf_name = do
  p_str "unf"
  name <- trimmed parse_name
  n <- trimmed parse_nat
  return # Tactic # tac_nth_unfold_goal name n

parse_tac_ind :: ReadPrec Tactic
parse_tac_ind = do
  p_str "ind"
  n <- trimmed parse_nat
  return # Tactic # simp_after # induct n Nothing

parse_tac_ind_gen :: ReadPrec Tactic
parse_tac_ind_gen = do
  p_str "ind"
  n <- trimmed parse_nat
  spec_lists <- trimmed # (readPrec :: ReadPrec [[Expr]])
  return # Tactic # simp_after # induct n (Just spec_lists)

parse_tac_by_cases :: ReadPrec Tactic
parse_tac_by_cases = do
  p_str "bcs"
  eq <- trimmed parse_eq
  return # Tactic # do
    goal <- get_goal
    let vars = _goal_vars goal
    by_cases # put_vars_eq vars eq