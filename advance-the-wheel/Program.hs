module Program (run_program, run_program') where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)
import Control.Monad.State

import Base
import Util
import Queue

type Queues = Map N Queue

data ProgT = MkProg
  { _input_len :: N,
    _queues    :: Queues,
    _src       :: Bits,
    _sptr      :: N,
    _qptr      :: N,
    _cptr      :: N,
    _done      :: Prop }

instance Show ProgT where
  show = evalState show_prog

type Prog = State ProgT

run_program :: Bits -> [Bits] -> Bits -> Bits
run_program src args inp = evalState exec_prog # init_prog src args inp

run_program' :: Bits -> [Bits] -> Bits -> ProgT
run_program' src args inp = execState exec_prog # init_prog src args inp

init_prog :: Bits -> [Bits] -> Bits -> ProgT
init_prog src args inp = MkProg
  { _input_len = fi # length inp,
    _queues    = init_queues args inp,
    _src       = src,
    _sptr      = 0,
    _qptr      = 1,
    _cptr      = 0,
    _done      = False }

init_queues :: [Bits] -> Bits -> Queues
init_queues args inp = Map.fromList #
  zipWith (,) [1..] (map q_mk args) ++ if null inp then [] else [(0, q_mk inp)]

exec_prog :: Prog Bits
exec_prog = do
  run_prog
  out <- get_out
  return out

get_io :: Prog (Bits, Bits)
get_io = do
  i <- gets _input_len
  q <- get_queue 0
  return # splitAt (fi i) # q_bits q

get_inp :: Prog Bits
get_inp = do
  io <- get_io
  return # fst io

get_out :: Prog Bits
get_out = do
  io <- get_io
  return # snd io

run_prog :: Prog ()
run_prog = do
  done <- gets _done
  if done then pure () else do
    b <- get_src_bit
    cmd <- get_cmd
    if b then run_cmd cmd else pure ()
    run_prog

run_cmd :: N -> Prog ()
run_cmd 0 = do
  qi <- get_cur_qi
  b <- pop_bit qi
  if b then advance else pure ()
run_cmd 2 = modify # \p -> p {_qptr = _qptr p - 1}
run_cmd 3 = get_cur_qi >>= push_bit 0
run_cmd 4 = get_cur_qi >>= push_bit 1
run_cmd 6 = advance
run_cmd 8 = modify # \p -> p {_qptr = _qptr p + 1}
run_cmd _ = pure ()

get_src_bit :: Prog Bit
get_src_bit = do
  src <- gets _src
  i <- gets _sptr
  modify # \p -> p {_sptr = (i + 1) `mod` fi (length src)}
  return # src !! fi i

get_cmd :: Prog N
get_cmd = do
  cptr <- gets _cptr
  advance
  return cptr

advance :: Prog ()
advance = do
  cptr <- gets _cptr
  modify # \p -> p {_cptr = (cptr + 1) `mod` 9}

get_queue :: N -> Prog Queue
get_queue i = do
  qs <- gets _queues
  return # Map.findWithDefault q_empty i qs

set_queue :: N -> Queue -> Prog ()
set_queue i q = do
  qs <- gets _queues
  let qs' = if q_null q then Map.delete i qs else Map.insert i q qs
  modify # \p -> p {_queues = qs'}

pop_bit :: N -> Prog Bit
pop_bit i = do
  n <- gets _input_len
  if i == 0 && n == 0 then halt 0 else do
    if i == 0 then modify # \p -> p {_input_len = n - 1} else pure ()
    q <- get_queue i
    if q_null q then halt 0 else do
      let (b, q') = q_pop q
      set_queue i q'
      return b

push_bit :: Bit -> N -> Prog ()
push_bit b i = do
  done <- gets _done
  if done then return () else do
    q <- get_queue i
    set_queue i # q_push b q

get_cur_qi :: Prog N
get_cur_qi = do
  qptr <- gets _qptr
  return # calc_qi_from_qptr qptr

calc_qi_from_qptr :: N -> N
calc_qi_from_qptr 0 = 0
calc_qi_from_qptr n = if odd n then 1 else 1 + calc_qi_from_qptr (n `div` 2)

halt :: a -> Prog a
halt x = do
  modify # \p -> p {_done = True}
  return x

get_qi_min :: Prog N
get_qi_min = do
  qs <- gets _queues
  return # if Map.null qs then undefined else head # Map.keys qs

get_qi_max :: Prog N
get_qi_max = do
  qs <- gets _queues
  return # if Map.null qs then undefined else last # Map.keys qs

get_qi_len :: Prog N
get_qi_len = do
  qs <- gets _queues
  if Map.null qs then return 0 else do
    qi_min <- get_qi_min
    qi_max <- get_qi_max
    return # qi_max - qi_min + 1

get_qi_bounds :: Prog (N, N)
get_qi_bounds = do
  qi_min <- get_qi_min
  qi_len <- get_qi_len
  return (qi_min, qi_len)

get_qi_indices :: Prog [N]
get_qi_indices = do
  (qi_min, qi_len) <- get_qi_bounds
  return # map (qi_min+) [0 .. qi_len - 1]

show_prog :: Prog String
show_prog = do
  (inp, out) <- get_io
  indices <- get_qi_indices
  let pad = max_str_len indices
  qstr <- mapM (mk_qstr_for_index pad) indices
  return # concat
    [ "inp:", mk_bits_str inp, "\n",
      "out:", mk_bits_str out, "\n",
      "queues:", concat qstr ]

mk_bits_str :: Bits -> String
mk_bits_str bs = if null bs then "" else " " ++ show_bits bs

mk_qstr_for_index :: N -> N -> Prog String
mk_qstr_for_index pad i = do
  q <- get_queue i
  return # concat
    [ "\n", indent 1,
      pad_start pad sp # show i, ":",
      mk_bits_str # q_bits q ]