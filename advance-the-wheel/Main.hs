import System.Environment

import Base
import Util
import Program

src_file :: FilePath
src_file = "src.txt"

inp_file :: FilePath
inp_file = "inp.txt"

emulated_args :: Maybe [String]
emulated_args = Just ["arg.txt"]

main :: IO ()
main = do
  src <- read_file src_file
  inp <- read_file inp_file
  args <- get_cmd_args >>= mapM read_file
  let out = run_program' src args inp
  print out

read_file :: FilePath -> IO Bits
read_file pth = do
  str <- readFile pth
  return # str_to_bits str

get_cmd_args :: IO [String]
get_cmd_args = maybe getArgs pure emulated_args

str_to_bits :: String -> Bits
str_to_bits = map char_to_bit . filter (`elem` "01")

char_to_bit :: Char -> Bit
char_to_bit = ('1'==)