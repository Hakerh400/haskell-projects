module Base where

sorry = error "Sorry"

infixr 0 #
(#) = id

type N = Integer
type Prop = Bool
type Bit = Bool
type Bits = [Bit]

instance Num Bool where
  (+) = (/=)
  (-) = (/=)
  (*) = (&&)
  abs = id
  signum = id
  fromInteger = odd

show_bit :: Bit -> String
show_bit b = if b then "1" else "0"

show_bits :: Bits -> String
show_bits = (>>= show_bit)

pp :: (Show a) => a -> b
pp = error . show