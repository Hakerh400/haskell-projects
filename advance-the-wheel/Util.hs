module Util where

import Base

max_str_len :: (Show a) => [a] -> N
max_str_len = fromIntegral . maximum . map (length . show)

pad_start :: N -> Char -> String -> String
pad_start n c s = mk_pad_str n c s ++ s

pad_end :: N -> Char -> String -> String
pad_end n c s = s ++ mk_pad_str n c s

mk_pad_str :: N -> Char -> String -> String
mk_pad_str n c s = replicate (fromIntegral n - length s) c

indent :: N -> String
indent n = replicate (fromIntegral n * 2) sp

sp :: Char
sp = ' '

fi :: (Integral a, Num b) => a -> b
fi = fromIntegral