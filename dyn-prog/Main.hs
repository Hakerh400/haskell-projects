import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Numeric 
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util
import DynProg

main :: IO ()
main = do
  mapM_ (print . fib) [1 .. 10 ^ 3]

fib :: N -> N
fib = dyn_prog_mk # \n -> ite (n <= 1) (pure 1) # do
  a <- dyn_prog_calc (n - 1)
  b <- dyn_prog_calc (n - 2)
  return # a + b