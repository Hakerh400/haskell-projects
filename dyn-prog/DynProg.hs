module DynProg where

import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Numeric 
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util

data DynProgT k v = DynProgT
  { _dyn_prog_fn :: k -> DynProg k v
  , _dyn_prog_mp :: Map k v
  }

type DynProg k v = State (DynProgT k v) v

dyn_prog_calc :: (Ord k) => k -> DynProg k v
dyn_prog_calc k = do
  s <- get
  case map_get' k # _dyn_prog_mp s of
    Just v -> pure v
    Nothing -> do
      v <- _dyn_prog_fn s k
      modify # \s -> s
        {_dyn_prog_mp = map_insert k v # _dyn_prog_mp s}
      return v

dyn_prog_mk :: (Ord k) => (k -> DynProg k v) -> k -> v
dyn_prog_mk f k = evalState (f k) # DynProgT
  { _dyn_prog_fn = f
  , _dyn_prog_mp = Map.empty
  }