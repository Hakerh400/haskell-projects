import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Data.Ratio
import Numeric 
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util
import DynProg

sides_num = 20

main :: IO ()
main = do
  mapM_ <~ [1 .. 50] # \n -> do
    let s = show_rat 20 # calc_p n
    putStrLn # concat
      [pad_end 4 space # show n, " - ", s]

calc_p :: N -> Rational
calc_p = dyn_prog_mk # \n ->
  if n < 0 then pure 0 else
  if n == 0 then pure 1 else do
    s <- (\f -> foldM f 0 [1 .. sides_num]) # \a k -> do
      p <- dyn_prog_calc # n - k
      return # a + p
    return # s / fi sides_num

show_rat :: N -> Rational -> String
show_rat prec n = ("0." ++) # pad_start prec '0' # show #
    div (numerator n * 10 ^ prec) (denominator n)