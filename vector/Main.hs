{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE UndecidableInstances #-}

import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import GHC.Types
import Data.Type.Equality
import Unsafe.Coerce
import Data.Void
import Control.Monad.Identity

import Util

-----

type SProp :: Prop -> Type
data SProp a where
  STrue :: SProp True
  SFalse :: SProp False

type Proof :: Prop -> Type
data Proof a where
  Triv :: Proof True

type Imp :: Prop -> Prop -> Prop
type family Imp a b where
  Imp True b = b
  Imp False _ = True

imp_refl :: SProp a -> Proof (Imp a a)
imp_refl a = case a of
  STrue -> Triv
  SFalse -> Triv

type Not :: Prop -> Prop
type family Not a where
  Not True = False
  Not False = True

-- type Exi :: (k -> Type) -> Type
-- data Exi f where
--   Exi :: f a -> Exi f

type And :: Prop -> Prop -> Prop
type family And a b where
  And True b = b
  And False _ = False

type FAnd :: (a -> Prop) -> (a -> Prop) -> a -> Prop
type family FAnd f g x where
  FAnd f g x = And (f x) (g x)

-- type Dec :: Type -> Type
-- data Dec a where
--   DecT :: a -> Dec a
--   DecF :: (Not a) -> Dec a

type Iff :: Prop -> Prop -> Prop
type family Iff a b where
  Iff a b = And (Imp a b) (Imp b a)

type Xor :: Prop -> Prop -> Prop
type family Xor a b where
  Xor a b = Not (Iff a b)

-----

type Nat :: Type
data Nat where
  Zero :: Nat
  Succ :: Nat -> Nat

type SNat :: Nat -> Type
data SNat n where
  SZero :: SNat Zero
  SSucc :: SNat n -> SNat (Succ n)

thm_nat_ind :: forall (f :: Nat -> Type).
  f Zero -> (forall n. SNat n -> f n -> f (Succ n)) ->
  forall n. SNat n -> f n
thm_nat_ind h0 h1 n = case n of
  SZero -> h0
  SSucc n -> h1 n # thm_nat_ind h0 h1 n

type NatArb :: Nat -> Prop
type family NatArb n where
  NatArb _ = True

-- type Fin :: Nat -> Type
-- data Fin n where
--   FinZero :: Fin (Succ n)
--   FinSucc :: Fin n -> Fin (Succ n)

type NatAdd :: Nat -> Nat -> Nat
type family NatAdd n m where
  NatAdd n Zero = n
  NatAdd n (Succ m) = Succ (NatAdd n m)

thm_nat_zero_add :: forall n. SNat n -> NatAdd Zero n :~: n
thm_nat_zero_add n = case n of
  SZero -> Refl
  SSucc n -> case thm_nat_zero_add n of
    Refl -> Refl

thm_nat_succ_add :: SNat n -> SNat m -> NatAdd (Succ n) m :~: Succ (NatAdd n m)
thm_nat_succ_add n m = case m of
  SZero -> Refl
  SSucc m -> case thm_nat_succ_add n m of
    Refl -> Refl

type NatLe :: Nat -> Nat -> Prop
type family NatLe n m where
  NatLe Zero _ = True
  NatLe (Succ _) Zero = False
  NatLe (Succ n) (Succ m) = NatLe n m

type NatGe :: Nat -> Nat -> Prop
type family NatGe n m where
  NatGe n m = NatLe m n

thm_nat_le_refl :: SNat n -> Proof (NatLe n n)
thm_nat_le_refl n = case n of
  SZero -> Triv
  SSucc n1 -> thm_nat_le_refl n1

thm_nat_eq_zero_of_le_zero :: SNat n -> Proof (NatLe n Zero) -> n :~: Zero
thm_nat_eq_zero_of_le_zero n h = case n of
  SZero -> Refl
  SSucc n1 -> case h of

thm_nat_le_succ_of_le :: SNat n -> SNat m -> Proof (NatLe n m) -> Proof (NatLe n (Succ m))
thm_nat_le_succ_of_le n m h = case n of
  SZero -> Triv
  SSucc n1 -> case m of
    SZero -> case h of
    SSucc m1 -> thm_nat_le_succ_of_le n1 m1 h

thm_nat_le_succ_self :: SNat n -> Proof (NatLe n (Succ n))
thm_nat_le_succ_self n = thm_nat_le_succ_of_le n n # thm_nat_le_refl n

snat_add :: SNat n -> SNat m -> SNat (NatAdd n m)
snat_add n m = case m of
  SZero -> n
  SSucc m1 -> SSucc # snat_add n m1

thm_nat_le_add_right :: SNat n -> SNat m -> Proof (NatLe n (NatAdd n m))
thm_nat_le_add_right n m = case n of
  SZero -> Triv
  SSucc n1 -> case thm_nat_succ_add n1 m of
    Refl -> thm_nat_le_add_right n1 m

thm_nat_add_comm :: SNat n -> SNat m -> NatAdd n m :~: NatAdd m n
thm_nat_add_comm n m = case n of
  SZero -> thm_nat_zero_add m
  SSucc n1 -> case thm_nat_add_comm n1 m of
    Refl -> thm_nat_succ_add n1 m

thm_nat_add_le_add_left_of_le :: SNat n -> SNat m -> SNat k -> Proof (NatLe m k) ->
  Proof (NatLe (NatAdd m n) (NatAdd k n))
thm_nat_add_le_add_left_of_le n m k h = case n of
  SZero -> h
  SSucc n1 -> thm_nat_add_le_add_left_of_le n1 m k h

thm_nat_add_le_add_right_of_le :: SNat n -> SNat m -> SNat k -> Proof (NatLe m k) ->
  Proof (NatLe (NatAdd n m) (NatAdd n k))
thm_nat_add_le_add_right_of_le n m k h = case thm_nat_add_comm n m of
  Refl -> case thm_nat_add_comm n k of
    Refl -> thm_nat_add_le_add_left_of_le n m k h

thm_nat_le_trans :: SNat n -> SNat m -> SNat k ->
  Proof (NatLe n m) -> Proof (NatLe m k) -> Proof (NatLe n k)
thm_nat_le_trans n m k h1 h2 = case n of
  SZero -> Triv
  SSucc n1 -> case (m, k) of
    (SZero, _) -> case h1 of
    (SSucc _, SZero) -> case h2 of
    (SSucc m1, SSucc k1) -> thm_nat_le_trans n1 m1 k1 h1 h2

type NatMul :: Nat -> Nat -> Nat
type family NatMul n m where
  NatMul n Zero = Zero
  NatMul n (Succ m) = NatAdd n (NatMul n m)

thm_nat_zero_mul :: SNat n -> NatMul Zero n :~: Zero
thm_nat_zero_mul n = case n of
  SZero -> Refl
  SSucc n1 -> case thm_nat_zero_mul n1 of Refl -> Refl

thm_nat_add_assoc :: SNat n -> SNat m -> SNat k ->
  NatAdd n (NatAdd m k) :~: NatAdd (NatAdd n m) k
thm_nat_add_assoc n m k = case k of
  SZero -> Refl
  SSucc k1 -> case thm_nat_add_assoc n m k1 of Refl -> Refl

snat_mul :: SNat n -> SNat m -> SNat (NatMul n m)
snat_mul n m = case m of
  SZero -> SZero
  SSucc m1 -> snat_add n # snat_mul n m1

thm_nat_succ_mul :: SNat n -> SNat m -> NatMul (Succ n) m :~: NatAdd m (NatMul n m)
thm_nat_succ_mul n m = case m of
  SZero -> Refl
  SSucc m1 -> case thm_nat_succ_mul n m1 of
    Refl -> let k = snat_mul n m1 in
      case (thm_nat_add_assoc (SSucc n) m1 k, thm_nat_add_assoc (SSucc m1) n k) of
        (Refl, Refl) -> case (thm_nat_succ_add n m1, thm_nat_succ_add m1 n) of
          (Refl, Refl) -> case thm_nat_add_comm n m1 of Refl -> Refl

type NatMin :: Nat -> Nat -> Nat
type family NatMin n m where
  NatMin Zero m = Zero
  NatMin (Succ n) Zero = Zero
  NatMin (Succ n) (Succ m) = Succ (NatMin n m)

type NatTPred :: Nat -> Nat
type family NatTPred n where
  NatTPred Zero = Zero
  NatTPred (Succ n) = n

type NatTSub :: Nat -> Nat -> Nat
type family NatTSub n m where
  NatTSub n Zero = n
  NatTSub (Succ n) (Succ m) = NatTSub n m
  NatTSub Zero (Succ m) = Zero

thm_nat_zero_tsub :: SNat n -> NatTSub Zero n :~: Zero
thm_nat_zero_tsub n = case n of
  SZero -> Refl
  SSucc _ -> Refl

type NatEq :: Nat -> Nat -> Prop
type family NatEq n m where
  NatEq Zero Zero = True
  NatEq Zero (Succ _) = False
  NatEq (Succ _) Zero = False
  NatEq (Succ n) (Succ m) = NatEq n m

type NatLt :: Nat -> Nat -> Prop
type family NatLt n m where
  NatLt n m = And (NatLe n m) (Not (NatEq n m))

type NatGt :: Nat -> Nat -> Prop
type family NatGt n m where
  NatGt n m = NatLt m n

-- thm_nat_exi_gt :: SNat n -> Exi (NatLt n)
-- thm_nat_exi_gt n = Exi # NatLt (thm_nat_le_succ_self n) #
--   Not # \h -> case h of

-- thm_eq_zero_of_not_exi_lt :: SNat n -> Not (Exi (NatGt n)) -> n :~: Zero
-- thm_eq_zero_of_not_exi_lt n h = case n of
--   SZero -> Refl
--   SSucc n1 -> case h of
--     Not h1 -> let
--       h3 = h1 # Exi # NatGt # NatLt (thm_nat_le_succ_self n1) #
--         Not # \h -> case h of
--       in case h3 of

type NatBetween :: Nat -> Nat -> Nat -> Prop
type family NatBetween lb ub n where
  NatBetween lb ub n = And (NatLe lb n) (NatLe n ub)

-- thm_nat_between_lb :: SNat lb -> SNat ub -> NatLe lb ub -> NatBetween lb ub lb
-- thm_nat_between_lb lb ub h = NatBetween (thm_nat_le_refl lb) h
-- 
-- thm_nat_between_ub :: SNat lb -> SNat ub -> NatLe lb ub -> NatBetween lb ub ub
-- thm_nat_between_ub lb ub h = NatBetween h (thm_nat_le_refl ub)
-- 
-- thm_nat_between_all_same :: SNat n -> NatBetween n n n
-- thm_nat_between_all_same n = let h = thm_nat_le_refl n in NatBetween h h
-- 
-- thm_nat_tsub_succ_le_tsub :: SNat n -> SNat m -> NatLe (NatTSub n (Succ m)) (NatTSub n m)
-- thm_nat_tsub_succ_le_tsub n m = case n of
--   SZero -> NatLeZ
--   SSucc n1 -> case m of
--     SZero -> thm_nat_le_succ_self n1
--     SSucc m1 -> thm_nat_tsub_succ_le_tsub n1 m1
-- 
-- thm_nat_tsub_le_intro_right :: SNat n -> NatLe m k -> NatLe (NatTSub m n) (NatTSub k n)
-- thm_nat_tsub_le_intro_right n h = case n of
--   SZero -> h
--   SSucc n1 -> case h of
--     NatLeZ -> NatLeZ
--     NatLeS h1 -> thm_nat_tsub_le_intro_right n1 h1
-- 
-- dec_nat_le :: SNat n -> SNat m -> Dec (NatLe n m)
-- dec_nat_le n m = case n of
--   SZero -> DecT NatLeZ
--   SSucc n1 -> case m of
--     SZero -> DecF # Not # \h -> case h of
--     SSucc m1 -> case dec_nat_le n1 m1 of
--       DecT h -> DecT # NatLeS h
--       DecF (Not h1) -> DecF # Not # \h2 -> case h2 of
--         NatLeS h3 -> h1 h3
-- 
-- dec_not :: Dec p -> Dec (Not p)
-- dec_not h = case h of
--   DecT h1 -> DecF # Not # \h2 -> case h2 of
--     Not h3 -> h3 h1
--   DecF h1 -> DecT h1
-- 
-- iff_symm :: Iff a b -> Iff b a
-- iff_symm (Iff f g) = Iff g f
-- 
-- thm_nat_zero_lt_succ :: SNat n -> NatLt Zero (Succ n)
-- thm_nat_zero_lt_succ n = NatLt NatLeZ # Not # \h -> case h of
-- 
-- thm_nat_succ_lt_succ_of_lt :: NatLt n m -> NatLt (Succ n) (Succ m)
-- thm_nat_succ_lt_succ_of_lt h = case h of
--   NatLt h1 (Not h2) -> NatLt (NatLeS h1) #
--     Not # \h4 -> case h4 of Refl -> h2 Refl
-- 
-- thm_nat_eq_of_le_of_le :: SNat n -> SNat m -> NatLe n m -> NatLe m n -> n :~: m
-- thm_nat_eq_of_le_of_le n m h1 h2 = case (n, m) of
--   (SZero, SZero) -> Refl
--   (SZero, SSucc m1) -> case thm_nat_eq_zero_of_le_zero m h2 of
--   (SSucc n1, SZero) -> case thm_nat_eq_zero_of_le_zero n h1 of
--   (SSucc n1, SSucc m1) -> case (h1, h2) of
--     (NatLeS h3, NatLeS h4) -> case thm_nat_eq_of_le_of_le n1 m1 h3 h4 of
--       Refl -> Refl
-- 
-- thm_nat_lt_of_not_le :: SNat n -> SNat m -> Not (NatLe m n) -> NatLt n m
-- thm_nat_lt_of_not_le n m (Not h1) = case m of
--   SZero -> case h1 NatLeZ of
--   SSucc m1 -> case n of
--     SZero -> thm_nat_zero_lt_succ m1
--     SSucc n1 -> thm_nat_succ_lt_succ_of_lt #
--       thm_nat_lt_of_not_le n1 m1 # Not # \h2 -> h1 # NatLeS h2
-- 
-- -- thm_nat_le_of_not_lt :: SNat n -> SNat m -> Not (NatLt n m) -> NatLe m n
-- -- thm_nat_le_of_not_lt n m (Not h1) = case m of
-- --   SZero -> NatLeZ
-- --   SSucc m1 -> case n of
-- --     SZero -> case h1 # thm_nat_zero_lt_succ m1 of
-- --     SSucc n1 -> NatLeS # thm_nat_le_of_not_lt n1 m1 # Not #
-- --       \(NatLt h2 (Not h3)) -> h1 # NatLt (NatLeS h2) # Not #
-- --       \h4 -> case h4 of Refl -> h3 Refl
-- 
-- thm_nat_le_of_not_lt :: SNat n -> SNat m -> NatLt m n -> Not (NatLe n m)
-- thm_nat_le_of_not_lt n m (NatLt h1 (Not h2)) = Not # \h3 ->
--   h2 # thm_nat_eq_of_le_of_le m n h1 h3
-- 
-- thm_nat_not_le_iff_lt :: SNat n -> SNat m -> Iff (Not (NatLe m n)) (NatLt n m)
-- thm_nat_not_le_iff_lt n m = Iff (thm_nat_lt_of_not_le n m) #
--   thm_nat_le_of_not_lt m n
-- 
-- dec_fmap_of_iff :: Iff a b -> Dec a -> Dec b
-- dec_fmap_of_iff h dec = case h of
--   Iff f g -> case dec of
--     DecT h1 -> DecT # f h1
--     DecF (Not h1) -> DecF # Not # \h2 -> h1 # g h2
-- 
-- dec_contramap_of_iff :: Iff a b -> Dec b -> Dec a
-- dec_contramap_of_iff h = dec_fmap_of_iff # iff_symm h
-- 
-- not_not_of :: a -> Not (Not a)
-- not_not_of a = Not # \(Not h) -> h a
-- 
-- of_not_not :: Dec a -> Not (Not a) -> a
-- of_not_not h1 (Not h2) = case h1 of
--   DecT a -> a
--   DecF na -> case h2 na of
-- 
-- thm_iff_refl :: Iff a a
-- thm_iff_refl = Iff id id
-- 
-- thm_xor_irrefl :: Not (Xor a a)
-- thm_xor_irrefl = Not # \(Xor f g) ->
--   let r = \a -> case f a of Not h -> h a in r # g # Not r
-- 
-- not_iff_of_xor :: Xor a b -> Not (Iff a b)
-- not_iff_of_xor (Xor fx gx) = Not # \(Iff f g) -> let
--   a = gx # Not # \b -> case fx # g b of Not h -> h b
--   in case fx a of Not h -> h # f a
-- 
-- not_iff_not_of_xor :: Xor a b -> Iff a (Not b)
-- not_iff_not_of_xor (Xor f g) = Iff f g
-- 
-- dec_nat_lt :: SNat n -> SNat m -> Dec (NatLt n m)
-- dec_nat_lt n m = dec_fmap_of_iff (thm_nat_not_le_iff_lt n m) #
--   dec_not # dec_nat_le m n
-- 
-- nat_lt_of_succ_lt_succ :: NatLt (Succ n) (Succ m) -> NatLt n m
-- nat_lt_of_succ_lt_succ (NatLt h1 (Not h2)) = case h1 of
--   NatLeS h3 -> NatLt h3 # Not # \h3 -> case h3 of Refl -> h2 Refl
-- 
-- nat_lt_succ_of_le :: SNat n -> SNat m -> NatLe n m -> NatLt n (Succ m)
-- nat_lt_succ_of_le n m h1 = thm_nat_lt_of_not_le n (SSucc m) # Not # \h2 ->
--   case thm_nat_eq_of_le_of_le (SSucc m) m (thm_nat_le_trans h2 h1) #
--     thm_nat_le_succ_self m of
-- 
-- fin_of_lt :: SNat n -> SNat m -> NatLt n m -> Fin m
-- fin_of_lt n m h@(NatLt h1 (Not h2)) = case m of
--   SZero -> case thm_nat_eq_zero_of_le_zero n h1 of
--     Refl -> case h2 Refl of
--   SSucc m1 -> case n of
--     SZero -> FinZero
--     SSucc n1 -> FinSucc # fin_of_lt n1 m1 # nat_lt_of_succ_lt_succ h
-- 
-- fin_of_le_succ :: SNat n -> SNat m -> NatLe n m -> Fin (Succ m)
-- fin_of_le_succ n m h = fin_of_lt n (SSucc m) # nat_lt_succ_of_le n m h
-- 
-- snat_tsub :: SNat n -> SNat m -> SNat (NatTSub n m)
-- snat_tsub n m = case n of
--   SZero -> case thm_nat_zero_tsub m of Refl -> SZero
--   SSucc n1 -> case m of
--     SZero -> n
--     SSucc m1 -> snat_tsub n1 m1
-- 
-- -- nat_le_of_lt_succ :: SNat n -> SNat m -> NatLt n (Succ m) -> NatLe n m
-- -- nat_le_of_lt_succ n m (NatLt h1 (Not h2)) = case n of
-- --   SZero -> NatLeZ
-- --   SSucc n1 -> case m of
-- --     SZero -> case h1 of
-- --       NatLeS h3 -> case thm_nat_eq_zero_of_le_zero n1 h3 of
-- --         Refl -> _
-- 
-- {-
-- 
-- h1 : S n1 <= S m
-- h2 : S n1 /= S m
-- |- S n1 <= m
-- 
-- -}
-- 
-- -- type NatTMod_1 :: Nat -> Nat -> Nat -> Nat
-- -- type family NatTMod_1 n m k where
-- --   NatTMod_1 Zero Zero k 
-- -- 
-- -- type NatTMod :: Nat -> Nat -> Nat
-- -- type family NatTMod n m where
-- --   NatTMod n Zero = Zero
-- --   NatTMod n (Succ m) = NatTMod_1 n m
-- --     (NatTMod (NatTSub n (Succ m)) (Succ m))
-- 
-- -----
-- 
-- type Vec :: Nat -> Type -> Type
-- data Vec n a where
--   VecNil :: Vec Zero a
--   VecCons :: a -> Vec n a -> Vec (Succ n) a
-- 
-- type EVec :: (Nat -> Type) -> Type -> Type
-- data EVec f a where
--   EVec :: Vec n a -> f n -> EVec f a
-- 
-- instance (Show a) => Show (EVec f a) where
--   show (EVec xs _) = show xs
-- 
-- type VecPair :: Nat -> Type -> Type
-- data VecPair n a where
--   VecPair :: Vec n a -> Vec m a -> VecPair (NatAdd n m) a
-- 
-- instance (Show a) => Show (VecPair n a) where
--   show (VecPair xs ys) = show (xs, ys)
-- 
-- type Seq :: Type -> Type
-- data Seq a where
--   Seq :: (forall n. SNat n -> a) -> Seq a
-- 
-- -----
-- 
-- list_to_vec :: [a] -> EVec NatArb a
-- list_to_vec xs = case xs of
--   [] -> EVec VecNil NatArb
--   (x : xs) -> case list_to_vec xs of
--     EVec ys _ -> EVec (VecCons x ys) NatArb
-- 
-- vec_to_list :: Vec n a -> [a]
-- vec_to_list xs = case xs of
--   VecNil -> []
--   VecCons x xs -> x : vec_to_list xs
-- 
-- instance (Show a) => Show (Vec n a) where
--   show xs = show # vec_to_list xs
-- 
-- vec_at :: Fin n -> Vec n a -> a
-- vec_at i xs = case xs of
--   VecNil -> case i of
--   VecCons x xs -> case i of
--     FinZero -> x
--     FinSucc i -> vec_at i xs
-- 
-- vec_eq :: Eq a => Vec n a -> Vec m a -> Prop
-- vec_eq xs ys = case (xs, ys) of
--   (VecNil, VecNil) -> True
--   (VecCons x xs, VecCons y ys) -> x == y && vec_eq xs ys
--   _ -> False
-- 
-- vec_map :: (a -> b) -> Vec n a -> Vec n b
-- vec_map f xs = case xs of
--   VecNil -> VecNil
--   VecCons x xs -> VecCons (f x) # vec_map f xs
-- 
-- vec_len :: Vec n a -> SNat n
-- vec_len xs = case xs of
--   VecNil -> SZero
--   VecCons _ xs -> SSucc # vec_len xs
-- 
-- vec_append :: Vec n a -> Vec m a -> Vec (NatAdd n m) a
-- vec_append xs ys = case xs of
--   VecNil -> case thm_nat_zero_add (vec_len ys) of
--     Refl -> ys
--   VecCons x xs -> case thm_nat_succ_add (vec_len xs) (vec_len ys) of
--     Refl -> VecCons x # vec_append xs ys
-- 
-- reverse' :: Vec n a -> Vec m a -> Vec (NatAdd n m) a
-- reverse' xs ys = case ys of
--   VecNil -> xs
--   VecCons y ys -> case thm_nat_succ_add (vec_len xs) (vec_len ys) of
--     Refl -> reverse' (VecCons y xs) ys
-- 
-- vec_reverse :: Vec n a -> Vec n a
-- vec_reverse xs = case thm_nat_zero_add (vec_len xs) of
--   Refl -> reverse' VecNil xs
-- 
-- vec_head :: Vec (Succ n) a -> a
-- vec_head (VecCons x _) = x
-- 
-- vec_tail :: Vec (Succ n) a -> Vec n a
-- vec_tail (VecCons _ xs) = xs
-- 
-- vec_last :: Vec (Succ n) a -> a
-- vec_last (VecCons x xs) = case xs of
--   VecNil -> x
--   VecCons _ _ -> vec_last xs
-- 
-- vec_init :: Vec (Succ n) a -> Vec n a
-- vec_init (VecCons x xs) = case xs of
--   VecNil -> VecNil
--   VecCons _ _ -> VecCons x # vec_init xs
-- 
-- vec_span :: (a -> Prop) -> Vec n a -> VecPair n a
-- vec_span f xs = case xs of
--   VecNil -> VecPair VecNil VecNil
--   VecCons x ys -> if not # f x
--     then case thm_nat_zero_add (vec_len xs) of
--       Refl -> VecPair VecNil xs
--     else case vec_span f ys of
--       VecPair xs ys -> case thm_nat_succ_add (vec_len xs) (vec_len ys) of
--         Refl -> VecPair (VecCons x xs) ys
-- 
-- vec_replicate :: SNat n -> a -> Vec n a
-- vec_replicate n x = case n of
--   SZero -> VecNil
--   SSucc n -> VecCons x # vec_replicate n x
-- 
-- vec_filter :: (a -> Prop) -> Vec n a -> EVec (NatGe n) a
-- vec_filter f xs = case xs of
--   VecNil -> EVec VecNil # NatGe NatLeZ
--   VecCons x ys -> case vec_filter f ys of
--     e@(EVec ys h) -> case h of
--       NatGe h -> if not # f x
--         then EVec ys # NatGe # thm_nat_le_succ_of_le h
--         else EVec (VecCons x ys) # NatGe # NatLeS h
-- 
-- vec_elem :: Eq a => a -> Vec n a -> Prop
-- vec_elem x xs = case xs of
--   VecNil -> False
--   VecCons y ys -> x == y || vec_elem x ys
-- 
-- vec_nub :: Eq a => Vec n a -> EVec (NatGe n) a
-- vec_nub xs = case xs of
--   VecNil -> EVec VecNil # NatGe NatLeZ
--   VecCons y ys -> case vec_nub ys of
--     EVec zs h -> case h of
--       NatGe h -> if vec_elem y zs
--         then EVec zs # NatGe # thm_nat_le_succ_of_le h
--         else EVec (VecCons y zs) # NatGe # NatLeS h
-- 
-- vec_union :: Eq a => Vec n a -> Vec m a -> EVec (NatBetween n (NatAdd n m)) a
-- vec_union xs ys = case vec_nub ys of
--   EVec zs h1 -> case vec_filter <~ zs # \y -> not # vec_elem y xs of
--     EVec as h2 -> case (h1, h2) of
--       (NatGe h1, NatGe h2) -> EVec (vec_append xs as) # NatBetween
--         (thm_nat_le_add_right (vec_len xs) (vec_len as)) #
--         thm_nat_add_le_add_right_of_le (vec_len xs) (vec_len as) (vec_len ys) #
--         thm_nat_le_trans h2 h1
-- 
-- vec_concat :: SNat m -> Vec n (Vec m a) -> Vec (NatMul n m) a
-- vec_concat m xss = case xss of
--   VecNil -> case thm_nat_zero_mul m of Refl -> VecNil
--   VecCons xs yss -> case thm_nat_succ_mul (vec_len yss) m of
--     Refl -> vec_append xs # vec_concat m yss
-- 
-- vec_concat_map :: (a -> EVec f b) -> Vec n a -> EVec NatArb b
-- vec_concat_map f xs = case xs of
--   VecNil -> EVec VecNil NatArb
--   VecCons x ys -> case vec_concat_map f ys of
--     EVec zs _ -> case f x of
--       EVec as _ -> EVec (vec_append as zs) NatArb
-- 
-- vec_concat_map_same_size :: SNat m -> (a -> Vec m b) -> Vec n a -> Vec (NatMul n m) b
-- vec_concat_map_same_size m f xs = vec_concat m # vec_map f xs
-- 
-- vec_zip_with :: (a -> b -> c) -> Vec n a -> Vec m b -> Vec (NatMin n m) c
-- vec_zip_with f xs ys = case xs of
--   VecNil -> VecNil
--   VecCons x as -> case ys of
--     VecNil -> VecNil
--     VecCons y bs -> VecCons (f x y) # vec_zip_with f as bs
-- 
-- vec_zip :: Vec n a -> Vec m b -> Vec (NatMin n m) (a, b)
-- vec_zip = vec_zip_with mk_pair
-- 
-- vec_sngl :: a -> Vec (Succ Zero) a
-- vec_sngl x = VecCons x VecNil
-- 
-- vec_pair :: a -> a -> Vec (Succ (Succ Zero)) a
-- vec_pair x y = VecCons x # vec_sngl y
-- 
-- vec_intersperse' :: a -> Vec n a -> Vec (NatMul n (Succ (Succ Zero))) a
-- vec_intersperse' z = vec_concat_map_same_size (SSucc # SSucc SZero) # vec_pair z
-- 
-- vec_intersperse :: a -> Vec n a -> Vec (NatTPred (NatMul n (Succ (Succ Zero)))) a
-- vec_intersperse z xs = case xs of
--   VecNil -> VecNil
--   VecCons _ _ -> vec_tail # vec_intersperse' z xs
-- 
-- vec_take :: SNat n -> Vec m a -> Vec (NatMin n m) a
-- vec_take n xs = case n of
--   SZero -> VecNil
--   SSucc n1 -> case xs of
--     VecNil -> VecNil
--     VecCons x ys -> VecCons x # vec_take n1 ys
-- 
-- vec_drop :: SNat n -> Vec m a -> Vec (NatTSub m n) a
-- vec_drop n xs = case n of
--   SZero -> xs
--   SSucc n1 -> case xs of
--     VecNil -> case thm_nat_zero_tsub n of Refl -> VecNil
--     VecCons _ ys -> vec_drop n1 ys
-- 
-- vec_intercalate :: SNat k -> Vec n a -> Vec m (Vec k a) ->
--   Vec (NatTSub (NatMul m (NatAdd n k)) n) a
-- vec_intercalate k xs yss = vec_drop (vec_len xs) # vec_concat_map_same_size
--   (snat_add (vec_len xs) k) (vec_append xs) yss
-- 
-- vec_remove_first_by :: (a -> Prop) -> Vec n a -> EVec (NatBetween (NatTPred n) n) a
-- vec_remove_first_by f xs = case xs of
--   VecNil -> EVec VecNil # thm_nat_between_all_same SZero
--   VecCons x ys -> let n1 = vec_len ys in if f x
--     then EVec ys # thm_nat_between_lb n1 (SSucc n1) # thm_nat_le_succ_self n1
--     else case vec_remove_first_by f ys of
--       EVec zs (NatBetween h1 h2) -> EVec (VecCons x zs) #
--         NatBetween <~ NatLeS h2 # case n1 of
--           SZero -> NatLeZ
--           SSucc n3 -> NatLeS h1
-- 
-- vec_remove_first :: Eq a => a -> Vec n a -> EVec (NatBetween (NatTPred n) n) a
-- vec_remove_first x = vec_remove_first_by (x ==)
-- 
-- vec_remove_firsts_by :: (a -> b -> Prop) -> Vec n a -> Vec m b ->
--   EVec (NatBetween (NatTSub n m) n) a
-- vec_remove_firsts_by f xs ys = let n = vec_len xs in case ys of
--   VecNil -> EVec xs # thm_nat_between_all_same n
--   VecCons y zs -> case vec_remove_first_by (f <~ y) xs of
--     EVec as (NatBetween h1 h2) -> case vec_remove_firsts_by f as zs of
--       EVec bs (NatBetween h3 h4) -> EVec bs #
--         NatBetween <~ thm_nat_le_trans h4 h2 # case n of
--           SZero -> NatLeZ
--           SSucc n4 -> thm_nat_le_trans <~ h3 #
--             thm_nat_tsub_le_intro_right (vec_len zs) h1
-- 
-- vec_remove_firsts :: Eq a => Vec n a -> Vec m a -> EVec (NatBetween (NatTSub n m) n) a
-- vec_remove_firsts = vec_remove_firsts_by (==)
-- 
-- vec_dif :: Eq a => Vec n a -> Vec m a -> EVec (NatBetween (NatTSub n m) n) a
-- vec_dif = vec_remove_firsts
-- 
-- vec_all :: (a -> Prop) -> Vec n a -> Prop
-- vec_all f xs = case xs of
--   VecNil -> True
--   VecCons x ys -> f x && vec_all f ys
-- 
-- vec_any :: (a -> Prop) -> Vec n a -> Prop
-- vec_any f xs = case xs of
--   VecNil -> False
--   VecCons x ys -> f x || vec_any f ys
-- 
-- vec_and :: Vec n Prop -> Prop
-- vec_and = vec_all id
-- 
-- vec_or :: Vec n Prop -> Prop
-- vec_or = vec_any id
-- 
-- vec_break :: (a -> Prop) -> Vec n a -> VecPair n a
-- vec_break f = vec_span # not . f
-- 
-- -- vec_cycle :: Vec n a -> Seq a
-- -- vec_cycle xs = Seq (\n -> vec_at <~ xs # nat_mod_fin n # vec_len xs)
-- 
-- -- cycle
-- -- delete
-- -- deleteBy
-- -- drop
-- -- dropWhile
-- -- dropWhileEnd
-- -- elem
-- -- elemIndex
-- -- elemIndices
-- -- filter
-- -- find
-- -- findIndex
-- -- findIndices
-- -- foldl
-- -- foldl'
-- -- foldl1
-- -- foldl1'
-- -- foldr
-- -- foldr1
-- -- genericDrop
-- -- genericIndex
-- -- genericLength
-- -- genericReplicate
-- -- genericSplitAt
-- -- genericTake
-- -- group
-- -- groupBy
-- -- head
-- -- init
-- -- inits
-- -- insert
-- -- insertBy
-- -- intercalate
-- -- intersect
-- -- intersectBy
-- -- intersperse
-- -- isInfixOf
-- -- isPrefixOf
-- -- isSubsequenceOf
-- -- isSuffixOf
-- -- iterate
-- -- iterate'
-- -- last
-- -- length
-- -- lines
-- -- lookup
-- -- map
-- -- mapAccumL
-- -- mapAccumR
-- -- maximum
-- -- maximumBy
-- -- minimum
-- -- minimumBy
-- -- notElem
-- -- nub
-- -- nubBy
-- -- null
-- -- partition
-- -- permutations
-- -- product
-- -- repeat
-- -- replicate
-- -- reverse
-- -- scanl
-- -- scanl'
-- -- scanl1
-- -- scanr
-- -- scanr1
-- -- singleton
-- -- sort
-- -- sortBy
-- -- sortOn
-- -- span
-- -- splitAt
-- -- stripPrefix
-- -- subsequences
-- -- sum
-- -- tail
-- -- tails
-- -- take
-- -- takeWhile
-- -- transpose
-- -- uncons
-- -- unfoldr
-- -- union
-- -- unionBy
-- -- unlines
-- -- unwords
-- -- unzip
-- -- unzip3
-- -- unzip4
-- -- unzip5
-- -- unzip6
-- -- unzip7
-- -- words
-- -- zip
-- -- zip3
-- -- zip4
-- -- zip5
-- -- zip6
-- -- zip7
-- -- zipWith
-- -- zipWith3
-- -- zipWith4
-- -- zipWith5
-- -- zipWith6
-- -- zipWith7
-- 
-- -----
-- 
-- main :: IO ()
-- main = do
--   print #
--     vec_append
--     (VecCons 'a' # VecCons 'b' # VecCons 'c' # VecCons 'd' # VecNil)
--     (VecCons 'e' # VecCons 'f' # VecCons 'g' # VecCons 'h' # VecNil)

main :: IO ()
main = putStrLn "ok"