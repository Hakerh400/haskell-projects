import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.Maybe
import Data.List
import Data.Char
import Control.Monad
import Control.Monad.State
import Control.Monad.Except

import Util

main :: IO ()
main = do
  putStrLn # intercalate "\n" # map show #
    list_quagdonic 10 200

is_quagdonic :: N -> N -> Prop
is_quagdonic m n = let
  f n = ite (n == 0) 0 # 1 + f (div n m)
  ub = f n - 1
  in any <~ [1 .. ub] # \k -> let
    p = product # map <~ [k .. ub] # \i ->
      mod (div n # m ^ i) m
    in p == mod n (m ^ k)

list_quagdonic :: N -> N -> [N]
list_quagdonic m n = take (fi n) #
  filter (is_quagdonic m) [1 ..]