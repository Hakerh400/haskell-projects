module DB
  ( DB
  , Struct(..)
  , dbInit
  , dbStId
  , dbIdSt
  , dbHasSt
  , dbHasId
  , dbInsert
  ) where

import Data.Maybe

import Data.Set (Set)
import Data.Map (Map)
import qualified Data.Set as Set
import qualified Data.Map as Map

import Base

data Struct = Nil | Pair Struct Struct
  deriving (Eq, Ord)

type Map1 = Map Struct (Map Struct N)
type Map2 = [Struct]
data DB = DB Map1 Map2

-- stExa :: (Struct -> Struct -> a) -> a -> Struct -> a
-- stExa _ z Nil = z
-- stExa f _ (Pair a b) = f a b

dbInit :: IO DB
dbInit = pure # DB Map.empty []

dbStId :: Struct -> DB -> IO (Maybe N)
dbStId Nil _ = pure # Just 0
dbStId (Pair a b) (DB m _) = pure # Map.lookup a m >>= Map.lookup b

dbIdSt :: N -> DB -> IO (Maybe Struct)
dbIdSt n (DB _ m)
  | n < 0 = pure # Nothing
  | n == 0 = pure # Just Nil
  | otherwise = pure # listGet (n - 1) m

dbHasSt :: Struct -> DB -> IO Bool
dbHasSt s db = dbStId s db >>= pure . isJust

dbHasId :: N -> DB -> IO Bool
dbHasId n db = dbIdSt n db >>= pure . isJust

dbInsert :: Struct -> DB -> IO (N, DB)
dbInsert Nil db = pure (0, db)
dbInsert st@(Pair a b) db@(DB m1 m2) = do
  sti <- dbStId st db
  case sti of
    Just i -> pure (i, db)
    Nothing -> let
      i = len m2 + 1
      m = Map.findWithDefault Map.empty a m1
      m1' = Map.insert a (Map.insert b i m) m1
      m2' = st : m2
      in pure (i, DB m1' m2')