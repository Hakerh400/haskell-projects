module Base where

type N = Integer

infixr 0 #
(#) :: (a -> b) -> a -> b
(#) = ($)

len :: [a] -> N
len = fromIntegral . length

listGet :: N -> [a] -> Maybe a
listGet _ [] = Nothing
listGet 0 (x:_) = Just x
listGet n (_:xs) = listGet (n - 1) xs

(⊥) :: a
(⊥) = undefined