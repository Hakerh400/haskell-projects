module HOL
  (
  ) where

import Base
import DB

newtype HOL = HOL DB

data SortEnum = TYPE | EXPR | PROOF
  deriving (Enum)

data TypeEnum = POLY | BOOL | NAT | FUNC
  deriving (Enum)

data ValEnum = COMB_K | COMB_S | CALL | IMP | EQ | THE
  deriving (Enum)

type Axiom = HOL -> IO (N, Struct, HOL)

holInit :: IO HOL
holInit = dbInit >>= pure . HOL

applyK :: Axiom
applyK = litSt #
  Pair (enumToSt EXPR) #
  Pair (enumToSt COMB_K) #
  Pair (natToSt 2) #
  Pair (enumToSt FUNC) # Pair
    (Pair (enumToSt POLY) (natToSt 0))
    (Pair (enumToSt FUNC) # Pair
      (Pair (enumToSt POLY) (natToSt 1))
      (Pair (enumToSt POLY) (natToSt 0)))

litSt :: Struct -> Axiom
litSt st (HOL db) = do
  (i, db') <- dbInsert st db
  return (i, st, HOL db')

natToSt :: N -> Struct
natToSt 0 = Nil
natToSt n = Pair Nil # natToSt (n - 1)

enumToSt :: (Enum a) => a -> Struct
enumToSt = natToSt . fromIntegral . fromEnum