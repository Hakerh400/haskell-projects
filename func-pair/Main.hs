{-# LANGUAGE RankNTypes #-}

import Util

import Data.List
import Data.Set (Set)
import Data.Map (Map)

import qualified Data.Set as Set
import qualified Data.Map as Map

import Prelude hiding (fst, snd)

main :: IO ()
main = do
  let p = pair 5 "ok"
  print # fst p
  print # snd p

type Pair a b = forall c. (a -> b -> c) -> c

pair :: a -> b -> Pair a b
pair a b f = f a b

fst :: Pair a b -> a
fst f = f # \a b -> a

snd :: Pair a b -> b
snd f = f # \a b -> b