import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)
import Data.Maybe
import Data.List

import Util
import Bit

n = 1234

main :: IO ()
main = do
  putStrLn # format_from # iterate f n

f :: N -> N
f n = let
  k = ceil_sqrt # n + 1
  in k * (k ^ 2 - n)

ceil_sqrt :: N -> N
ceil_sqrt n = bisect # \k ->
  n <= k ^ 2

bisect_aux' :: (N -> Prop) -> N -> N -> N
bisect_aux' f i j = if j - i <= 1 then j else let
  k = div (i + j) 2
  in if f k
    then bisect_aux' f i k
    else bisect_aux' f k j

bisect_aux :: (N -> Prop) -> N -> N
bisect_aux f j = if f j
  then bisect_aux' f (div j 2) j
  else bisect_aux f # max 1 (j * 2)

bisect :: (N -> Prop) -> N
bisect f = bisect_aux f 0

replace :: (Ord a) => Map a a -> [a] -> [a]
replace mp = map # \x -> case Map.lookup x mp of
  Nothing -> x
  Just y -> y

take_from' :: (Ord a) => Map a N -> [a] -> ([a], N)
take_from' mp (x:xs) = case Map.lookup x mp of
  Nothing -> let
    i = map_size mp
    (ys, j) = take_from' (map_insert x i mp) xs
    in (x : ys, j)
  Just i -> ([], i)

take_from :: (Ord a) => [a] -> ([a], N)
take_from = take_from' Map.empty

format_from :: (Ord a, Show a) => [a] -> String
format_from xs = fromJust # do
  (xs, i) <- pure # take_from xs
  (xs, ys) <- pure # splitAt (fi i) # map show xs
  let n = maximum (map len ys) + 4
  (z:ys) <- pure ys
  z <- pure # snoc' '\\' # pad_end n '-' # z ++ " <"
  ys <- pure # flip map ys # \s ->
    pad_end n space s ++ "|"
  let r = Map.fromList [('\\', '/'), ('<', '-')]
  ys <- pure # z : snoc ys (replace r z)
  return # unlines' # xs ++ ys