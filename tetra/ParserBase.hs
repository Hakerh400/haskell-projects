module ParserBase where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Control.Applicative
import Control.Monad
import GHC.Read
import Text.ParserCombinators.ReadPrec
import qualified Text.ParserCombinators.ReadP as ReadP

import Util

infixr 5 ++>
(++>) :: ReadPrec a -> ReadPrec a -> ReadPrec a
(++>) = flip (<++)

parse_raw :: ReadPrec a -> String -> [(a, String)]
parse_raw m str = readPrec_to_S m 0 str

parse' :: ReadPrec a -> String -> Either Message a
parse' m str = case filter (null . snd) # parse_raw m str of
  [(a, "")] -> Right a
  (a : b : _) -> Left ["Ambiguous parse"]
  _ -> Left ["No parse"]

parse :: ReadPrec a -> String -> a
parse m str = case parse' m str of
  Left msg -> error # concat msg
  Right a -> a

p_char :: Char -> ReadPrec Char
p_char c = get >>= \c1 -> ite (c1 == c) (pure c) pfail

p_char' :: Char -> ReadPrec ()
p_char' c = p_char c >> pure ()

p_str :: String -> ReadPrec String
p_str [] = pure ""
p_str s@(c:cs) = p_char c >> p_str cs >> pure s

p_all' :: ReadPrec a -> ReadPrec [a]
p_all' r = pure [] ++> do
  x <- r
  xs <- p_all' r
  return # x : xs

p_all :: ReadPrec a -> ReadPrec [a]
p_all r = do
  xs <- p_all' r
  ite (null xs) pfail (pure xs)

p_sep' :: ReadPrec a -> ReadPrec b -> ReadPrec ([a], [b])
p_sep' r s = pure ([], []) ++> do
  x <- r
  zs <- p_all' # do
    b <- s
    a <- r
    return (a, b)
  let (xs, ys) = unzip zs
  return (x : xs, ys)

p_sep :: ReadPrec a -> ReadPrec b -> ReadPrec ([a], [b])
p_sep r s = do
  (a, b) <- p_sep' r s
  ite (null a) pfail (pure (a, b))

p_sep1 :: ReadPrec a -> ReadPrec b -> ReadPrec ([a], [b])
p_sep1 r s = do
  (a, b) <- p_sep r s
  ite (null # tail a) pfail (pure (a, b))

p_sepf' :: ReadPrec a -> ReadPrec b -> ReadPrec [a]
p_sepf' r s = do
  result <- p_sep' r s
  return # fst result

p_sepf :: ReadPrec a -> ReadPrec b -> ReadPrec [a]
p_sepf r s = do
  result <- p_sep r s
  return # fst result

p_take_while :: (Char -> Prop) -> ReadPrec String
p_take_while f = p_all' # get >>= \c -> ite (f c) (pure c) pfail

p_drop_while :: (Char -> Prop) -> ReadPrec ()
p_drop_while f = p_take_while f >> pure ()

p_take_while1 :: (Char -> Prop) -> ReadPrec String
p_take_while1 f = do
  xs <- p_take_while f
  if null xs then pfail else pure xs

is_ident_char :: Char -> Prop
is_ident_char c = or
  [ is_letter c
  , is_digit c
  , is_sub_digit c
  , c == '\x5F'
  , c == '\''
  ]

p_ident :: ReadPrec String
p_ident = do
  c <- get
  if is_letter c then do
    s <- p_take_while is_ident_char
    return # c : s
  else pfail

p_ident' :: ReadPrec String
p_ident' = p_take_while1 is_ident_char

p_nat :: ReadPrec N
p_nat = do
  s <- p_ident'
  ite (p_is_valid_nat s) (pure # (read s :: N)) pfail

p_is_valid_nat :: String -> Prop
p_is_valid_nat s = s == "0" ||
  ( not (null s) &&
    all isDigit s &&
    all (/= '0') s )

p_parens :: ReadPrec a -> ReadPrec a
p_parens r = do
  p_tok "("
  a <- r
  p_tok ")"
  return a

p_parens_lf :: ReadPrec a -> ReadPrec a
p_parens_lf r = do
  p_tok_lf "("
  a <- r
  p_tok ")"
  return a

p_either :: ReadPrec a -> ReadPrec b -> ReadPrec (Either a b)
p_either a b = (a >>= liftm Left) +++ (b >>= liftm Right)

p_either' :: ReadPrec a -> ReadPrec b -> ReadPrec (Either a b)
p_either' a b = (a >>= liftm Left) <++ (b >>= liftm Right)

p_sp :: ReadPrec ()
p_sp = p_char' space

p_rest :: ReadPrec String
p_rest = p_take_while # const True

p_eof :: ReadPrec ()
p_eof = lift ReadP.eof

choice' :: [ReadPrec a] -> ReadPrec a
choice' xs = if null xs then pfail else foldr1 (<++) xs

p_new_line' :: ReadPrec String
p_new_line' = p_str "\r\n" <++ choice' [p_str "\r", p_str "\n"]

p_new_line :: ReadPrec ()
p_new_line = p_new_line' >> nop

p_ws_aux :: Prop -> Prop -> ReadPrec ()
p_ws_aux lf one = do
  let f = ite one p_all p_all'
  let p = ite lf (choice' [p_sp, p_new_line]) p_sp
  f p >> nop

p_ws :: ReadPrec ()
p_ws = p_ws_aux False False

p_ws_lf :: ReadPrec ()
p_ws_lf = p_ws_aux True False

p_ws1 :: ReadPrec ()
p_ws1 = p_ws_aux False True

p_ws1_lf :: ReadPrec ()
p_ws1_lf = p_ws_aux True True

trimmed_aux :: ReadPrec () -> ReadPrec a -> ReadPrec a
trimmed_aux ws r = do
  ws
  x <- r
  ws
  return x

trimmed :: ReadPrec a -> ReadPrec a
trimmed = trimmed_aux p_ws

trimmed_lf :: ReadPrec a -> ReadPrec a
trimmed_lf = trimmed_aux p_ws_lf

p_tok :: String -> ReadPrec String
p_tok t = trimmed # p_str t

p_tok_lf :: String -> ReadPrec String
p_tok_lf t = trimmed_lf # p_str t

p_tok' :: String -> ReadPrec String
p_tok' t = trimmed # do
  p_str # t ++ " "
  return t

p_toks :: [String] -> ReadPrec [String]
p_toks = mapM p_tok

p_toks_lf :: [String] -> ReadPrec [String]
p_toks_lf = mapM p_tok_lf

p_toks' :: [String] -> ReadPrec [String]
p_toks' = mapM p_tok'

p_maybe :: ReadPrec a -> ReadPrec (Maybe a)
p_maybe p = (p >>= liftm Just) <++ pure Nothing

p_maybe' :: ReadPrec a -> ReadPrec (Maybe a)
p_maybe' p = (p >>= liftm Just) +++ pure Nothing

p_list :: ReadPrec a -> ReadPrec [a]
p_list p = do
  p_tok "["
  xs <- p_sepf p (p_tok_lf ",")
  p_tok "]"
  return xs

p_list' :: ReadPrec a -> ReadPrec [a]
p_list' p = (p >>= liftm pure) <++ p_list p

p_list_arg :: (Prop -> ReadPrec a) -> ReadPrec [a]
p_list_arg f = (f False >>= liftm pure) <++ p_list (f True)

p_assert :: Prop -> ReadPrec ()
p_assert p = if p then nop else pfail