import Util
import ParserBase

import Data.List
import Data.Set (Set)
import Data.Map (Map)
import GHC.Read
import Text.ParserCombinators.ReadPrec hiding (step)

import qualified Data.Set as Set
import qualified Data.Map as Map

data Group = Group {_gs :: Groups}
type Groups = [Group]

is_empty :: Group -> Prop
is_empty = null . _gs

is_nempty :: Group -> Prop
is_nempty = not . is_empty

show_group :: Group -> String
show_group g = put_in_parens # _gs g >>= show_group

show_groups :: Groups -> String
show_groups gs = gs >>= show_group

show_groups' :: Groups -> String
show_groups' = show_groups . filter is_nempty

parse_group :: ReadPrec Group
parse_group = p_parens # do
  gs <- parse_groups
  return # Group gs

parse_groups :: ReadPrec Groups
parse_groups = p_all' parse_group

step :: Groups -> (Groups, Prop)
step gs = case gs of
  [] -> ([], False)
  (g : rest) -> case _gs g of
    (Group [] : gs') -> let
      g' = Group gs'
      in (g' : g' : rest, True)
    gs -> let
      (gs', p) = step gs
      in if p then (Group gs' : rest, True) else let
        (gs', p) = step rest
        in (g : gs', p)

run :: Groups -> [Groups]
run gs = gs : let
  (gs', p) = step gs
  in if p then run gs' else []

main :: IO ()
main = do
  let gs = parse parse_groups "(()(()()))"
  let res = run gs
  putStrLn # unlines' # map show_groups' res
  print # len # last res