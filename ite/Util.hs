module Util where

type N = Integer
type Prop = Bool

infixr 0 #
(#) = id