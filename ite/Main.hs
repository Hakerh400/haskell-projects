import Util

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Maybe

ite :: Prop -> a -> a -> a
ite p a b = fromJust # Map.lookup p #
  Map.fromList [(True, a), (False, b)]

f :: Prop -> String
f p = ite p "A" "B"

main :: IO ()
main = do
  putStrLn # f True
  putStrLn # f False