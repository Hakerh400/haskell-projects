{-# LANGUAGE TypeFamilies #-}

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)
import Data.IORef
import Data.Kind (Type)

import Util
import Bit

class Representable f where
  type Rep f :: Type
  tabulate :: (Rep f -> x) -> f x
  index :: f x -> Rep f -> x

data Nat = Z | S Nat
  deriving (Eq, Ord, Show)

data Stream a = Cons a (Stream a)
  deriving (Eq, Ord, Show)

instance Representable Stream where
  type Rep Stream = Nat
  tabulate f = Cons (f Z) # tabulate # f . S
  index (Cons x xs) i = case i of
    Z -> x
    S i -> index xs i

newtype Prod f g a = Prod (f a, g a)
newtype Sum f g a = Sum (Either (f a) (g a))

instance (Representable f, Representable g) => Representable (Prod f g) where
  type Rep (Prod f g) = Either (Rep f) (Rep g)
  tabulate f = Prod (tabulate # f . Left, tabulate # f . Right)
  index (Prod (a, b)) i = case i of
    Left i -> index a i
    Right i -> index b i

data Iso a b = Iso (a -> b) (b -> a)
data F1 a f = F1 (forall x. (a -> x) -> f x)

yoneda_1 :: forall a f. (Functor f) => Iso (F1 a f) (f a)
yoneda_1 = Iso (\(F1 f) -> f id) (\d -> F1 (fmap <~ d))

type F2' g = forall f a. (Functor f) => g a f
data F2a = F2a (F2' F1)
data F2b' a f = F2b' (f a)
data F2b = F2b (F2' F2b')

yoneda_2 :: Iso F2a F2b
yoneda_2 = Iso
  (\(F2a f) -> F2b (F2b' (case yoneda_1 of Iso g _ -> g f)))
  (\f -> F2a (case (f, yoneda_1) of (F2b (F2b' f), Iso _ g) -> g f))

main :: IO ()
main = do
  putStrLn "ok"