import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Ord
import Data.Foldable
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Control.Applicative hiding ((<|>))
import Control.Monad
import Control.Monad.State hiding (fix)
import Control.Monad.Except hiding (fix)
import Control.Monad.Trans.Maybe hiding (fix)
import System.IO.Unsafe
import GHC.Stack
import qualified Prelude
import Prelude hiding (print, putStrLn, getLine)
import System.IO hiding (print, putStrLn, getLine)
import Data.IORef

import Util

data Expr
  = K
  | S
  | App Expr Expr
  deriving (Eq, Ord)

instance Show Expr where
  show = show_expr

type Parser = StateT String Maybe
type Reducer = StateT N Maybe
type Result = (Expr, Maybe (Expr, N))

exprs_num = 100 * 10 ^ 3
max_reduction_steps = 10 ^ 3

main :: IO ()
main = do
  mapM_ (hSetBuffering <~ NoBuffering) [stdin, stdout, stderr]
  
  -- xs <- pure # sortBy sort_fn # map <~
  --   take exprs_num all_exprs # \e -> (e, reduce' e)
  -- s <- pure # unlines' # map <~ xs # \(e, r) -> concat # case r of
  --   Nothing -> ["!!! ", show e]
  --   Just (r, n) -> [show n, " ", show e, " ---> ", show r]
  -- writeFile "C:/Users/User/Downloads/1.txt" s
  
  e <- find_expr_m # \e -> isJust # do
    e <- reduce e
    x <- pure # App K # App K # App K # App K # App K S
    r <- reduce # App e x
    asrt # r == x
    -- K <- reduce # App e S
    return ()
  logb
  print e

find_expr :: (Expr -> Prop) -> Expr
find_expr f = fromJust # find f all_exprs

find_expr_m' :: (Expr -> Prop) -> N -> [Expr] -> IO Expr
find_expr_m' f i es = case es of
  (e : es) -> if f e then pure e else do
    ifm (i == 0) # print e
    i <- pure # mod (i + 1) # 10 ^ 3
    find_expr_m' f i es
  _ -> undefined

find_expr_m :: (Expr -> Prop) -> IO Expr
find_expr_m f = find_expr_m' f 0 all_exprs

sort_fn :: Result -> Result -> Ordering
sort_fn (e1, r1) (e2, r2) = let
  dflt = compare e1 e2
  in case compare (isJust r1) (isJust r2) of
    EQ -> case (r1, r2) of
      (Just (r1, n1), Just (r2, n2)) -> case compare n2 n1 of
        EQ -> dflt
        r -> r
      _ -> dflt
    r -> r

reduce_step :: Reducer ()
reduce_step = do
  n <- get
  asrt # n /= 0
  modify dec

mk_app :: Expr -> Expr -> Reducer Expr
mk_app a b = case a of
  App K x -> reduce_step >> pure x
  App (App S f) g -> do
    reduce_step
    r1 <- mk_app f b
    r2 <- mk_app g b
    mk_app r1 r2
  _ -> pure # App a b

reduce_expr :: Expr -> Reducer Expr
reduce_expr e = case e of
  App a b -> do
    a <- reduce_expr a
    b <- reduce_expr b
    mk_app a b
  _ -> pure e

reduce' :: Expr -> Maybe (Expr, N)
reduce' e = do
  let n = max_reduction_steps
  (e, n') <- runStateT (reduce_expr e) n
  return (e, n - n')

reduce :: Expr -> Maybe Expr
reduce e = fmap fst # reduce' e

show_expr :: Expr -> String
show_expr e = case e of
  K -> "K"
  S -> "S"
  App a b -> let
    sa = show a
    sb = show b
    in sa ++ case b of
      App _ _ -> put_in_parens sb
      _ -> sb

parse_expr_1 :: Prop -> Parser Expr
parse_expr_1 h = do
  (c : s) <- get
  put s
  case c of
    'K' -> pure K
    'S' -> pure S
    '(' -> do
      True <- pure h
      e <- parse_expr
      (')' : s) <- get
      put s
      return e
    _ -> lift Nothing

parse_expr' :: Maybe Expr -> Parser Expr
parse_expr' e0 = do
  s <- get
  case s of
    "" -> do
      Just e <- pure e0
      return e
    (')' : _) -> do
      Just e@(App _ _) <- pure e0
      return e
    _ -> do
      e <- parse_expr_1 # isJust e0
      e <- pure # case e0 of
        Nothing -> e
        Just e0 -> App e0 e
      parse_expr' # Just e

parse_expr :: Parser Expr
parse_expr = parse_expr' Nothing

parse :: Parser a -> String -> Maybe a
parse f s = do
  (e, "") <- runStateT f s
  return e

mk_expr_strs' :: N -> [N] -> [String]
mk_expr_strs' n stack =
  if n == 0 then pure "" else do
    c <- ")KS("
    stack <- case c of
      '(' -> pure # 0 : stack
      ')' -> do
        (k : stack) <- pure stack
        asrt # k >= 2
        return stack
      _ -> case stack of
        [] -> pure []
        (k : stack) -> pure # (k + 1) : stack
    n <- pure # n - 1
    asrt # len stack <= n
    s <- mk_expr_strs' n stack
    return # c : s

mk_expr_strs :: N -> [String]
mk_expr_strs n = mk_expr_strs' n []

all_exprs_n :: N -> [Expr]
all_exprs_n n = do
  s <- mk_expr_strs n
  -- tracem s
  Just e <- pure # parse parse_expr s
  return e

all_exprs :: [Expr]
all_exprs = [0..] >>= all_exprs_n