import Util

import Data.List
import Data.Set (Set)
import Data.Map (Map)

import qualified Data.Set as Set
import qualified Data.Map as Map

to_digits' :: N -> [N]
to_digits' n = if n == 0 then [] else
  mod n 10 : to_digits' (div n 10)

to_digits :: N -> [N]
to_digits n = if n == 0 then [0] else
  reverse # to_digits' n

f :: N -> N -> [N]
f a b = to_digits # a + b

main :: IO ()
main = do
  flip mapM_ (run # to_digits 1234) # \xs ->
    putStrLn # xs >>= show
  
  -- flip mapM_ [1..10000] # \n -> do
  --   putStr # concat [show n, " -> "]
  --   putStrLn # show # ite (terminates # to_digits n) 1 0

  -- print # take 10 #
  --   sortOn ((0-) . snd) #
  --   map (\(n, res) -> (n, len res)) #
  --   filter (terminates_res . snd) #
  --   map (\n -> (n, run # to_digits n)) [50000000..100000000]

pairwise :: [N] -> [N]
pairwise xs = zip xs (tail xs) >>= uncurry f

check_repeat :: (Eq a) => a -> N -> [a] -> Prop
check_repeat z n xs = if n == 0 then True else case xs of
  [] -> False
  (x:xs) -> x == z && check_repeat z (n - 1) xs

check_nterm_aux_1 :: [N] -> Prop
check_nterm_aux_1 xs = check_repeat 9 2 xs &&
  case drop 2 xs of
    (n:_) -> n >= 1
    _ -> False

check_nterm_aux_2 :: [N] -> Prop
check_nterm_aux_2 xs = check_repeat 6 3 xs &&
  case drop 3 xs of
    (n:_) -> n >= 4
    _ -> False

check_nterm :: [N] -> Prop
check_nterm xs = if null xs then False
  -- else if len xs >= 20 then True
  else if check_nterm_aux_1 xs then True
  else if check_nterm_aux_2 xs then True
  else if check_repeat 8 4 xs then True
  else check_nterm # tail xs

run_step :: [N] -> [[N]]
run_step xs = if check_nterm xs then [] else
  run # pairwise xs

run :: [N] -> [[N]]
run xs = xs : case xs of
  [_] -> []
  _ -> run_step xs

terminates_res :: [[N]] -> Prop
terminates_res res = null # tail # last res

terminates :: [N] -> Prop
terminates xs = terminates_res # run xs