import qualified Data.Set as Set
import qualified Data.Map as Map

import Util
import Bit

data ProgT = ProgT
  { _prog_cats_num :: N
  , _prog_ents_num :: N
  , _prog_cats :: [[N]]
  } deriving (Eq, Ord, Show)

type Prog = StateT ProgT []

main :: IO ()
main = do
  let xs = run_prog prog
  mapM_ print xs
  -- print # len xs

prog_set_size :: N -> N -> Prog ()
prog_set_size cn en = do
  let c = ico en
  cs <- replicateM (fi cn - 1) # lift # permutations c
  put # ProgT
    { _prog_cats_num = cn
    , _prog_ents_num = en
    , _prog_cats = c : cs
    }

get_cats :: Prog [[N]]
get_cats = gets _prog_cats

get_cat :: N -> Prog [N]
get_cat i = gets # list_get i . _prog_cats

asrt :: Prop -> Prog ()
asrt p = ifmn p # lift []

prog :: Prog ()
prog = do
  prog_set_size 4 5
  
  (
    [ ns@[n1, n2, n3, n4, n5]
    , [calocarpa, dame, happenstance, trier, winecup]
    , [casey, hannah, ollie, shannon, wendy]
    , [dunlap, enfield, fort, hardy, spragueville]
    ]) <- get_cats
  
  n <- pure # \i -> fi # fromJust # elemIndex i [0..]
  
  asrt # ns == sort [happenstance, wendy, fort, n3, n4]
  asrt # n enfield == n hannah + 2
  asrt # dame /= n3
  asrt # n hardy == n dunlap - 1
  asrt # (casey == dunlap) /= (casey == winecup)
  asrt # calocarpa /= n3 && sort [calocarpa, n3] == sort [spragueville, ollie]
  asrt # n calocarpa == n enfield + 1
  asrt # (enfield == wendy) /= (enfield == ollie)

inv_perm :: [N] -> [N]
inv_perm xs = map <~ (ico # len xs) #
  \i -> fi # fromJust # elemIndex i xs

run_prog :: Prog () -> [[[N]]]
run_prog f = evalStateT <~ und # f >> do
  cats <- gets _prog_cats
  return # map inv_perm cats