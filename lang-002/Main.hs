{-# LANGUAGE TypeSynonymInstances #-}

import Util
import Bit

import Data.Char
import Data.List
import Data.Set (Set)
import Data.Map (Map)

import qualified Data.Set as Set
import qualified Data.Map as Map

src_file = "src.txt"
inp_file = "inp.txt"

data S = N | P S S
  deriving (Eq, Ord)

struct_to_list :: S -> [S]
struct_to_list s = case s of
  N -> []
  P a b -> a : struct_to_list b

show_struct :: S -> String
show_struct s = case struct_to_list s of
  [] -> "."
  xs -> case last xs of
    N -> put_in_brackets # init xs >>= show
    _ -> put_in_parens # xs >>= show

instance Show S where
  show = show_struct

main :: IO ()
main = do
  src <- readFile src_file
  inp <- readFile inp_file
  src <- pure # parse_struct src
  inp <- pure # to_struct [0::N,1,2]--inp
  print src
  print inp

parse_group' :: String -> (Maybe S, String)
parse_group' str@(c:_) = case c of
  ')' -> (Nothing, str)
  _ -> let
    (a, str1) = parse_struct' str
    (b, str2) = parse_group' str1
    s = maybe a (P a) b
    in (Just s, str2)

parse_group :: String -> (S, String)
parse_group str = case parse_group' str of
  (Just s, str) -> (s, str)
  (Nothing, str) -> (N, str)

parse_struct' :: String -> (S, String)
parse_struct' (c:str) = case c of
  '.' -> (N, str)
  '(' -> let (s, ')':str1) = parse_group str
    in (s, str1)

parse_struct :: String -> S
parse_struct str = case parse_struct' # filter (`elem` ".()") str of
  (s, "") -> s

class ToStruct a where
  to_struct :: a -> S

instance ToStruct N where
  to_struct n = if n == 0 then N else
    P N # to_struct # n - 1

instance ToStruct Int where
  to_struct n = to_struct (fi n :: N)

instance ToStruct Char where
  to_struct c = to_struct # ord c

instance (ToStruct a) => ToStruct [a] where
  to_struct = foldr (P . to_struct) N